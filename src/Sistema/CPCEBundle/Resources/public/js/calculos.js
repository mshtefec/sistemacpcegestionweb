var total = 0;
var eVal = 0;
function calcularTotal() {
    total = 0;
    calculo_monto_honorario = parseFloat($('.calculo_monto_honorario').val().replace(",", "."));
    calculo_monto_honorario = (isNaN(calculo_monto_honorario)) ? 0 : calculo_monto_honorario;
    $('.calculo_monto_honorario').val(calculo_monto_honorario.toLocaleString().replace(".", ""));
    //calculo_monto_honorario = parseFloat($('.calculo_monto_honorario').val());
    calculo_monto_iva = parseFloat($('.calculo_monto_iva').val().replace(",", "."));
    calculo_monto_iva = (isNaN(calculo_monto_iva)) ? 0 : calculo_monto_iva;
    $('.calculo_monto_iva').val(calculo_monto_iva.toLocaleString().replace(".", ""));
    //calculo_monto_iva       = parseFloat($('.calculo_monto_iva').val());
    calculo_monto_aporte = parseFloat($('.calculo_monto_aporte').val().replace(",", "."));
    calculo_monto_aporte = (isNaN(calculo_monto_aporte)) ? 0 : calculo_monto_aporte;
    $('.calculo_monto_aporte').val(calculo_monto_aporte.toLocaleString().replace(".", ""));
    //calculo_monto_aporte    = parseFloat($('.calculo_monto_aporte').val());
    total = calculo_monto_honorario + calculo_monto_iva + calculo_monto_aporte;
    $('.calculo_total').val(total.toLocaleString().replace(".", ""));
}
$(".calculo_monto_honorario").change(function() {
    calcularTotal();
});
$(".calculo_monto_iva").change(function() {
    calcularTotal();
});
$(".calculo_monto_aporte").change(function() {
    calcularTotal();
});