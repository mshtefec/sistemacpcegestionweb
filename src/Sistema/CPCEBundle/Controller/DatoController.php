<?php

namespace Sistema\CPCEBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Symfony\Component\Yaml\Yaml;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\CPCEBundle\Entity\Afiliado;
use Sistema\CPCEBundle\Form\DatosType;
use Knp\Snappy\Pdf;

/**
 * Afiliado controller.
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 *
 * @Route("/matriculado/datos")
 */
class DatoController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/CPCEBundle/Resources/config/AfiliadoDato.yml',
    );

    protected function getConfig(){
        $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir().'/../src/'.$this->config['yml']));
        foreach ($configs as $key => $value) {
            $config[$key] = $value;
        }
        foreach ($this->config as $key => $value) {
            if ($key != 'yml') {
                $config[$key] = $value;
            }
        }
        return $config;
    }

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
            ->createQueryBuilder('a')
        ;

        return $queryBuilder;
    }

    /**
     * Finds and displays a Afiliado entity.
     *
     * @Route("/", name="front_dato")
     * @Method("GET")
     * @Template()
     */
    public function datosAction()
    {
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $entity = $em->getRepository($config['repository'])
            ->findAfiliadoCategoriaDescByDoc($user->getTipdoc(), $user->getNrodoc());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        
        return array(
            'config' => $config,
            'entity' => $entity,
            'user'   => $user,
        );
    }

    /**
     * Displays a form to edit an existing Afiliado entity.
     *
     * @Route("/editar", name="front_dato_editar")
     * @Method("GET")
     * @Template("SistemaCPCEBundle:Dato:edit.html.twig")
     */
    public function editarAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userId = $this->getUser()->getNrodoc();

        $entity = $em->getRepository('SistemaCPCEBundle:Afiliado')->find($userId);
            
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '. 'Afiliado' .' entity.');
        }
        
        $form = $this->createForm(new DatosType(), $entity, array(
            'action' => $this->generateUrl('front_dato_editar'),
            'method' => 'GET',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.save',
                'attr'               => array(
                    'class' => 'form-control btn-success',
                    'col'   => 'col-lg-2',
                )
            ))
        ;

        $request = $this->getRequest();
        $form->handleRequest($request);
        
        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Datos Actualizados');

            return $this->redirect($this->generateUrl('front_dato'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and generate PDF a Dato entity.
     *
     * @Route("/pdf", name="front_dato_pdf")
     * @Method("GET")
     * @Template()
     */
    public function datosPdfAction()
    {
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $entity = $em->getRepository($config['repository'])
            ->findAfiliadoCategoriaDescByDoc($user->getTipdoc(), $user->getNrodoc());

        $fecha = new \Datetime('today');

        $html = $this->renderView('SistemaCPCEBundle:Dato:datosPdf.html.twig', array(
            'fecha'  => $fecha,
            'entity' => $entity[0],
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml(
                $html,
                array(
                    'images'                => true,
                    'enable-external-links' => true
                )
            ),
            200,
            array(
                'images'                => true,
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="matriculado_nro_' . $entity[0]->getAfiMatricula() . '.pdf"'
            )
        );
    }
}