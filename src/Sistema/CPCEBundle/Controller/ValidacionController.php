<?php

namespace Sistema\CPCEBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sistema\CPCEBundle\Entity\Afiliado;
use Sistema\CPCEBundle\Form\ValidacionType;


/**
 * Validacion controller.
 * @author Max Shtefec <max.shtefec@gmail.com>
 *
 * @Route("/validacion")
 */
class ValidacionController extends Controller
{
    /**
     * @Route("/", name="validacion_index")
     * @Template()
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        $entity = new Afiliado;
        
        $form = $this->createForm(new ValidacionType(), $entity, array(
		    'action' => $this->generateUrl('validacion_index'),
		    'method' => 'GET',
		));

        $form
            ->add('validar', 'submit', array(
                'label' => 'Verificar',
                'attr'  => array(
                    'class' => 'form-control btn-info',
                )
            ))
        ;

        $form->handleRequest($request);

        if ($form->isValid()) {
        	$titulo = $entity->getAfiTitulo();
        	$matricula = $entity->getAfiMatricula();
        	
        	$em = $this->getDoctrine()->getManager();
        	$matriculado = $em->getRepository('SistemaCPCEBundle:Afiliado')->findAfiliadoByMatricula($titulo, $matricula);

            $categorias = '110101 110102 110103 110104 110105 120000 120101 120102 120103 120104 170000 170100 170101';
            
            if (!$matriculado) {
            	$this->get('session')->getFlashBag()->add('error', 'Matriculado No Registrado');
        	} else {
                if (strpos($categorias, $matriculado['categoria']) !== false) {
                    //ladybug_dump_die("llega");
                    $this->get('session')->getFlashBag()->add('success', 'Matriculado Activo');
                } else {
                    //ladybug_dump_die("no llega");
                    $this->get('session')->getFlashBag()->add('error', 'Matriculado No Activo');
                }
        	}
        }

        return array(
            'form'   => $form->createView(),
        );
    }
}