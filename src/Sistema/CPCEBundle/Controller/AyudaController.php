<?php

namespace Sistema\CPCEBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\BackBundle\Entity\Archivo;


/**
 * Ayuda controller.
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 *
 * @Route("/matriculado")
 */
class AyudaController extends Controller
{
    /**
     * @Route("/ayuda", name="ayuda_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/novedades", name="ayuda_novedad_index")
     * @Template()
     */
    public function novedadIndexAction()
    {
        return array();
    }

    /**
     * @Route("/servicios/{seccion}", name="ayuda_servicios_index")
     * @Method("GET")
     * @Template()
     */
    public function serviciosIndexAction($seccion = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaBackBundle:Archivo')->getArchivoBySeccion($seccion);
        
        return array(
            'archivo' => $entity,
        );
    }

    /**
     * get a archivo pdf.
     *
     * @Route("/get-archivo/{archivoId}", name="getArchivo")
     * @Method("GET")
     */
    public function getArchivo($archivoId)
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('SistemaBackBundle:Archivo')->find($archivoId);

        $response = new Response();

        $response->headers->set('Content-type', mime_content_type($entity->getWebPath()));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $entity->getFilePath() . '";');

        $response->setContent(file_get_contents($entity->getWebPath()));

        return $response;
    }
}