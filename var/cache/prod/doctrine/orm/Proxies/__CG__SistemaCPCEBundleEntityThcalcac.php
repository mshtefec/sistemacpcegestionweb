<?php

namespace Proxies\__CG__\Sistema\CPCEBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Thcalcac extends \Sistema\CPCEBundle\Entity\Thcalcac implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcFijo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcAdicporc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcExcedente', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcMondes', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcMonhas'];
        }

        return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcFijo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcAdicporc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcExcedente', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcMondes', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcalcac' . "\0" . 'thcMonhas'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Thcalcac $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setThcFijo($thcFijo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThcFijo', [$thcFijo]);

        return parent::setThcFijo($thcFijo);
    }

    /**
     * {@inheritDoc}
     */
    public function getThcFijo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThcFijo', []);

        return parent::getThcFijo();
    }

    /**
     * {@inheritDoc}
     */
    public function setThcAdicporc($thcAdicporc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThcAdicporc', [$thcAdicporc]);

        return parent::setThcAdicporc($thcAdicporc);
    }

    /**
     * {@inheritDoc}
     */
    public function getThcAdicporc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThcAdicporc', []);

        return parent::getThcAdicporc();
    }

    /**
     * {@inheritDoc}
     */
    public function setThcExcedente($thcExcedente)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThcExcedente', [$thcExcedente]);

        return parent::setThcExcedente($thcExcedente);
    }

    /**
     * {@inheritDoc}
     */
    public function getThcExcedente()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThcExcedente', []);

        return parent::getThcExcedente();
    }

    /**
     * {@inheritDoc}
     */
    public function setThcMondes($thcMondes)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThcMondes', [$thcMondes]);

        return parent::setThcMondes($thcMondes);
    }

    /**
     * {@inheritDoc}
     */
    public function getThcMondes()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getThcMondes();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThcMondes', []);

        return parent::getThcMondes();
    }

    /**
     * {@inheritDoc}
     */
    public function setThcMonhas($thcMonhas)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThcMonhas', [$thcMonhas]);

        return parent::setThcMonhas($thcMonhas);
    }

    /**
     * {@inheritDoc}
     */
    public function getThcMonhas()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getThcMonhas();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThcMonhas', []);

        return parent::getThcMonhas();
    }

}
