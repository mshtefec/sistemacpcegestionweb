<?php

namespace Proxies\__CG__\Sistema\CPCEBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Chequera extends \Sistema\CPCEBundle\Entity\Chequera implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaNrocli', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaTipbco', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaNrobco', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaTipo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaFecha', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaSerie', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaCheini', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaChefin', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaLote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaCuebco', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaNrotal'];
        }

        return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaNrocli', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaTipbco', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaNrobco', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaTipo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaFecha', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaSerie', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaCheini', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaChefin', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaLote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaCuebco', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Chequera' . "\0" . 'chaNrotal'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Chequera $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setChaNrocli($chaNrocli)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaNrocli', [$chaNrocli]);

        return parent::setChaNrocli($chaNrocli);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaNrocli()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaNrocli', []);

        return parent::getChaNrocli();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaTipbco($chaTipbco)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaTipbco', [$chaTipbco]);

        return parent::setChaTipbco($chaTipbco);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaTipbco()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaTipbco', []);

        return parent::getChaTipbco();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaNrobco($chaNrobco)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaNrobco', [$chaNrobco]);

        return parent::setChaNrobco($chaNrobco);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaNrobco()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaNrobco', []);

        return parent::getChaNrobco();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaTipo($chaTipo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaTipo', [$chaTipo]);

        return parent::setChaTipo($chaTipo);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaTipo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaTipo', []);

        return parent::getChaTipo();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaFecha($chaFecha)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaFecha', [$chaFecha]);

        return parent::setChaFecha($chaFecha);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaFecha()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaFecha', []);

        return parent::getChaFecha();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaSerie($chaSerie)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaSerie', [$chaSerie]);

        return parent::setChaSerie($chaSerie);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaSerie()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaSerie', []);

        return parent::getChaSerie();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaCheini($chaCheini)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaCheini', [$chaCheini]);

        return parent::setChaCheini($chaCheini);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaCheini()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaCheini', []);

        return parent::getChaCheini();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaChefin($chaChefin)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaChefin', [$chaChefin]);

        return parent::setChaChefin($chaChefin);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaChefin()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaChefin', []);

        return parent::getChaChefin();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaLote($chaLote)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaLote', [$chaLote]);

        return parent::setChaLote($chaLote);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaLote()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaLote', []);

        return parent::getChaLote();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaCuebco($chaCuebco)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaCuebco', [$chaCuebco]);

        return parent::setChaCuebco($chaCuebco);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaCuebco()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getChaCuebco();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaCuebco', []);

        return parent::getChaCuebco();
    }

    /**
     * {@inheritDoc}
     */
    public function setChaNrotal($chaNrotal)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChaNrotal', [$chaNrotal]);

        return parent::setChaNrotal($chaNrotal);
    }

    /**
     * {@inheritDoc}
     */
    public function getChaNrotal()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getChaNrotal();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChaNrotal', []);

        return parent::getChaNrotal();
    }

}
