<?php

namespace Proxies\__CG__\Sistema\CPCEBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Farcat extends \Sistema\CPCEBundle\Entity\Farcat implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farActivo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNrofar', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNombre', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farSucursal', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farControlador', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farServer', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farUsuario', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farPassword', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBasedatos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTipred', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farPropia', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farMargen', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farRaiz', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farLugbak', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farLote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farFecha', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farHora', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farEstado', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farErrore', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farCodpos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farComent', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTipoiva', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farPorceiva', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNrocui', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farDgr', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farGan', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farProvin', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farLocali', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farDirecc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTelefo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTope', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farCancuo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farAtrazo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farImpres', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farImcola', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farIntere', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBonpe1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBonpe2', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBonpe3', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTipimp', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farInventario', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farConnect', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farIdmsjimed', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farConvdefault', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farUnegos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNrocli'];
        }

        return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farActivo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNrofar', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNombre', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farSucursal', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farControlador', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farServer', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farUsuario', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farPassword', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBasedatos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTipred', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farPropia', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farMargen', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farRaiz', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farLugbak', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farLote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farFecha', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farHora', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farEstado', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farErrore', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farCodpos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farComent', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTipoiva', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farPorceiva', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNrocui', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farDgr', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farGan', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farProvin', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farLocali', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farDirecc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTelefo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTope', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farCancuo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farAtrazo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farImpres', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farImcola', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farIntere', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBonpe1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBonpe2', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farBonpe3', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farTipimp', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farInventario', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farConnect', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farIdmsjimed', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farConvdefault', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farUnegos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Farcat' . "\0" . 'farNrocli'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Farcat $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setFarActivo($farActivo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarActivo', [$farActivo]);

        return parent::setFarActivo($farActivo);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarActivo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarActivo', []);

        return parent::getFarActivo();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarNrofar($farNrofar)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarNrofar', [$farNrofar]);

        return parent::setFarNrofar($farNrofar);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarNrofar()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarNrofar', []);

        return parent::getFarNrofar();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarNombre($farNombre)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarNombre', [$farNombre]);

        return parent::setFarNombre($farNombre);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarNombre()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarNombre', []);

        return parent::getFarNombre();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarSucursal($farSucursal)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarSucursal', [$farSucursal]);

        return parent::setFarSucursal($farSucursal);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarSucursal()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarSucursal', []);

        return parent::getFarSucursal();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarControlador($farControlador)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarControlador', [$farControlador]);

        return parent::setFarControlador($farControlador);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarControlador()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarControlador', []);

        return parent::getFarControlador();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarServer($farServer)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarServer', [$farServer]);

        return parent::setFarServer($farServer);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarServer()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarServer', []);

        return parent::getFarServer();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarUsuario($farUsuario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarUsuario', [$farUsuario]);

        return parent::setFarUsuario($farUsuario);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarUsuario()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarUsuario', []);

        return parent::getFarUsuario();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarPassword($farPassword)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarPassword', [$farPassword]);

        return parent::setFarPassword($farPassword);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarPassword', []);

        return parent::getFarPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarBasedatos($farBasedatos)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarBasedatos', [$farBasedatos]);

        return parent::setFarBasedatos($farBasedatos);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarBasedatos()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarBasedatos', []);

        return parent::getFarBasedatos();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarTipred($farTipred)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarTipred', [$farTipred]);

        return parent::setFarTipred($farTipred);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarTipred()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarTipred', []);

        return parent::getFarTipred();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarPropia($farPropia)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarPropia', [$farPropia]);

        return parent::setFarPropia($farPropia);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarPropia()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarPropia', []);

        return parent::getFarPropia();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarMargen($farMargen)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarMargen', [$farMargen]);

        return parent::setFarMargen($farMargen);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarMargen()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarMargen', []);

        return parent::getFarMargen();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarRaiz($farRaiz)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarRaiz', [$farRaiz]);

        return parent::setFarRaiz($farRaiz);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarRaiz()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarRaiz', []);

        return parent::getFarRaiz();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarLugbak($farLugbak)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarLugbak', [$farLugbak]);

        return parent::setFarLugbak($farLugbak);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarLugbak()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarLugbak', []);

        return parent::getFarLugbak();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarLote($farLote)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarLote', [$farLote]);

        return parent::setFarLote($farLote);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarLote()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarLote', []);

        return parent::getFarLote();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarFecha($farFecha)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarFecha', [$farFecha]);

        return parent::setFarFecha($farFecha);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarFecha()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarFecha', []);

        return parent::getFarFecha();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarHora($farHora)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarHora', [$farHora]);

        return parent::setFarHora($farHora);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarHora()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarHora', []);

        return parent::getFarHora();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarEstado($farEstado)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarEstado', [$farEstado]);

        return parent::setFarEstado($farEstado);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarEstado()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarEstado', []);

        return parent::getFarEstado();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarErrore($farErrore)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarErrore', [$farErrore]);

        return parent::setFarErrore($farErrore);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarErrore()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarErrore', []);

        return parent::getFarErrore();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarCodpos($farCodpos)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarCodpos', [$farCodpos]);

        return parent::setFarCodpos($farCodpos);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarCodpos()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarCodpos', []);

        return parent::getFarCodpos();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarComent($farComent)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarComent', [$farComent]);

        return parent::setFarComent($farComent);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarComent()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarComent', []);

        return parent::getFarComent();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarTipoiva($farTipoiva)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarTipoiva', [$farTipoiva]);

        return parent::setFarTipoiva($farTipoiva);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarTipoiva()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarTipoiva', []);

        return parent::getFarTipoiva();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarPorceiva($farPorceiva)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarPorceiva', [$farPorceiva]);

        return parent::setFarPorceiva($farPorceiva);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarPorceiva()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarPorceiva', []);

        return parent::getFarPorceiva();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarNrocui($farNrocui)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarNrocui', [$farNrocui]);

        return parent::setFarNrocui($farNrocui);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarNrocui()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarNrocui', []);

        return parent::getFarNrocui();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarDgr($farDgr)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarDgr', [$farDgr]);

        return parent::setFarDgr($farDgr);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarDgr()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarDgr', []);

        return parent::getFarDgr();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarGan($farGan)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarGan', [$farGan]);

        return parent::setFarGan($farGan);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarGan()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarGan', []);

        return parent::getFarGan();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarProvin($farProvin)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarProvin', [$farProvin]);

        return parent::setFarProvin($farProvin);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarProvin()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarProvin', []);

        return parent::getFarProvin();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarLocali($farLocali)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarLocali', [$farLocali]);

        return parent::setFarLocali($farLocali);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarLocali()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarLocali', []);

        return parent::getFarLocali();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarDirecc($farDirecc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarDirecc', [$farDirecc]);

        return parent::setFarDirecc($farDirecc);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarDirecc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarDirecc', []);

        return parent::getFarDirecc();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarTelefo($farTelefo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarTelefo', [$farTelefo]);

        return parent::setFarTelefo($farTelefo);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarTelefo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarTelefo', []);

        return parent::getFarTelefo();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarTope($farTope)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarTope', [$farTope]);

        return parent::setFarTope($farTope);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarTope()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarTope', []);

        return parent::getFarTope();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarCancuo($farCancuo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarCancuo', [$farCancuo]);

        return parent::setFarCancuo($farCancuo);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarCancuo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarCancuo', []);

        return parent::getFarCancuo();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarAtrazo($farAtrazo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarAtrazo', [$farAtrazo]);

        return parent::setFarAtrazo($farAtrazo);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarAtrazo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarAtrazo', []);

        return parent::getFarAtrazo();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarImpres($farImpres)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarImpres', [$farImpres]);

        return parent::setFarImpres($farImpres);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarImpres()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarImpres', []);

        return parent::getFarImpres();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarImcola($farImcola)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarImcola', [$farImcola]);

        return parent::setFarImcola($farImcola);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarImcola()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarImcola', []);

        return parent::getFarImcola();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarIntere($farIntere)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarIntere', [$farIntere]);

        return parent::setFarIntere($farIntere);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarIntere()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarIntere', []);

        return parent::getFarIntere();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarBonpe1($farBonpe1)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarBonpe1', [$farBonpe1]);

        return parent::setFarBonpe1($farBonpe1);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarBonpe1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarBonpe1', []);

        return parent::getFarBonpe1();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarBonpe2($farBonpe2)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarBonpe2', [$farBonpe2]);

        return parent::setFarBonpe2($farBonpe2);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarBonpe2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarBonpe2', []);

        return parent::getFarBonpe2();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarBonpe3($farBonpe3)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarBonpe3', [$farBonpe3]);

        return parent::setFarBonpe3($farBonpe3);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarBonpe3()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarBonpe3', []);

        return parent::getFarBonpe3();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarTipimp($farTipimp)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarTipimp', [$farTipimp]);

        return parent::setFarTipimp($farTipimp);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarTipimp()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarTipimp', []);

        return parent::getFarTipimp();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarInventario($farInventario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarInventario', [$farInventario]);

        return parent::setFarInventario($farInventario);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarInventario()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarInventario', []);

        return parent::getFarInventario();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarConnect($farConnect)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarConnect', [$farConnect]);

        return parent::setFarConnect($farConnect);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarConnect()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarConnect', []);

        return parent::getFarConnect();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarIdmsjimed($farIdmsjimed)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarIdmsjimed', [$farIdmsjimed]);

        return parent::setFarIdmsjimed($farIdmsjimed);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarIdmsjimed()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarIdmsjimed', []);

        return parent::getFarIdmsjimed();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarConvdefault($farConvdefault)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarConvdefault', [$farConvdefault]);

        return parent::setFarConvdefault($farConvdefault);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarConvdefault()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarConvdefault', []);

        return parent::getFarConvdefault();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarUnegos($farUnegos)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarUnegos', [$farUnegos]);

        return parent::setFarUnegos($farUnegos);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarUnegos()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getFarUnegos();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarUnegos', []);

        return parent::getFarUnegos();
    }

    /**
     * {@inheritDoc}
     */
    public function setFarNrocli($farNrocli)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFarNrocli', [$farNrocli]);

        return parent::setFarNrocli($farNrocli);
    }

    /**
     * {@inheritDoc}
     */
    public function getFarNrocli()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getFarNrocli();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFarNrocli', []);

        return parent::getFarNrocli();
    }

}
