<?php

namespace Proxies\__CG__\Sistema\CPCEBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Comitente extends \Sistema\CPCEBundle\Entity\Comitente implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiNrodoc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiNombre', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiFecnac', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTipo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTitulo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiMatricula', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiDireccion', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiLocalidad', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiProvincia', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiZona', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiCodpos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTelefono1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiCelular', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiMail', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTipoiva', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiCuit', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiDgr', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiLote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTipdoc'];
        }

        return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiNrodoc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiNombre', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiFecnac', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTipo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTitulo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiMatricula', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiDireccion', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiLocalidad', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiProvincia', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiZona', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiCodpos', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTelefono1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiCelular', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiMail', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTipoiva', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiCuit', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiDgr', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiLote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Comitente' . "\0" . 'afiTipdoc'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Comitente $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', []);

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiZonaDelegacion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiZonaDelegacion', []);

        return parent::getAfiZonaDelegacion();
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiSexoString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiSexoString', []);

        return parent::getAfiSexoString();
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiCivilEstado()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiCivilEstado', []);

        return parent::getAfiCivilEstado();
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiGananciasCondicionIva()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiGananciasCondicionIva', []);

        return parent::getAfiGananciasCondicionIva();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiNombre($afiNombre)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiNombre', [$afiNombre]);

        return parent::setAfiNombre($afiNombre);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiNombre()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiNombre', []);

        return parent::getAfiNombre();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiFecnac($afiFecnac)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiFecnac', [$afiFecnac]);

        return parent::setAfiFecnac($afiFecnac);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiFecnac()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiFecnac', []);

        return parent::getAfiFecnac();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiTipo($afiTipo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiTipo', [$afiTipo]);

        return parent::setAfiTipo($afiTipo);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiTipo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiTipo', []);

        return parent::getAfiTipo();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiTitulo($afiTitulo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiTitulo', [$afiTitulo]);

        return parent::setAfiTitulo($afiTitulo);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiTitulo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiTitulo', []);

        return parent::getAfiTitulo();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiMatricula($afiMatricula)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiMatricula', [$afiMatricula]);

        return parent::setAfiMatricula($afiMatricula);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiMatricula()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiMatricula', []);

        return parent::getAfiMatricula();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiDireccion($afiDireccion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiDireccion', [$afiDireccion]);

        return parent::setAfiDireccion($afiDireccion);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiDireccion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiDireccion', []);

        return parent::getAfiDireccion();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiLocalidad($afiLocalidad)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiLocalidad', [$afiLocalidad]);

        return parent::setAfiLocalidad($afiLocalidad);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiLocalidad()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiLocalidad', []);

        return parent::getAfiLocalidad();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiProvincia($afiProvincia)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiProvincia', [$afiProvincia]);

        return parent::setAfiProvincia($afiProvincia);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiProvincia()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiProvincia', []);

        return parent::getAfiProvincia();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiZona($afiZona)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiZona', [$afiZona]);

        return parent::setAfiZona($afiZona);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiZona()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiZona', []);

        return parent::getAfiZona();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiCodpos($afiCodpos)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiCodpos', [$afiCodpos]);

        return parent::setAfiCodpos($afiCodpos);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiCodpos()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiCodpos', []);

        return parent::getAfiCodpos();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiTelefono1($afiTelefono1)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiTelefono1', [$afiTelefono1]);

        return parent::setAfiTelefono1($afiTelefono1);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiTelefono1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiTelefono1', []);

        return parent::getAfiTelefono1();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiCelular($afiCelular)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiCelular', [$afiCelular]);

        return parent::setAfiCelular($afiCelular);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiCelular()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiCelular', []);

        return parent::getAfiCelular();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiMail($afiMail)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiMail', [$afiMail]);

        return parent::setAfiMail($afiMail);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiMail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiMail', []);

        return parent::getAfiMail();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiTipoiva($afiTipoiva)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiTipoiva', [$afiTipoiva]);

        return parent::setAfiTipoiva($afiTipoiva);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiTipoiva()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiTipoiva', []);

        return parent::getAfiTipoiva();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiCuit($afiCuit)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiCuit', [$afiCuit]);

        return parent::setAfiCuit($afiCuit);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiCuit()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiCuit', []);

        return parent::getAfiCuit();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiDgr($afiDgr)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiDgr', [$afiDgr]);

        return parent::setAfiDgr($afiDgr);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiDgr()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiDgr', []);

        return parent::getAfiDgr();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiLote($afiLote)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiLote', [$afiLote]);

        return parent::setAfiLote($afiLote);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiLote()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiLote', []);

        return parent::getAfiLote();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiTipdoc($afiTipdoc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiTipdoc', [$afiTipdoc]);

        return parent::setAfiTipdoc($afiTipdoc);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiTipdoc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiTipdoc', []);

        return parent::getAfiTipdoc();
    }

    /**
     * {@inheritDoc}
     */
    public function setAfiNrodoc($afiNrodoc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAfiNrodoc', [$afiNrodoc]);

        return parent::setAfiNrodoc($afiNrodoc);
    }

    /**
     * {@inheritDoc}
     */
    public function getAfiNrodoc()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getAfiNrodoc();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAfiNrodoc', []);

        return parent::getAfiNrodoc();
    }

}
