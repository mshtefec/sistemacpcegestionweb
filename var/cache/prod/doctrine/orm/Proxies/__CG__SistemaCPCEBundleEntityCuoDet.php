<?php

namespace Proxies\__CG__\Sistema\CPCEBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class CuoDet extends \Sistema\CPCEBundle\Entity\CuoDet implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'tipo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'total', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'd1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'd2', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'd3', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c2', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c3', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c4', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c5', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c6', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c7', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c8', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'fAlta', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'fVto', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'fPago', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'asiento', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'recibo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'numero', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'ncuota', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'matricula'];
        }

        return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'tipo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'total', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'd1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'd2', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'd3', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c1', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c2', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c3', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c4', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c5', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c6', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c7', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'c8', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'fAlta', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'fVto', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'fPago', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'asiento', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'recibo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'numero', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'ncuota', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\CuoDet' . "\0" . 'matricula'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (CuoDet $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setTipo($tipo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTipo', [$tipo]);

        return parent::setTipo($tipo);
    }

    /**
     * {@inheritDoc}
     */
    public function getTipo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTipo', []);

        return parent::getTipo();
    }

    /**
     * {@inheritDoc}
     */
    public function setTotal($total)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTotal', [$total]);

        return parent::setTotal($total);
    }

    /**
     * {@inheritDoc}
     */
    public function getTotal()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTotal', []);

        return parent::getTotal();
    }

    /**
     * {@inheritDoc}
     */
    public function setD1($d1)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setD1', [$d1]);

        return parent::setD1($d1);
    }

    /**
     * {@inheritDoc}
     */
    public function getD1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getD1', []);

        return parent::getD1();
    }

    /**
     * {@inheritDoc}
     */
    public function setD2($d2)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setD2', [$d2]);

        return parent::setD2($d2);
    }

    /**
     * {@inheritDoc}
     */
    public function getD2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getD2', []);

        return parent::getD2();
    }

    /**
     * {@inheritDoc}
     */
    public function setD3($d3)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setD3', [$d3]);

        return parent::setD3($d3);
    }

    /**
     * {@inheritDoc}
     */
    public function getD3()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getD3', []);

        return parent::getD3();
    }

    /**
     * {@inheritDoc}
     */
    public function setC1($c1)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC1', [$c1]);

        return parent::setC1($c1);
    }

    /**
     * {@inheritDoc}
     */
    public function getC1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC1', []);

        return parent::getC1();
    }

    /**
     * {@inheritDoc}
     */
    public function setC2($c2)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC2', [$c2]);

        return parent::setC2($c2);
    }

    /**
     * {@inheritDoc}
     */
    public function getC2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC2', []);

        return parent::getC2();
    }

    /**
     * {@inheritDoc}
     */
    public function setC3($c3)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC3', [$c3]);

        return parent::setC3($c3);
    }

    /**
     * {@inheritDoc}
     */
    public function getC3()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC3', []);

        return parent::getC3();
    }

    /**
     * {@inheritDoc}
     */
    public function setC4($c4)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC4', [$c4]);

        return parent::setC4($c4);
    }

    /**
     * {@inheritDoc}
     */
    public function getC4()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC4', []);

        return parent::getC4();
    }

    /**
     * {@inheritDoc}
     */
    public function setC5($c5)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC5', [$c5]);

        return parent::setC5($c5);
    }

    /**
     * {@inheritDoc}
     */
    public function getC5()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC5', []);

        return parent::getC5();
    }

    /**
     * {@inheritDoc}
     */
    public function setC6($c6)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC6', [$c6]);

        return parent::setC6($c6);
    }

    /**
     * {@inheritDoc}
     */
    public function getC6()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC6', []);

        return parent::getC6();
    }

    /**
     * {@inheritDoc}
     */
    public function setC7($c7)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC7', [$c7]);

        return parent::setC7($c7);
    }

    /**
     * {@inheritDoc}
     */
    public function getC7()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC7', []);

        return parent::getC7();
    }

    /**
     * {@inheritDoc}
     */
    public function setC8($c8)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setC8', [$c8]);

        return parent::setC8($c8);
    }

    /**
     * {@inheritDoc}
     */
    public function getC8()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getC8', []);

        return parent::getC8();
    }

    /**
     * {@inheritDoc}
     */
    public function setFAlta($fAlta)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFAlta', [$fAlta]);

        return parent::setFAlta($fAlta);
    }

    /**
     * {@inheritDoc}
     */
    public function getFAlta()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFAlta', []);

        return parent::getFAlta();
    }

    /**
     * {@inheritDoc}
     */
    public function setFVto($fVto)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFVto', [$fVto]);

        return parent::setFVto($fVto);
    }

    /**
     * {@inheritDoc}
     */
    public function getFVto()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFVto', []);

        return parent::getFVto();
    }

    /**
     * {@inheritDoc}
     */
    public function setFPago($fPago)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFPago', [$fPago]);

        return parent::setFPago($fPago);
    }

    /**
     * {@inheritDoc}
     */
    public function getFPago()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFPago', []);

        return parent::getFPago();
    }

    /**
     * {@inheritDoc}
     */
    public function setAsiento($asiento)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAsiento', [$asiento]);

        return parent::setAsiento($asiento);
    }

    /**
     * {@inheritDoc}
     */
    public function getAsiento()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAsiento', []);

        return parent::getAsiento();
    }

    /**
     * {@inheritDoc}
     */
    public function setRecibo($recibo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRecibo', [$recibo]);

        return parent::setRecibo($recibo);
    }

    /**
     * {@inheritDoc}
     */
    public function getRecibo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRecibo', []);

        return parent::getRecibo();
    }

    /**
     * {@inheritDoc}
     */
    public function setNumero($numero)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNumero', [$numero]);

        return parent::setNumero($numero);
    }

    /**
     * {@inheritDoc}
     */
    public function getNumero()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getNumero();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNumero', []);

        return parent::getNumero();
    }

    /**
     * {@inheritDoc}
     */
    public function setNcuota($ncuota)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNcuota', [$ncuota]);

        return parent::setNcuota($ncuota);
    }

    /**
     * {@inheritDoc}
     */
    public function getNcuota()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getNcuota();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNcuota', []);

        return parent::getNcuota();
    }

    /**
     * {@inheritDoc}
     */
    public function setMatricula($matricula)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMatricula', [$matricula]);

        return parent::setMatricula($matricula);
    }

    /**
     * {@inheritDoc}
     */
    public function getMatricula()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getMatricula();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMatricula', []);

        return parent::getMatricula();
    }

}
