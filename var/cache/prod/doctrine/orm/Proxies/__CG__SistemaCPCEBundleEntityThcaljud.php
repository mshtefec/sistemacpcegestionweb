<?php

namespace Proxies\__CG__\Sistema\CPCEBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Thcaljud extends \Sistema\CPCEBundle\Entity\Thcaljud implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjFijo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjAdicporc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjExcedente', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjMondes', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjMonhas'];
        }

        return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjFijo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjAdicporc', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjExcedente', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjMondes', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Thcaljud' . "\0" . 'thjMonhas'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Thcaljud $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setThjFijo($thjFijo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThjFijo', [$thjFijo]);

        return parent::setThjFijo($thjFijo);
    }

    /**
     * {@inheritDoc}
     */
    public function getThjFijo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThjFijo', []);

        return parent::getThjFijo();
    }

    /**
     * {@inheritDoc}
     */
    public function setThjAdicporc($thjAdicporc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThjAdicporc', [$thjAdicporc]);

        return parent::setThjAdicporc($thjAdicporc);
    }

    /**
     * {@inheritDoc}
     */
    public function getThjAdicporc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThjAdicporc', []);

        return parent::getThjAdicporc();
    }

    /**
     * {@inheritDoc}
     */
    public function setThjExcedente($thjExcedente)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThjExcedente', [$thjExcedente]);

        return parent::setThjExcedente($thjExcedente);
    }

    /**
     * {@inheritDoc}
     */
    public function getThjExcedente()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThjExcedente', []);

        return parent::getThjExcedente();
    }

    /**
     * {@inheritDoc}
     */
    public function setThjMondes($thjMondes)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThjMondes', [$thjMondes]);

        return parent::setThjMondes($thjMondes);
    }

    /**
     * {@inheritDoc}
     */
    public function getThjMondes()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getThjMondes();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThjMondes', []);

        return parent::getThjMondes();
    }

    /**
     * {@inheritDoc}
     */
    public function setThjMonhas($thjMonhas)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThjMonhas', [$thjMonhas]);

        return parent::setThjMonhas($thjMonhas);
    }

    /**
     * {@inheritDoc}
     */
    public function getThjMonhas()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getThjMonhas();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThjMonhas', []);

        return parent::getThjMonhas();
    }

}
