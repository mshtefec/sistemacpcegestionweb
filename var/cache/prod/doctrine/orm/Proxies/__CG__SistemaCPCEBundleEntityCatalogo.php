<?php

namespace Proxies\__CG__\Sistema\CPCEBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Catalogo extends \Sistema\CPCEBundle\Entity\Catalogo implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catIndice', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catUltsucursal', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catUltlote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catVaiven', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catCampofar', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catFiltro', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catOrden', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catBloqueo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catTabla'];
        }

        return ['__isInitialized__', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catIndice', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catUltsucursal', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catUltlote', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catVaiven', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catCampofar', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catFiltro', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catOrden', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catBloqueo', '' . "\0" . 'Sistema\\CPCEBundle\\Entity\\Catalogo' . "\0" . 'catTabla'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Catalogo $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setCatIndice($catIndice)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatIndice', [$catIndice]);

        return parent::setCatIndice($catIndice);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatIndice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatIndice', []);

        return parent::getCatIndice();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatUltsucursal($catUltsucursal)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatUltsucursal', [$catUltsucursal]);

        return parent::setCatUltsucursal($catUltsucursal);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatUltsucursal()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatUltsucursal', []);

        return parent::getCatUltsucursal();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatUltlote($catUltlote)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatUltlote', [$catUltlote]);

        return parent::setCatUltlote($catUltlote);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatUltlote()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatUltlote', []);

        return parent::getCatUltlote();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatVaiven($catVaiven)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatVaiven', [$catVaiven]);

        return parent::setCatVaiven($catVaiven);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatVaiven()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatVaiven', []);

        return parent::getCatVaiven();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatCampofar($catCampofar)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatCampofar', [$catCampofar]);

        return parent::setCatCampofar($catCampofar);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatCampofar()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatCampofar', []);

        return parent::getCatCampofar();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatFiltro($catFiltro)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatFiltro', [$catFiltro]);

        return parent::setCatFiltro($catFiltro);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatFiltro()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatFiltro', []);

        return parent::getCatFiltro();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatOrden($catOrden)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatOrden', [$catOrden]);

        return parent::setCatOrden($catOrden);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatOrden()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatOrden', []);

        return parent::getCatOrden();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatBloqueo($catBloqueo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatBloqueo', [$catBloqueo]);

        return parent::setCatBloqueo($catBloqueo);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatBloqueo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatBloqueo', []);

        return parent::getCatBloqueo();
    }

    /**
     * {@inheritDoc}
     */
    public function getCatTabla()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getCatTabla();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatTabla', []);

        return parent::getCatTabla();
    }

}
