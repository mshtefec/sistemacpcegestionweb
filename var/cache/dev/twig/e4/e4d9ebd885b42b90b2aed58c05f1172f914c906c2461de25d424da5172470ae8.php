<?php

/* SistemaCPCEBundle::longLayout.html.twig */
class __TwigTemplate_a51821611cd4c12057e1b157ba7b86985d470a84aeb5270773b860c14adf60fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaCPCEBundle::layout.html.twig", "SistemaCPCEBundle::longLayout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaCPCEBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1997e43a0bfcf60062c2a17abebf1d35d37fe89de4d946eec10f1bee875b7029 = $this->env->getExtension("native_profiler");
        $__internal_1997e43a0bfcf60062c2a17abebf1d35d37fe89de4d946eec10f1bee875b7029->enter($__internal_1997e43a0bfcf60062c2a17abebf1d35d37fe89de4d946eec10f1bee875b7029_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle::longLayout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1997e43a0bfcf60062c2a17abebf1d35d37fe89de4d946eec10f1bee875b7029->leave($__internal_1997e43a0bfcf60062c2a17abebf1d35d37fe89de4d946eec10f1bee875b7029_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f1cf5ac074d09d7ad1e6f0e3f1ccb632a9a2c86b3444f1aec6c058713f359b7c = $this->env->getExtension("native_profiler");
        $__internal_f1cf5ac074d09d7ad1e6f0e3f1ccb632a9a2c86b3444f1aec6c058713f359b7c->enter($__internal_f1cf5ac074d09d7ad1e6f0e3f1ccb632a9a2c86b3444f1aec6c058713f359b7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style type=\"text/css\">
\t\t@media (min-width: 769px) {
\t\t    .content {
\t\t        margin: 0 3em;
\t\t    }
\t\t}
    </style>
";
        
        $__internal_f1cf5ac074d09d7ad1e6f0e3f1ccb632a9a2c86b3444f1aec6c058713f359b7c->leave($__internal_f1cf5ac074d09d7ad1e6f0e3f1ccb632a9a2c86b3444f1aec6c058713f359b7c_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle::longLayout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'SistemaCPCEBundle::layout.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <style type="text/css">*/
/* 		@media (min-width: 769px) {*/
/* 		    .content {*/
/* 		        margin: 0 3em;*/
/* 		    }*/
/* 		}*/
/*     </style>*/
/* {% endblock %}*/
