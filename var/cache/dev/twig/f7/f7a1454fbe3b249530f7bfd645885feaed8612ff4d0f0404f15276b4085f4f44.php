<?php

/* MWSimpleAdminCrudBundle:widget:fields.html.twig */
class __TwigTemplate_035c9b6239f3e314b95b441e7497117794f1fe4f0618b97bc1aa5c3c9e26f2be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "MWSimpleAdminCrudBundle:widget:fields.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'textarea_widget' => array($this, 'block_textarea_widget'),
                'button_widget' => array($this, 'block_button_widget'),
                'money_widget' => array($this, 'block_money_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'mwspeso_widget' => array($this, 'block_mwspeso_widget'),
                'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'form_label' => array($this, 'block_form_label'),
                'choice_label' => array($this, 'block_choice_label'),
                'checkbox_label' => array($this, 'block_checkbox_label'),
                'radio_label' => array($this, 'block_radio_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'choice_row' => array($this, 'block_choice_row'),
                'date_row' => array($this, 'block_date_row'),
                'time_row' => array($this, 'block_time_row'),
                'datetime_row' => array($this, 'block_datetime_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'form_errors' => array($this, 'block_form_errors'),
                'submit_row' => array($this, 'block_submit_row'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1cc84c3ada0c3629eba9f7251e0777128d62ff43e2150cfce3dc2116b52ebe6b = $this->env->getExtension("native_profiler");
        $__internal_1cc84c3ada0c3629eba9f7251e0777128d62ff43e2150cfce3dc2116b52ebe6b->enter($__internal_1cc84c3ada0c3629eba9f7251e0777128d62ff43e2150cfce3dc2116b52ebe6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle:widget:fields.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('button_widget', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('money_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 55
        echo "
";
        // line 56
        $this->displayBlock('date_widget', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('time_widget', $context, $blocks);
        // line 90
        $this->displayBlock('mwspeso_widget', $context, $blocks);
        // line 99
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 103
        echo "
";
        // line 104
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 125
        echo "
";
        // line 126
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 137
        echo "
";
        // line 138
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 148
        echo "
";
        // line 150
        echo "
";
        // line 151
        $this->displayBlock('form_label', $context, $blocks);
        // line 155
        echo "
";
        // line 156
        $this->displayBlock('choice_label', $context, $blocks);
        // line 161
        echo "
";
        // line 162
        $this->displayBlock('checkbox_label', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('radio_label', $context, $blocks);
        // line 169
        echo "
";
        // line 170
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 194
        echo "
";
        // line 196
        echo "
";
        // line 197
        $this->displayBlock('form_row', $context, $blocks);
        // line 204
        echo "
";
        // line 205
        $this->displayBlock('button_row', $context, $blocks);
        // line 210
        echo "
";
        // line 211
        $this->displayBlock('choice_row', $context, $blocks);
        // line 215
        echo "
";
        // line 216
        $this->displayBlock('date_row', $context, $blocks);
        // line 220
        echo "
";
        // line 221
        $this->displayBlock('time_row', $context, $blocks);
        // line 225
        echo "
";
        // line 226
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 230
        echo "
";
        // line 231
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 237
        echo "
";
        // line 238
        $this->displayBlock('radio_row', $context, $blocks);
        // line 244
        echo "
";
        // line 246
        echo "
";
        // line 247
        $this->displayBlock('form_errors', $context, $blocks);
        // line 258
        echo "
";
        // line 260
        echo "
";
        // line 261
        $this->displayBlock('submit_row', $context, $blocks);
        
        $__internal_1cc84c3ada0c3629eba9f7251e0777128d62ff43e2150cfce3dc2116b52ebe6b->leave($__internal_1cc84c3ada0c3629eba9f7251e0777128d62ff43e2150cfce3dc2116b52ebe6b_prof);

    }

    // line 5
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_d8c90d87152d7575ece8f2b0592568b2b79b66ef3413008d12eb8ee3d67d4a8c = $this->env->getExtension("native_profiler");
        $__internal_d8c90d87152d7575ece8f2b0592568b2b79b66ef3413008d12eb8ee3d67d4a8c->enter($__internal_d8c90d87152d7575ece8f2b0592568b2b79b66ef3413008d12eb8ee3d67d4a8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 6
        if (( !array_key_exists("type", $context) || ("file" != (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type"))))) {
            // line 7
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        }
        // line 9
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_d8c90d87152d7575ece8f2b0592568b2b79b66ef3413008d12eb8ee3d67d4a8c->leave($__internal_d8c90d87152d7575ece8f2b0592568b2b79b66ef3413008d12eb8ee3d67d4a8c_prof);

    }

    // line 12
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_e9604f24e69a9cb51599f00c16885ae66d10df60f3c824d1167e2bc4f89ffc1c = $this->env->getExtension("native_profiler");
        $__internal_e9604f24e69a9cb51599f00c16885ae66d10df60f3c824d1167e2bc4f89ffc1c->enter($__internal_e9604f24e69a9cb51599f00c16885ae66d10df60f3c824d1167e2bc4f89ffc1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 13
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        // line 14
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_e9604f24e69a9cb51599f00c16885ae66d10df60f3c824d1167e2bc4f89ffc1c->leave($__internal_e9604f24e69a9cb51599f00c16885ae66d10df60f3c824d1167e2bc4f89ffc1c_prof);

    }

    // line 17
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_b9749b99312e6e27316e5eb670d2ba3580949ec4b6ed5607ef26b8bd64fb3ae4 = $this->env->getExtension("native_profiler");
        $__internal_b9749b99312e6e27316e5eb670d2ba3580949ec4b6ed5607ef26b8bd64fb3ae4->enter($__internal_b9749b99312e6e27316e5eb670d2ba3580949ec4b6ed5607ef26b8bd64fb3ae4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 18
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "btn-default")) : ("btn-default")) . " btn"))));
        // line 19
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_b9749b99312e6e27316e5eb670d2ba3580949ec4b6ed5607ef26b8bd64fb3ae4->leave($__internal_b9749b99312e6e27316e5eb670d2ba3580949ec4b6ed5607ef26b8bd64fb3ae4_prof);

    }

    // line 22
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_b214b876dcec5017f4b0de924f002822d99487eb46e162bae65505d3144948b9 = $this->env->getExtension("native_profiler");
        $__internal_b214b876dcec5017f4b0de924f002822d99487eb46e162bae65505d3144948b9->enter($__internal_b214b876dcec5017f4b0de924f002822d99487eb46e162bae65505d3144948b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 23
        echo "<div class=\"input-group\">
        ";
        // line 24
        $context["prepend"] = ("{{" == twig_slice($this->env, (isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), 0, 2));
        // line 25
        echo "        ";
        if ( !(isset($context["prepend"]) ? $context["prepend"] : $this->getContext($context, "prepend"))) {
            // line 26
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 28
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 29
        if ((isset($context["prepend"]) ? $context["prepend"] : $this->getContext($context, "prepend"))) {
            // line 30
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 32
        echo "    </div>";
        
        $__internal_b214b876dcec5017f4b0de924f002822d99487eb46e162bae65505d3144948b9->leave($__internal_b214b876dcec5017f4b0de924f002822d99487eb46e162bae65505d3144948b9_prof);

    }

    // line 35
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_c853a00c6b5e95b4dbc519872aa98456ee1f1d0ac786d1ac603c60babdb66be3 = $this->env->getExtension("native_profiler");
        $__internal_c853a00c6b5e95b4dbc519872aa98456ee1f1d0ac786d1ac603c60babdb66be3->enter($__internal_c853a00c6b5e95b4dbc519872aa98456ee1f1d0ac786d1ac603c60babdb66be3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 36
        echo "<div class=\"input-group\">";
        // line 37
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 38
        echo "<span class=\"input-group-addon\">%</span>
    </div>";
        
        $__internal_c853a00c6b5e95b4dbc519872aa98456ee1f1d0ac786d1ac603c60babdb66be3->leave($__internal_c853a00c6b5e95b4dbc519872aa98456ee1f1d0ac786d1ac603c60babdb66be3_prof);

    }

    // line 42
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_346091b7605cb31aaffdf6c84d49f0bd384fe64650f04be6e25c7d4e63f22947 = $this->env->getExtension("native_profiler");
        $__internal_346091b7605cb31aaffdf6c84d49f0bd384fe64650f04be6e25c7d4e63f22947->enter($__internal_346091b7605cb31aaffdf6c84d49f0bd384fe64650f04be6e25c7d4e63f22947_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 43
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 44
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 46
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 47
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 48
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'errors');
            // line 49
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'errors');
            // line 50
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            echo "&nbsp;";
            // line 51
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            // line 52
            echo "</div>";
        }
        
        $__internal_346091b7605cb31aaffdf6c84d49f0bd384fe64650f04be6e25c7d4e63f22947->leave($__internal_346091b7605cb31aaffdf6c84d49f0bd384fe64650f04be6e25c7d4e63f22947_prof);

    }

    // line 56
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_3156339945b534e7353f86ef6c058bb34b3b19d54577dec77b3a776b1e8206a5 = $this->env->getExtension("native_profiler");
        $__internal_3156339945b534e7353f86ef6c058bb34b3b19d54577dec77b3a776b1e8206a5->enter($__internal_3156339945b534e7353f86ef6c058bb34b3b19d54577dec77b3a776b1e8206a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 57
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 58
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 60
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 61
            if (( !array_key_exists("datetime", $context) ||  !(isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime")))) {
                // line 62
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 64
            echo twig_replace_filter((isset($context["date_pattern"]) ? $context["date_pattern"] : $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 65
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "year", array()), 'widget'), "{{ month }}" =>             // line 66
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "month", array()), 'widget'), "{{ day }}" =>             // line 67
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "day", array()), 'widget')));
            // line 69
            if (( !array_key_exists("datetime", $context) ||  !(isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime")))) {
                // line 70
                echo "</div>";
            }
        }
        
        $__internal_3156339945b534e7353f86ef6c058bb34b3b19d54577dec77b3a776b1e8206a5->leave($__internal_3156339945b534e7353f86ef6c058bb34b3b19d54577dec77b3a776b1e8206a5_prof);

    }

    // line 75
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_f6bf4ebe8939258f13464ebbae2bc621c7b95a6492cccc58a583ba7a59d61e95 = $this->env->getExtension("native_profiler");
        $__internal_f6bf4ebe8939258f13464ebbae2bc621c7b95a6492cccc58a583ba7a59d61e95->enter($__internal_f6bf4ebe8939258f13464ebbae2bc621c7b95a6492cccc58a583ba7a59d61e95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 76
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 77
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 79
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 80
            if (( !array_key_exists("datetime", $context) || (false == (isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime"))))) {
                // line 81
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 83
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hour", array()), 'widget');
            if ((isset($context["with_minutes"]) ? $context["with_minutes"] : $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minute", array()), 'widget');
            }
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second", array()), 'widget');
            }
            // line 84
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == (isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime"))))) {
                // line 85
                echo "</div>";
            }
        }
        
        $__internal_f6bf4ebe8939258f13464ebbae2bc621c7b95a6492cccc58a583ba7a59d61e95->leave($__internal_f6bf4ebe8939258f13464ebbae2bc621c7b95a6492cccc58a583ba7a59d61e95_prof);

    }

    // line 90
    public function block_mwspeso_widget($context, array $blocks = array())
    {
        $__internal_6bf1bfe0aab40651eab30021e32950f0505cddf11ea9dab4f7bbbcb491b6e7cb = $this->env->getExtension("native_profiler");
        $__internal_6bf1bfe0aab40651eab30021e32950f0505cddf11ea9dab4f7bbbcb491b6e7cb->enter($__internal_6bf1bfe0aab40651eab30021e32950f0505cddf11ea9dab4f7bbbcb491b6e7cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mwspeso_widget"));

        // line 92
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 93
        echo "<div class=\"input-group\">
        ";
        // line 94
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
        <span class=\"input-group-addon\">\$</span>
    </div>";
        
        $__internal_6bf1bfe0aab40651eab30021e32950f0505cddf11ea9dab4f7bbbcb491b6e7cb->leave($__internal_6bf1bfe0aab40651eab30021e32950f0505cddf11ea9dab4f7bbbcb491b6e7cb_prof);

    }

    // line 99
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_9fc2fb2451cebfde0e8d17a313e1241c7f7761ea5eee9a26882d91a48514896a = $this->env->getExtension("native_profiler");
        $__internal_9fc2fb2451cebfde0e8d17a313e1241c7f7761ea5eee9a26882d91a48514896a->enter($__internal_9fc2fb2451cebfde0e8d17a313e1241c7f7761ea5eee9a26882d91a48514896a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 100
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        // line 101
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        
        $__internal_9fc2fb2451cebfde0e8d17a313e1241c7f7761ea5eee9a26882d91a48514896a->leave($__internal_9fc2fb2451cebfde0e8d17a313e1241c7f7761ea5eee9a26882d91a48514896a_prof);

    }

    // line 104
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_e0378777ae9ca114d429bff96a69a2e74f479bedb6ba2fe64a5cf9138ccb23f9 = $this->env->getExtension("native_profiler");
        $__internal_e0378777ae9ca114d429bff96a69a2e74f479bedb6ba2fe64a5cf9138ccb23f9->enter($__internal_e0378777ae9ca114d429bff96a69a2e74f479bedb6ba2fe64a5cf9138ccb23f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 105
        if (twig_in_filter("-inline", (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) {
            // line 106
            echo "<div class=\"control-group\">";
            // line 107
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 108
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 109
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 110
(isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 113
            echo "</div>";
        } else {
            // line 115
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 116
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 117
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 118
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 119
(isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 122
            echo "</div>";
        }
        
        $__internal_e0378777ae9ca114d429bff96a69a2e74f479bedb6ba2fe64a5cf9138ccb23f9->leave($__internal_e0378777ae9ca114d429bff96a69a2e74f479bedb6ba2fe64a5cf9138ccb23f9_prof);

    }

    // line 126
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_017fdd6e01c7b36059005bdf0552ef3e08794e177f841670e751bb54168cdf28 = $this->env->getExtension("native_profiler");
        $__internal_017fdd6e01c7b36059005bdf0552ef3e08794e177f841670e751bb54168cdf28->enter($__internal_017fdd6e01c7b36059005bdf0552ef3e08794e177f841670e751bb54168cdf28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 127
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " mws_checkbox"))));
        // line 128
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 129
        if (twig_in_filter("checkbox-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 130
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 132
            echo "<div class=\"checkbox ";
            if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col", array(), "any", true, true)) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col", array()), "html", null, true);
            }
            echo "\">";
            // line 133
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 134
            echo "</div>";
        }
        
        $__internal_017fdd6e01c7b36059005bdf0552ef3e08794e177f841670e751bb54168cdf28->leave($__internal_017fdd6e01c7b36059005bdf0552ef3e08794e177f841670e751bb54168cdf28_prof);

    }

    // line 138
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_abf377becf85ae3188d86dae0f306b403963d4de06d9a598a4ab41ea38893ae9 = $this->env->getExtension("native_profiler");
        $__internal_abf377becf85ae3188d86dae0f306b403963d4de06d9a598a4ab41ea38893ae9->enter($__internal_abf377becf85ae3188d86dae0f306b403963d4de06d9a598a4ab41ea38893ae9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 139
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 140
        if (twig_in_filter("radio-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 141
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 143
            echo "<div class=\"radio\">";
            // line 144
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 145
            echo "</div>";
        }
        
        $__internal_abf377becf85ae3188d86dae0f306b403963d4de06d9a598a4ab41ea38893ae9->leave($__internal_abf377becf85ae3188d86dae0f306b403963d4de06d9a598a4ab41ea38893ae9_prof);

    }

    // line 151
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_ba1e5771735fd6ad8c698f8445707aff1b6494d141438ea5a14c6f91a259c7df = $this->env->getExtension("native_profiler");
        $__internal_ba1e5771735fd6ad8c698f8445707aff1b6494d141438ea5a14c6f91a259c7df->enter($__internal_ba1e5771735fd6ad8c698f8445707aff1b6494d141438ea5a14c6f91a259c7df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 152
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => trim(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " control-label"))));
        // line 153
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_ba1e5771735fd6ad8c698f8445707aff1b6494d141438ea5a14c6f91a259c7df->leave($__internal_ba1e5771735fd6ad8c698f8445707aff1b6494d141438ea5a14c6f91a259c7df_prof);

    }

    // line 156
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_6d07f8f494317e67f505535f7747133ba95422f75bcbebf39107b1bff6f8dc83 = $this->env->getExtension("native_profiler");
        $__internal_6d07f8f494317e67f505535f7747133ba95422f75bcbebf39107b1bff6f8dc83->enter($__internal_6d07f8f494317e67f505535f7747133ba95422f75bcbebf39107b1bff6f8dc83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 158
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => trim(twig_replace_filter((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 159
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_6d07f8f494317e67f505535f7747133ba95422f75bcbebf39107b1bff6f8dc83->leave($__internal_6d07f8f494317e67f505535f7747133ba95422f75bcbebf39107b1bff6f8dc83_prof);

    }

    // line 162
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_1f620d7c9b88c950a07c2a5609fa0427abfd133573df78a97bd0ddc4f9f67ebe = $this->env->getExtension("native_profiler");
        $__internal_1f620d7c9b88c950a07c2a5609fa0427abfd133573df78a97bd0ddc4f9f67ebe->enter($__internal_1f620d7c9b88c950a07c2a5609fa0427abfd133573df78a97bd0ddc4f9f67ebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 163
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_1f620d7c9b88c950a07c2a5609fa0427abfd133573df78a97bd0ddc4f9f67ebe->leave($__internal_1f620d7c9b88c950a07c2a5609fa0427abfd133573df78a97bd0ddc4f9f67ebe_prof);

    }

    // line 166
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_56d9fa53c2997d6c1ad0664a73c52b1aaca68a38c66d564485bc0558d0816af4 = $this->env->getExtension("native_profiler");
        $__internal_56d9fa53c2997d6c1ad0664a73c52b1aaca68a38c66d564485bc0558d0816af4->enter($__internal_56d9fa53c2997d6c1ad0664a73c52b1aaca68a38c66d564485bc0558d0816af4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 167
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_56d9fa53c2997d6c1ad0664a73c52b1aaca68a38c66d564485bc0558d0816af4->leave($__internal_56d9fa53c2997d6c1ad0664a73c52b1aaca68a38c66d564485bc0558d0816af4_prof);

    }

    // line 170
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_bea138729fc3e8e5163b7d115ca941d634f8116fdede4ed02523c184cae2f594 = $this->env->getExtension("native_profiler");
        $__internal_bea138729fc3e8e5163b7d115ca941d634f8116fdede4ed02523c184cae2f594->enter($__internal_bea138729fc3e8e5163b7d115ca941d634f8116fdede4ed02523c184cae2f594_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 171
        echo "    ";
        // line 172
        echo "    ";
        if (array_key_exists("widget", $context)) {
            // line 173
            echo "        ";
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 174
                echo "            ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => trim(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
                // line 175
                echo "        ";
            }
            // line 176
            echo "        ";
            if (array_key_exists("parent_label_class", $context)) {
                // line 177
                echo "            ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => trim((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " ") . (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class"))))));
                // line 178
                echo "        ";
            }
            // line 179
            echo "        ";
            if (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false) && twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))))) {
                // line 180
                if ( !twig_test_empty((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")))) {
                    // line 181
                    $context["label"] = twig_replace_filter((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")), array("%name%" =>                     // line 182
(isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "%id%" =>                     // line 183
(isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                } else {
                    // line 186
                    $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                }
            }
            // line 189
            echo "        <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 190
            echo (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) ? (((((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) ? ((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))) : ($this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 191
            echo "</label>
    ";
        }
        
        $__internal_bea138729fc3e8e5163b7d115ca941d634f8116fdede4ed02523c184cae2f594->leave($__internal_bea138729fc3e8e5163b7d115ca941d634f8116fdede4ed02523c184cae2f594_prof);

    }

    // line 197
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_bf504cd38f86c13952ef10e157aeefec77bed74a8cca27f89507359e25a31599 = $this->env->getExtension("native_profiler");
        $__internal_bf504cd38f86c13952ef10e157aeefec77bed74a8cca27f89507359e25a31599->enter($__internal_bf504cd38f86c13952ef10e157aeefec77bed74a8cca27f89507359e25a31599_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 198
        echo "<div class=\"form-group ";
        if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col", array(), "any", true, true)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col", array()), "html", null, true);
        }
        if ((( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) ? $context["force_error"] : $this->getContext($context, "force_error")), false)) : (false))) &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 199
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 201
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 202
        echo "</div>";
        
        $__internal_bf504cd38f86c13952ef10e157aeefec77bed74a8cca27f89507359e25a31599->leave($__internal_bf504cd38f86c13952ef10e157aeefec77bed74a8cca27f89507359e25a31599_prof);

    }

    // line 205
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_4e98710e6dab7db88b7d90d8eb150b502612559cbeb024ab6e4a54f02c0a01b2 = $this->env->getExtension("native_profiler");
        $__internal_4e98710e6dab7db88b7d90d8eb150b502612559cbeb024ab6e4a54f02c0a01b2->enter($__internal_4e98710e6dab7db88b7d90d8eb150b502612559cbeb024ab6e4a54f02c0a01b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 206
        echo "<div class=\"form-group\">";
        // line 207
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 208
        echo "</div>";
        
        $__internal_4e98710e6dab7db88b7d90d8eb150b502612559cbeb024ab6e4a54f02c0a01b2->leave($__internal_4e98710e6dab7db88b7d90d8eb150b502612559cbeb024ab6e4a54f02c0a01b2_prof);

    }

    // line 211
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_b5d82b22d1614ff4b40e810f0c9e77bc62544b063da57202ea3a3f0a34dc129d = $this->env->getExtension("native_profiler");
        $__internal_b5d82b22d1614ff4b40e810f0c9e77bc62544b063da57202ea3a3f0a34dc129d->enter($__internal_b5d82b22d1614ff4b40e810f0c9e77bc62544b063da57202ea3a3f0a34dc129d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 212
        $context["force_error"] = true;
        // line 213
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_b5d82b22d1614ff4b40e810f0c9e77bc62544b063da57202ea3a3f0a34dc129d->leave($__internal_b5d82b22d1614ff4b40e810f0c9e77bc62544b063da57202ea3a3f0a34dc129d_prof);

    }

    // line 216
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_2ca2efd7dcdb36a3d4569881df5f7717b23a26879576a6c2d2e657fbcf680246 = $this->env->getExtension("native_profiler");
        $__internal_2ca2efd7dcdb36a3d4569881df5f7717b23a26879576a6c2d2e657fbcf680246->enter($__internal_2ca2efd7dcdb36a3d4569881df5f7717b23a26879576a6c2d2e657fbcf680246_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 217
        $context["force_error"] = true;
        // line 218
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_2ca2efd7dcdb36a3d4569881df5f7717b23a26879576a6c2d2e657fbcf680246->leave($__internal_2ca2efd7dcdb36a3d4569881df5f7717b23a26879576a6c2d2e657fbcf680246_prof);

    }

    // line 221
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_93ec928408cf8c79b7b6134a5eaecee8fc42bf032ef452ef0c0576b910c11f4f = $this->env->getExtension("native_profiler");
        $__internal_93ec928408cf8c79b7b6134a5eaecee8fc42bf032ef452ef0c0576b910c11f4f->enter($__internal_93ec928408cf8c79b7b6134a5eaecee8fc42bf032ef452ef0c0576b910c11f4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 222
        $context["force_error"] = true;
        // line 223
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_93ec928408cf8c79b7b6134a5eaecee8fc42bf032ef452ef0c0576b910c11f4f->leave($__internal_93ec928408cf8c79b7b6134a5eaecee8fc42bf032ef452ef0c0576b910c11f4f_prof);

    }

    // line 226
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_f5ed8d838c08133d477e3c28022af278153d9768ccb13048a00778148737623a = $this->env->getExtension("native_profiler");
        $__internal_f5ed8d838c08133d477e3c28022af278153d9768ccb13048a00778148737623a->enter($__internal_f5ed8d838c08133d477e3c28022af278153d9768ccb13048a00778148737623a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 227
        $context["force_error"] = true;
        // line 228
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_f5ed8d838c08133d477e3c28022af278153d9768ccb13048a00778148737623a->leave($__internal_f5ed8d838c08133d477e3c28022af278153d9768ccb13048a00778148737623a_prof);

    }

    // line 231
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_504384d7ef35a44710eb551fd4903f9ebd4d78760ef5a225f621b9a68fd5d6bf = $this->env->getExtension("native_profiler");
        $__internal_504384d7ef35a44710eb551fd4903f9ebd4d78760ef5a225f621b9a68fd5d6bf->enter($__internal_504384d7ef35a44710eb551fd4903f9ebd4d78760ef5a225f621b9a68fd5d6bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 232
        echo "<div class=\"form-group";
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 233
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 234
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 235
        echo "</div>";
        
        $__internal_504384d7ef35a44710eb551fd4903f9ebd4d78760ef5a225f621b9a68fd5d6bf->leave($__internal_504384d7ef35a44710eb551fd4903f9ebd4d78760ef5a225f621b9a68fd5d6bf_prof);

    }

    // line 238
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_08798b0edda5b6fb36de9bcbadefae0d3be4104316373b2a004ee78aa4142f66 = $this->env->getExtension("native_profiler");
        $__internal_08798b0edda5b6fb36de9bcbadefae0d3be4104316373b2a004ee78aa4142f66->enter($__internal_08798b0edda5b6fb36de9bcbadefae0d3be4104316373b2a004ee78aa4142f66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 239
        echo "<div class=\"form-group";
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 240
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 241
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 242
        echo "</div>";
        
        $__internal_08798b0edda5b6fb36de9bcbadefae0d3be4104316373b2a004ee78aa4142f66->leave($__internal_08798b0edda5b6fb36de9bcbadefae0d3be4104316373b2a004ee78aa4142f66_prof);

    }

    // line 247
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_f872e8623bc206638ef8fee7056fdbef9ab2b5ee5222b46375f3628be88a36ed = $this->env->getExtension("native_profiler");
        $__internal_f872e8623bc206638ef8fee7056fdbef9ab2b5ee5222b46375f3628be88a36ed->enter($__internal_f872e8623bc206638ef8fee7056fdbef9ab2b5ee5222b46375f3628be88a36ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 248
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 249
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 250
            echo "    <ul class=\"list-unstyled\">";
            // line 251
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 252
                echo "<li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 254
            echo "</ul>
    ";
            // line 255
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_f872e8623bc206638ef8fee7056fdbef9ab2b5ee5222b46375f3628be88a36ed->leave($__internal_f872e8623bc206638ef8fee7056fdbef9ab2b5ee5222b46375f3628be88a36ed_prof);

    }

    // line 261
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_6a5376e50f65443c38343e7909cfaab9381fafd0d89ec50b2ed1a1304c7b4c8a = $this->env->getExtension("native_profiler");
        $__internal_6a5376e50f65443c38343e7909cfaab9381fafd0d89ec50b2ed1a1304c7b4c8a->enter($__internal_6a5376e50f65443c38343e7909cfaab9381fafd0d89ec50b2ed1a1304c7b4c8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 262
        ob_start();
        // line 263
        echo "    <div class=\"form-group ";
        if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col", array(), "any", true, true)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col", array()), "html", null, true);
        }
        echo "\">
        ";
        // line 264
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_6a5376e50f65443c38343e7909cfaab9381fafd0d89ec50b2ed1a1304c7b4c8a->leave($__internal_6a5376e50f65443c38343e7909cfaab9381fafd0d89ec50b2ed1a1304c7b4c8a_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle:widget:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  908 => 264,  901 => 263,  899 => 262,  893 => 261,  881 => 255,  878 => 254,  870 => 252,  866 => 251,  864 => 250,  858 => 249,  856 => 248,  850 => 247,  843 => 242,  841 => 241,  839 => 240,  833 => 239,  827 => 238,  820 => 235,  818 => 234,  816 => 233,  810 => 232,  804 => 231,  797 => 228,  795 => 227,  789 => 226,  782 => 223,  780 => 222,  774 => 221,  767 => 218,  765 => 217,  759 => 216,  752 => 213,  750 => 212,  744 => 211,  737 => 208,  735 => 207,  733 => 206,  727 => 205,  720 => 202,  718 => 201,  716 => 200,  714 => 199,  705 => 198,  699 => 197,  690 => 191,  686 => 190,  671 => 189,  667 => 186,  664 => 183,  663 => 182,  662 => 181,  660 => 180,  657 => 179,  654 => 178,  651 => 177,  648 => 176,  645 => 175,  642 => 174,  639 => 173,  636 => 172,  634 => 171,  628 => 170,  621 => 167,  615 => 166,  608 => 163,  602 => 162,  595 => 159,  593 => 158,  587 => 156,  580 => 153,  578 => 152,  572 => 151,  564 => 145,  562 => 144,  560 => 143,  557 => 141,  555 => 140,  553 => 139,  547 => 138,  539 => 134,  537 => 133,  531 => 132,  528 => 130,  526 => 129,  524 => 128,  522 => 127,  516 => 126,  508 => 122,  502 => 119,  501 => 118,  500 => 117,  496 => 116,  492 => 115,  489 => 113,  483 => 110,  482 => 109,  481 => 108,  477 => 107,  475 => 106,  473 => 105,  467 => 104,  460 => 101,  458 => 100,  452 => 99,  442 => 94,  439 => 93,  437 => 92,  431 => 90,  422 => 85,  419 => 84,  409 => 83,  404 => 81,  402 => 80,  400 => 79,  397 => 77,  395 => 76,  389 => 75,  380 => 70,  378 => 69,  376 => 67,  375 => 66,  374 => 65,  373 => 64,  368 => 62,  366 => 61,  364 => 60,  361 => 58,  359 => 57,  353 => 56,  345 => 52,  343 => 51,  340 => 50,  338 => 49,  336 => 48,  332 => 47,  330 => 46,  327 => 44,  325 => 43,  319 => 42,  311 => 38,  309 => 37,  307 => 36,  301 => 35,  294 => 32,  288 => 30,  286 => 29,  284 => 28,  278 => 26,  275 => 25,  273 => 24,  270 => 23,  264 => 22,  257 => 19,  255 => 18,  249 => 17,  242 => 14,  240 => 13,  234 => 12,  227 => 9,  224 => 7,  222 => 6,  216 => 5,  209 => 261,  206 => 260,  203 => 258,  201 => 247,  198 => 246,  195 => 244,  193 => 238,  190 => 237,  188 => 231,  185 => 230,  183 => 226,  180 => 225,  178 => 221,  175 => 220,  173 => 216,  170 => 215,  168 => 211,  165 => 210,  163 => 205,  160 => 204,  158 => 197,  155 => 196,  152 => 194,  150 => 170,  147 => 169,  145 => 166,  142 => 165,  140 => 162,  137 => 161,  135 => 156,  132 => 155,  130 => 151,  127 => 150,  124 => 148,  122 => 138,  119 => 137,  117 => 126,  114 => 125,  112 => 104,  109 => 103,  107 => 99,  105 => 90,  103 => 75,  100 => 74,  98 => 56,  95 => 55,  93 => 42,  90 => 41,  88 => 35,  85 => 34,  83 => 22,  80 => 21,  78 => 17,  75 => 16,  73 => 12,  70 => 11,  68 => 5,  65 => 4,  62 => 2,  14 => 1,);
    }
}
/* {% use "form_div_layout.html.twig" %}*/
/* */
/* {# Widgets #}*/
/* */
/* {% block form_widget_simple -%}*/
/*     {% if type is not defined or 'file' != type %}*/
/*         {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) -%}*/
/*     {% endif %}*/
/*     {{- parent() -}}*/
/* {%- endblock form_widget_simple %}*/
/* */
/* {% block textarea_widget -%}*/
/*     {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}*/
/*     {{- parent() -}}*/
/* {%- endblock textarea_widget %}*/
/* */
/* {% block button_widget -%}*/
/*     {% set attr = attr|merge({class: (attr.class|default('btn-default') ~ ' btn')|trim}) %}*/
/*     {{- parent() -}}*/
/* {%- endblock %}*/
/* */
/* {% block money_widget -%}*/
/*     <div class="input-group">*/
/*         {% set prepend = '{{' == money_pattern[0:2] %}*/
/*         {% if not prepend %}*/
/*             <span class="input-group-addon">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>*/
/*         {% endif %}*/
/*         {{- block('form_widget_simple') -}}*/
/*         {% if prepend %}*/
/*             <span class="input-group-addon">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>*/
/*         {% endif %}*/
/*     </div>*/
/* {%- endblock money_widget %}*/
/* */
/* {% block percent_widget -%}*/
/*     <div class="input-group">*/
/*         {{- block('form_widget_simple') -}}*/
/*         <span class="input-group-addon">%</span>*/
/*     </div>*/
/* {%- endblock percent_widget %}*/
/* */
/* {% block datetime_widget -%}*/
/*     {% if widget == 'single_text' %}*/
/*         {{- block('form_widget_simple') -}}*/
/*     {% else -%}*/
/*         {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}*/
/*         <div {{ block('widget_container_attributes') }}>*/
/*             {{- form_errors(form.date) -}}*/
/*             {{- form_errors(form.time) -}}*/
/*             {{- form_widget(form.date, { datetime: true } ) -}}&nbsp;*/
/*             {{- form_widget(form.time, { datetime: true } ) -}}*/
/*         </div>*/
/*     {%- endif %}*/
/* {%- endblock datetime_widget %}*/
/* */
/* {% block date_widget -%}*/
/*     {% if widget == 'single_text' %}*/
/*         {{- block('form_widget_simple') -}}*/
/*     {% else -%}*/
/*         {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}*/
/*         {% if datetime is not defined or not datetime -%}*/
/*             <div {{ block('widget_container_attributes') -}}>*/
/*         {%- endif %}*/
/*             {{- date_pattern|replace({*/
/*                 '{{ year }}': form_widget(form.year),*/
/*                 '{{ month }}': form_widget(form.month),*/
/*                 '{{ day }}': form_widget(form.day),*/
/*             })|raw -}}*/
/*         {% if datetime is not defined or not datetime -%}*/
/*             </div>*/
/*         {%- endif -%}*/
/*     {% endif %}*/
/* {%- endblock date_widget %}*/
/* */
/* {% block time_widget -%}*/
/*     {% if widget == 'single_text' %}*/
/*         {{- block('form_widget_simple') -}}*/
/*     {% else -%}*/
/*         {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}*/
/*         {% if datetime is not defined or false == datetime -%}*/
/*             <div {{ block('widget_container_attributes') -}}>*/
/*         {%- endif -%}*/
/*         {{- form_widget(form.hour) }}{% if with_minutes %}:{{ form_widget(form.minute) }}{% endif %}{% if with_seconds %}:{{ form_widget(form.second) }}{% endif %}*/
/*         {% if datetime is not defined or false == datetime -%}*/
/*             </div>*/
/*         {%- endif -%}*/
/*     {% endif %}*/
/* {%- endblock time_widget %}*/
/* */
/* {%- block mwspeso_widget -%}*/
/*     {# type="number" doesn't work with floats #}*/
/*     {%- set type = type|default('text') -%}*/
/*     <div class="input-group">*/
/*         {{ block('form_widget_simple') }}*/
/*         <span class="input-group-addon">$</span>*/
/*     </div>*/
/* {%- endblock mwspeso_widget -%}*/
/* */
/* {% block choice_widget_collapsed -%}*/
/*     {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}*/
/*     {{- parent() -}}*/
/* {%- endblock %}*/
/* */
/* {% block choice_widget_expanded -%}*/
/*     {% if '-inline' in label_attr.class|default('') -%}*/
/*         <div class="control-group">*/
/*             {%- for child in form %}*/
/*                 {{- form_widget(child, {*/
/*                     parent_label_class: label_attr.class|default(''),*/
/*                     translation_domain: choice_translation_domain,*/
/*                 }) -}}*/
/*             {% endfor -%}*/
/*         </div>*/
/*     {%- else -%}*/
/*         <div {{ block('widget_container_attributes') }}>*/
/*             {%- for child in form %}*/
/*                 {{- form_widget(child, {*/
/*                     parent_label_class: label_attr.class|default(''),*/
/*                     translation_domain: choice_translation_domain,*/
/*                 }) -}}*/
/*             {% endfor -%}*/
/*         </div>*/
/*     {%- endif %}*/
/* {%- endblock choice_widget_expanded %}*/
/* */
/* {% block checkbox_widget -%}*/
/*     {% set attr = attr|merge({'class': (attr.class|default('') ~ ' mws_checkbox')|trim}) %}*/
/*     {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}*/
/*     {% if 'checkbox-inline' in parent_label_class %}*/
/*         {{- form_label(form, null, { widget: parent() }) -}}*/
/*     {% else -%}*/
/*         <div class="checkbox {% if attr.col is defined %}{{ attr.col }}{% endif %}">*/
/*             {{- form_label(form, null, { widget: parent() }) -}}*/
/*         </div>*/
/*     {%- endif %}*/
/* {%- endblock checkbox_widget %}*/
/* */
/* {% block radio_widget -%}*/
/*     {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}*/
/*     {% if 'radio-inline' in parent_label_class %}*/
/*         {{- form_label(form, null, { widget: parent() }) -}}*/
/*     {% else -%}*/
/*         <div class="radio">*/
/*             {{- form_label(form, null, { widget: parent() }) -}}*/
/*         </div>*/
/*     {%- endif %}*/
/* {%- endblock radio_widget %}*/
/* */
/* {# Labels #}*/
/* */
/* {% block form_label -%}*/
/*     {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' control-label')|trim}) -%}*/
/*     {{- parent() -}}*/
/* {%- endblock form_label %}*/
/* */
/* {% block choice_label -%}*/
/*     {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}*/
/*     {%- set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) -%}*/
/*     {{- block('form_label') -}}*/
/* {% endblock %}*/
/* */
/* {% block checkbox_label -%}*/
/*     {{- block('checkbox_radio_label') -}}*/
/* {%- endblock checkbox_label %}*/
/* */
/* {% block radio_label -%}*/
/*     {{- block('checkbox_radio_label') -}}*/
/* {%- endblock radio_label %}*/
/* */
/* {% block checkbox_radio_label %}*/
/*     {# Do not display the label if widget is not defined in order to prevent double label rendering #}*/
/*     {% if widget is defined %}*/
/*         {% if required %}*/
/*             {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}*/
/*         {% endif %}*/
/*         {% if parent_label_class is defined %}*/
/*             {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) %}*/
/*         {% endif %}*/
/*         {% if label is not same as(false) and label is empty %}*/
/*             {%- if label_format is not empty -%}*/
/*                 {% set label = label_format|replace({*/
/*                     '%name%': name,*/
/*                     '%id%': id,*/
/*                 }) %}*/
/*             {%- else -%}*/
/*                 {% set label = name|humanize %}*/
/*             {%- endif -%}*/
/*         {% endif %}*/
/*         <label{% for attrname, attrvalue in label_attr %} {{ attrname }}="{{ attrvalue }}"{% endfor %}>*/
/*             {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}*/
/*         </label>*/
/*     {% endif %}*/
/* {% endblock checkbox_radio_label %}*/
/* */
/* {# Rows #}*/
/* */
/* {% block form_row -%}*/
/*     <div class="form-group {% if attr.col is defined %}{{ attr.col }}{% endif %}{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}">*/
/*         {{- form_label(form) -}}*/
/*         {{- form_widget(form) -}}*/
/*         {{- form_errors(form) -}}*/
/*     </div>*/
/* {%- endblock form_row %}*/
/* */
/* {% block button_row -%}*/
/*     <div class="form-group">*/
/*         {{- form_widget(form) -}}*/
/*     </div>*/
/* {%- endblock button_row %}*/
/* */
/* {% block choice_row -%}*/
/*     {% set force_error = true %}*/
/*     {{- block('form_row') }}*/
/* {%- endblock choice_row %}*/
/* */
/* {% block date_row -%}*/
/*     {% set force_error = true %}*/
/*     {{- block('form_row') }}*/
/* {%- endblock date_row %}*/
/* */
/* {% block time_row -%}*/
/*     {% set force_error = true %}*/
/*     {{- block('form_row') }}*/
/* {%- endblock time_row %}*/
/* */
/* {% block datetime_row -%}*/
/*     {% set force_error = true %}*/
/*     {{- block('form_row') }}*/
/* {%- endblock datetime_row %}*/
/* */
/* {% block checkbox_row -%}*/
/*     <div class="form-group{% if not valid %} has-error{% endif %}">*/
/*         {{- form_widget(form) -}}*/
/*         {{- form_errors(form) -}}*/
/*     </div>*/
/* {%- endblock checkbox_row %}*/
/* */
/* {% block radio_row -%}*/
/*     <div class="form-group{% if not valid %} has-error{% endif %}">*/
/*         {{- form_widget(form) -}}*/
/*         {{- form_errors(form) -}}*/
/*     </div>*/
/* {%- endblock radio_row %}*/
/* */
/* {# Errors #}*/
/* */
/* {% block form_errors -%}*/
/*     {% if errors|length > 0 -%}*/
/*     {% if form.parent %}<span class="help-block">{% else %}<div class="alert alert-danger">{% endif %}*/
/*     <ul class="list-unstyled">*/
/*         {%- for error in errors -%}*/
/*             <li><span class="glyphicon glyphicon-exclamation-sign"></span> {{ error.message }}</li>*/
/*         {%- endfor -%}*/
/*     </ul>*/
/*     {% if form.parent %}</span>{% else %}</div>{% endif %}*/
/*     {%- endif %}*/
/* {%- endblock form_errors %}*/
/* */
/* {# Rows #}*/
/* */
/* {% block submit_row -%}*/
/* {% spaceless %}*/
/*     <div class="form-group {% if attr.col is defined %}{{ attr.col }}{% endif %}">*/
/*         {{ form_widget(form) }}*/
/*     </div>*/
/* {% endspaceless %}*/
/* {% endblock submit_row %}*/
