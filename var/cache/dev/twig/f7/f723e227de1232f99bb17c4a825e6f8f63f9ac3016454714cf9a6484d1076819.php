<?php

/* MWSimpleAdminCrudBundle:Form:select2_field.html.twig */
class __TwigTemplate_a82a87c0333f0f662c555b92696464f6eff4b49ab57eb438ecbaa4d819e833e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'select2_widget' => array($this, 'block_select2_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e156b189a9ad7f769c2a87a0bf7963613b3e105899446b0fc8dcb82e18807d6 = $this->env->getExtension("native_profiler");
        $__internal_6e156b189a9ad7f769c2a87a0bf7963613b3e105899446b0fc8dcb82e18807d6->enter($__internal_6e156b189a9ad7f769c2a87a0bf7963613b3e105899446b0fc8dcb82e18807d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle:Form:select2_field.html.twig"));

        // line 1
        $this->displayBlock('select2_widget', $context, $blocks);
        
        $__internal_6e156b189a9ad7f769c2a87a0bf7963613b3e105899446b0fc8dcb82e18807d6->leave($__internal_6e156b189a9ad7f769c2a87a0bf7963613b3e105899446b0fc8dcb82e18807d6_prof);

    }

    public function block_select2_widget($context, array $blocks = array())
    {
        $__internal_50680171d0698ea469b21d2c1642777e97c8e3ddb74a3e554230f29cab869cd9 = $this->env->getExtension("native_profiler");
        $__internal_50680171d0698ea469b21d2c1642777e97c8e3ddb74a3e554230f29cab869cd9->enter($__internal_50680171d0698ea469b21d2c1642777e97c8e3ddb74a3e554230f29cab869cd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "select2_widget"));

        // line 2
        echo "    <div class=\"form-group ";
        if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col", array(), "any", true, true)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col", array()), "html", null, true);
        }
        echo "\">
        ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        echo "
        <input id=\"select2_";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" type=\"text\" value=\"\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">
        <input type=\"hidden\" ";
        // line 5
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\" ";
        }
        echo ">
    </div>
    <script type=\"text/javascript\">
        (function(\$) {
            \$field = jQuery('#select2_";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "');
            \$store = jQuery('#";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "');
            \$configs = ";
        // line 11
        echo twig_jsonencode_filter((isset($context["configs"]) ? $context["configs"] : $this->getContext($context, "configs")));
        echo ";
            \$configs.ajax = {
                url: '";
        // line 13
        echo $this->env->getExtension('routing')->getPath((isset($context["url"]) ? $context["url"] : $this->getContext($context, "url")));
        echo "',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term, //search term
                        page_limit: 20, // page size
                    };
                },
                results: function (data, page) {
                    return {results: data};
                }
            }
            if(jQuery('#";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "').val().length > 0) {
                preload_data = JSON.parse(jQuery('#";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "').val());
                \$configs.initSelection = function (element, callback) {
                    var obj= preload_data;
                    callback(obj);
                };
            }
            \$configs.formatResult = function (data) {                  
                return \"<div class='select2-user-result'>\" + data.text + \"</div>\";
            };
            \$configs.dropdownCssClass = \"bigdrop\";// apply css that makes the dropdown taller
            if(jQuery('#";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "').val().length > 0) {
                \$field.select2(\$configs).select2('val', 1 );
            } else {
                \$field.select2(\$configs);
            }
            ";
        // line 41
        if (($this->getAttribute((isset($context["configs"]) ? $context["configs"] : $this->getContext($context, "configs")), "multiple", array()) == true)) {
            // line 42
            echo "                jQuery('#select2_";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "').on(\"change\", function(e) {
                    if (jQuery('#";
            // line 43
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "').val().length> 0) {
                        collection = JSON.parse(jQuery('#";
            // line 44
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "').val());
                    } else {
                        collection = [];
                    }
                    if (e.added){
                        collection.push(e.added);
                    } else {
                        collection = \$.grep(collection, function(j,i){ return j.id != e.removed.id });
                    }
                    jQuery('#";
            // line 53
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "').val(JSON.stringify(collection));
                })
            ";
        } else {
            // line 56
            echo "                jQuery('#select2_";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "').on(\"change\", function(e) {
                    jQuery('#";
            // line 57
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "').val(JSON.stringify(e.added));
                    //pregunta si tiene la key metodo existe
                    ";
            // line 59
            if ($this->getAttribute((isset($context["configs"]) ? $context["configs"] : null), "metodo", array(), "any", true, true)) {
                echo "                       
                        ";
                // line 60
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["configs"]) ? $context["configs"] : $this->getContext($context, "configs")), "metodo", array()), "html", null, true);
                echo "(e.added );
                    ";
            }
            // line 61
            echo " 
                })
            ";
        }
        // line 64
        echo "        })(jQuery);
        ";
        // line 65
        if ((($this->getAttribute((isset($context["configs"]) ? $context["configs"] : $this->getContext($context, "configs")), "multiple", array()) == false) &&  !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))))) {
            // line 66
            echo "            var value = JSON.parse('";
            echo (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"));
            echo "');
            jQuery('#s2id_select2_";
            // line 67
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo " .select2-chosen').html(value.text);
        ";
        }
        // line 69
        echo "    </script>
";
        
        $__internal_50680171d0698ea469b21d2c1642777e97c8e3ddb74a3e554230f29cab869cd9->leave($__internal_50680171d0698ea469b21d2c1642777e97c8e3ddb74a3e554230f29cab869cd9_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle:Form:select2_field.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  186 => 69,  181 => 67,  176 => 66,  174 => 65,  171 => 64,  166 => 61,  161 => 60,  157 => 59,  152 => 57,  147 => 56,  141 => 53,  129 => 44,  125 => 43,  120 => 42,  118 => 41,  110 => 36,  97 => 26,  93 => 25,  78 => 13,  73 => 11,  69 => 10,  65 => 9,  52 => 5,  46 => 4,  42 => 3,  35 => 2,  23 => 1,);
    }
}
/* {% block select2_widget %}*/
/*     <div class="form-group {% if attr.col is defined %}{{ attr.col }}{% endif %}">*/
/*         {{ form_label(form) }}*/
/*         <input id="select2_{{ id }}" type="text" value="" {{ block('widget_attributes') }}>*/
/*         <input type="hidden" {{ block('widget_attributes') }} {% if value is not empty %}value="{{ value }}" {% endif %}>*/
/*     </div>*/
/*     <script type="text/javascript">*/
/*         (function($) {*/
/*             $field = jQuery('#select2_{{ id }}');*/
/*             $store = jQuery('#{{ id }}');*/
/*             $configs = {{ configs|json_encode|raw }};*/
/*             $configs.ajax = {*/
/*                 url: '{{ path(url) }}',*/
/*                 dataType: 'json',*/
/*                 data: function (term, page) {*/
/*                     return {*/
/*                         q: term, //search term*/
/*                         page_limit: 20, // page size*/
/*                     };*/
/*                 },*/
/*                 results: function (data, page) {*/
/*                     return {results: data};*/
/*                 }*/
/*             }*/
/*             if(jQuery('#{{ id }}').val().length > 0) {*/
/*                 preload_data = JSON.parse(jQuery('#{{ id }}').val());*/
/*                 $configs.initSelection = function (element, callback) {*/
/*                     var obj= preload_data;*/
/*                     callback(obj);*/
/*                 };*/
/*             }*/
/*             $configs.formatResult = function (data) {                  */
/*                 return "<div class='select2-user-result'>" + data.text + "</div>";*/
/*             };*/
/*             $configs.dropdownCssClass = "bigdrop";// apply css that makes the dropdown taller*/
/*             if(jQuery('#{{ id }}').val().length > 0) {*/
/*                 $field.select2($configs).select2('val', 1 );*/
/*             } else {*/
/*                 $field.select2($configs);*/
/*             }*/
/*             {% if configs.multiple == true %}*/
/*                 jQuery('#select2_{{ id }}').on("change", function(e) {*/
/*                     if (jQuery('#{{ id }}').val().length> 0) {*/
/*                         collection = JSON.parse(jQuery('#{{ id }}').val());*/
/*                     } else {*/
/*                         collection = [];*/
/*                     }*/
/*                     if (e.added){*/
/*                         collection.push(e.added);*/
/*                     } else {*/
/*                         collection = $.grep(collection, function(j,i){ return j.id != e.removed.id });*/
/*                     }*/
/*                     jQuery('#{{ id }}').val(JSON.stringify(collection));*/
/*                 })*/
/*             {% else %}*/
/*                 jQuery('#select2_{{ id }}').on("change", function(e) {*/
/*                     jQuery('#{{ id }}').val(JSON.stringify(e.added));*/
/*                     //pregunta si tiene la key metodo existe*/
/*                     {% if configs.metodo is defined %}                       */
/*                         {{configs.metodo}}(e.added );*/
/*                     {% endif %} */
/*                 })*/
/*             {% endif %}*/
/*         })(jQuery);*/
/*         {% if configs.multiple == false and value is not empty %}*/
/*             var value = JSON.parse('{{ value|raw }}');*/
/*             jQuery('#s2id_select2_{{ id }} .select2-chosen').html(value.text);*/
/*         {% endif %}*/
/*     </script>*/
/* {% endblock %}*/
/* */
