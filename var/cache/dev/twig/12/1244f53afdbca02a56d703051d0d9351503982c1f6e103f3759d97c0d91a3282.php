<?php

/* SistemaCPCEBundle:Trabajo:index.html.twig */
class __TwigTemplate_65769c262f968eed60e70166aedcb2b076482a7ea4e7823ded729088655da026 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MWSimpleAdminCrudBundle:Default:index.html.twig", "SistemaCPCEBundle:Trabajo:index.html.twig", 1);
        $this->blocks = array(
            'buttons' => array($this, 'block_buttons'),
            'actions' => array($this, 'block_actions'),
            'buttonsbelow' => array($this, 'block_buttonsbelow'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MWSimpleAdminCrudBundle:Default:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab43d159a29873a57efff774d711f5c5e01b100932d6b37c8675948e6a9a3ad8 = $this->env->getExtension("native_profiler");
        $__internal_ab43d159a29873a57efff774d711f5c5e01b100932d6b37c8675948e6a9a3ad8->enter($__internal_ab43d159a29873a57efff774d711f5c5e01b100932d6b37c8675948e6a9a3ad8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Trabajo:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ab43d159a29873a57efff774d711f5c5e01b100932d6b37c8675948e6a9a3ad8->leave($__internal_ab43d159a29873a57efff774d711f5c5e01b100932d6b37c8675948e6a9a3ad8_prof);

    }

    // line 3
    public function block_buttons($context, array $blocks = array())
    {
        $__internal_2f8b0c00006d912f7e922750ac47940d1150d0a1b8ac07045584029d2b5414a7 = $this->env->getExtension("native_profiler");
        $__internal_2f8b0c00006d912f7e922750ac47940d1150d0a1b8ac07045584029d2b5414a7->enter($__internal_2f8b0c00006d912f7e922750ac47940d1150d0a1b8ac07045584029d2b5414a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttons"));

        // line 4
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 5
            echo "        <a class=\"btn btn-success\" href=\"";
            echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "new", array()));
            echo "\">
            ";
            // line 6
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.createnew", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
            echo "
        </a>
    ";
        }
        // line 9
        echo "    <div class=\"btn-group\">
        <button type=\"button\" class=\"btn btn-info dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
            ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.export", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo " <span class=\"caret\"></span>
        </button>
      <ul class=\"dropdown-menu\" role=\"menu\">
        <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "xls"));
        echo "\">EXCEL</a></li>
        <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "csv"));
        echo "\">CSV</a></li>
        <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "json"));
        echo "\">JSON</a></li>
      </ul>
    </div>
    <a id=\"reset_index_filters\" class=\"btn btn-danger\">
        ";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.reset", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
    </a>
    <a class=\"btn btn-primary dropdown-toggle\" data-toggle=\"collapse\" data-target=\"#filters\">
        ";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.filters", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
        <span class=\"caret\"></span>
    </a>
";
        
        $__internal_2f8b0c00006d912f7e922750ac47940d1150d0a1b8ac07045584029d2b5414a7->leave($__internal_2f8b0c00006d912f7e922750ac47940d1150d0a1b8ac07045584029d2b5414a7_prof);

    }

    // line 28
    public function block_actions($context, array $blocks = array())
    {
        $__internal_019468f6d690dbc0d35189b15c1ddf8ddb561ff3f9df85fe265b42118047b9e8 = $this->env->getExtension("native_profiler");
        $__internal_019468f6d690dbc0d35189b15c1ddf8ddb561ff3f9df85fe265b42118047b9e8->enter($__internal_019468f6d690dbc0d35189b15c1ddf8ddb561ff3f9df85fe265b42118047b9e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 29
        echo "    <a class=\"glyphicon glyphicon-search tooltips\"  href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "show", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.actions.show", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "\"></a>
    ";
        // line 30
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 31
            echo "        <a class=\"glyphicon glyphicon-edit tooltips\"  href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "edit", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.actions.edit", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
            echo "\"></a>
    ";
        }
        // line 33
        echo "    ";
        // line 36
        echo "    ";
        if ((null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "certificado", array()))) {
            // line 37
            echo "        <a class=\"glyphicon glyphicon-remove tooltips\"  href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "revisar", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"Estado de Revisión NO FINALIZADO\" onclick=\"return confirm('Marcar trabajo como NO FINALIZADO. Esta seguro?');\"></a>
    ";
        }
        
        $__internal_019468f6d690dbc0d35189b15c1ddf8ddb561ff3f9df85fe265b42118047b9e8->leave($__internal_019468f6d690dbc0d35189b15c1ddf8ddb561ff3f9df85fe265b42118047b9e8_prof);

    }

    // line 41
    public function block_buttonsbelow($context, array $blocks = array())
    {
        $__internal_c4fb96b5ee2671494f77527d3ee9f95ce43915584ef51adaa9eb3a5ae00c3db9 = $this->env->getExtension("native_profiler");
        $__internal_c4fb96b5ee2671494f77527d3ee9f95ce43915584ef51adaa9eb3a5ae00c3db9->enter($__internal_c4fb96b5ee2671494f77527d3ee9f95ce43915584ef51adaa9eb3a5ae00c3db9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonsbelow"));

        // line 42
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 43
            echo "        <div class=\"col-lg-8 col-md-8 col-sm-8\">
            <a class=\"btn btn-success likepaginator\" href=\"";
            // line 44
            echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "new", array()));
            echo "\">
                ";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.createnew", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
            echo "
            </a>
        </div>
    ";
        }
        
        $__internal_c4fb96b5ee2671494f77527d3ee9f95ce43915584ef51adaa9eb3a5ae00c3db9->leave($__internal_c4fb96b5ee2671494f77527d3ee9f95ce43915584ef51adaa9eb3a5ae00c3db9_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Trabajo:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 45,  149 => 44,  146 => 43,  143 => 42,  137 => 41,  126 => 37,  123 => 36,  121 => 33,  113 => 31,  111 => 30,  104 => 29,  98 => 28,  87 => 23,  81 => 20,  74 => 16,  70 => 15,  66 => 14,  60 => 11,  56 => 9,  50 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'MWSimpleAdminCrudBundle:Default:index.html.twig' %}*/
/* */
/* {% block buttons %}*/
/*     {% if is_granted('ROLE_ADMIN') %}*/
/*         <a class="btn btn-success" href="{{ path(config.new) }}">*/
/*             {{ 'views.index.createnew'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}*/
/*         </a>*/
/*     {% endif %}*/
/*     <div class="btn-group">*/
/*         <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">*/
/*             {{ 'views.index.export'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }} <span class="caret"></span>*/
/*         </button>*/
/*       <ul class="dropdown-menu" role="menu">*/
/*         <li><a href="{{ path(config.export, { 'format': "xls" }) }}">EXCEL</a></li>*/
/*         <li><a href="{{ path(config.export, { 'format': "csv" }) }}">CSV</a></li>*/
/*         <li><a href="{{ path(config.export, { 'format': "json" }) }}">JSON</a></li>*/
/*       </ul>*/
/*     </div>*/
/*     <a id="reset_index_filters" class="btn btn-danger">*/
/*         {{ 'views.index.reset'|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*     </a>*/
/*     <a class="btn btn-primary dropdown-toggle" data-toggle="collapse" data-target="#filters">*/
/*         {{ 'views.index.filters'|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*         <span class="caret"></span>*/
/*     </a>*/
/* {% endblock %}*/
/* */
/* {% block actions %}*/
/*     <a class="glyphicon glyphicon-search tooltips"  href="{{ path(config.show, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.show'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>*/
/*     {% if is_granted('ROLE_ADMIN') %}*/
/*         <a class="glyphicon glyphicon-edit tooltips"  href="{{ path(config.edit, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.edit'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>*/
/*     {% endif %}*/
/*     {#% if entity.certificado is null %}*/
/*         <a class="glyphicon glyphicon-ok tooltips"  href="{{ path(config.revisar, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="Estado de Revisión FINALIZADO" onclick="return confirm('Marcar trabajo como FINALIZADO. Esta seguro?');"></a>*/
/*     {% endif %#}*/
/*     {% if entity.certificado is null %}*/
/*         <a class="glyphicon glyphicon-remove tooltips"  href="{{ path(config.revisar, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="Estado de Revisión NO FINALIZADO" onclick="return confirm('Marcar trabajo como NO FINALIZADO. Esta seguro?');"></a>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
/* {% block buttonsbelow %}*/
/*     {% if is_granted('ROLE_ADMIN') %}*/
/*         <div class="col-lg-8 col-md-8 col-sm-8">*/
/*             <a class="btn btn-success likepaginator" href="{{ path(config.new) }}">*/
/*                 {{ 'views.index.createnew'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}*/
/*             </a>*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
