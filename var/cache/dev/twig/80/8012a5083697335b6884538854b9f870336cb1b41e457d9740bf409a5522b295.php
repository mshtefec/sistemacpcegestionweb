<?php

/* SistemaCPCEBundle:Validacion:index.html.twig */
class __TwigTemplate_b035ed6bde4f746723e69d011974537e739cb764bd07f4ff374a561e5c0abc96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaCPCEBundle::Longlayout.html.twig", "SistemaCPCEBundle:Validacion:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'wrapper' => array($this, 'block_wrapper'),
            'messages_error' => array($this, 'block_messages_error'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaCPCEBundle::Longlayout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_154e440158c647f21965f9be84186f056163d4279f0da8775a8304cc0932908a = $this->env->getExtension("native_profiler");
        $__internal_154e440158c647f21965f9be84186f056163d4279f0da8775a8304cc0932908a->enter($__internal_154e440158c647f21965f9be84186f056163d4279f0da8775a8304cc0932908a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Validacion:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_154e440158c647f21965f9be84186f056163d4279f0da8775a8304cc0932908a->leave($__internal_154e440158c647f21965f9be84186f056163d4279f0da8775a8304cc0932908a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_98d12ba6e47a57f46d42b17af5c89c9832f4a822e5d2d3641b4b391c0be74b7b = $this->env->getExtension("native_profiler");
        $__internal_98d12ba6e47a57f46d42b17af5c89c9832f4a822e5d2d3641b4b391c0be74b7b->enter($__internal_98d12ba6e47a57f46d42b17af5c89c9832f4a822e5d2d3641b4b391c0be74b7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t";
        $this->displayBlock('wrapper', $context, $blocks);
        // line 33
        echo "\t";
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_98d12ba6e47a57f46d42b17af5c89c9832f4a822e5d2d3641b4b391c0be74b7b->leave($__internal_98d12ba6e47a57f46d42b17af5c89c9832f4a822e5d2d3641b4b391c0be74b7b_prof);

    }

    // line 4
    public function block_wrapper($context, array $blocks = array())
    {
        $__internal_b1baa1c3a0b744f1627df1c3abb6cacc286b6b842547ede4d718886962fdb7a3 = $this->env->getExtension("native_profiler");
        $__internal_b1baa1c3a0b744f1627df1c3abb6cacc286b6b842547ede4d718886962fdb7a3->enter($__internal_b1baa1c3a0b744f1627df1c3abb6cacc286b6b842547ede4d718886962fdb7a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        // line 5
        echo "\t<div id=\"page-wrapper\" style=\"margin: 0px;\">
\t\t<div class=\"content\">
\t\t\t";
        // line 7
        $this->displayBlock('messages_error', $context, $blocks);
        // line 16
        echo "\t\t\t";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
\t\t\t<div class=\"panel panel-primary center-block\">
\t\t        <div class=\"panel-heading\">Validación de Matriculados</div>
\t\t        <div class=\"panel-body\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiTitulo", array()), 'row');
        echo "
\t\t\t\t\t\t";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiMatricula", array()), 'row');
        echo "
\t\t\t\t\t\t<div class=\"col-lg-4 col-md-4 col-sm-4\">
\t\t\t\t\t\t\t";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "validar", array()), 'row');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t        </div>
\t\t    </div>
\t\t    ";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
\t\t</div>
\t</div>
\t";
        
        $__internal_b1baa1c3a0b744f1627df1c3abb6cacc286b6b842547ede4d718886962fdb7a3->leave($__internal_b1baa1c3a0b744f1627df1c3abb6cacc286b6b842547ede4d718886962fdb7a3_prof);

    }

    // line 7
    public function block_messages_error($context, array $blocks = array())
    {
        $__internal_93a6ce31676115f72fcdb9f635d1e9855fac408e2967efe91973b9215f83f9d6 = $this->env->getExtension("native_profiler");
        $__internal_93a6ce31676115f72fcdb9f635d1e9855fac408e2967efe91973b9215f83f9d6->enter($__internal_93a6ce31676115f72fcdb9f635d1e9855fac408e2967efe91973b9215f83f9d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "messages_error"));

        // line 8
        echo "\t\t\t    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 9
            echo "\t\t\t        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 10
                echo "\t\t\t            <div class=\"alert ";
                if (($context["type"] == "success")) {
                    echo " alert-success ";
                } else {
                    echo " alert-danger ";
                }
                echo "\">
\t\t\t                ";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
\t\t\t            </div>
\t\t\t        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 14
            echo "\t\t\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "\t\t\t";
        
        $__internal_93a6ce31676115f72fcdb9f635d1e9855fac408e2967efe91973b9215f83f9d6->leave($__internal_93a6ce31676115f72fcdb9f635d1e9855fac408e2967efe91973b9215f83f9d6_prof);

    }

    // line 33
    public function block_footer($context, array $blocks = array())
    {
        $__internal_4ddc2af92258ffb7ff893295bf0f5a138d2b2f64fcfdd2c54e9e138ccec922e7 = $this->env->getExtension("native_profiler");
        $__internal_4ddc2af92258ffb7ff893295bf0f5a138d2b2f64fcfdd2c54e9e138ccec922e7->enter($__internal_4ddc2af92258ffb7ff893295bf0f5a138d2b2f64fcfdd2c54e9e138ccec922e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 34
        echo "\t";
        
        $__internal_4ddc2af92258ffb7ff893295bf0f5a138d2b2f64fcfdd2c54e9e138ccec922e7->leave($__internal_4ddc2af92258ffb7ff893295bf0f5a138d2b2f64fcfdd2c54e9e138ccec922e7_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Validacion:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 34,  150 => 33,  143 => 15,  137 => 14,  128 => 11,  119 => 10,  114 => 9,  109 => 8,  103 => 7,  92 => 29,  84 => 24,  79 => 22,  75 => 21,  66 => 16,  64 => 7,  60 => 5,  54 => 4,  46 => 33,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'SistemaCPCEBundle::Longlayout.html.twig' %}*/
/* */
/* {% block body %}*/
/* 	{% block wrapper %}*/
/* 	<div id="page-wrapper" style="margin: 0px;">*/
/* 		<div class="content">*/
/* 			{% block messages_error %}*/
/* 			    {% for type, messages in app.session.flashbag.all() %}*/
/* 			        {% for message in messages %}*/
/* 			            <div class="alert {% if type == "success" %} alert-success {% else %} alert-danger {% endif %}">*/
/* 			                {{ message|trans({}, 'FOSUserBundle') }}*/
/* 			            </div>*/
/* 			        {% endfor %}*/
/* 			    {% endfor %}*/
/* 			{% endblock messages_error %}*/
/* 			{{ form_start(form) }}*/
/* 			<div class="panel panel-primary center-block">*/
/* 		        <div class="panel-heading">Validación de Matriculados</div>*/
/* 		        <div class="panel-body">*/
/* 					<div class="row">*/
/* 						{{ form_row(form.afiTitulo) }}*/
/* 						{{ form_row(form.afiMatricula) }}*/
/* 						<div class="col-lg-4 col-md-4 col-sm-4">*/
/* 							{{ form_row(form.validar) }}*/
/* 						</div>*/
/* 					</div>*/
/* 		        </div>*/
/* 		    </div>*/
/* 		    {{ form_end(form) }}*/
/* 		</div>*/
/* 	</div>*/
/* 	{% endblock wrapper %}*/
/* 	{% block footer %}*/
/* 	{% endblock footer %}*/
/* {% endblock body %}	*/
/* */
