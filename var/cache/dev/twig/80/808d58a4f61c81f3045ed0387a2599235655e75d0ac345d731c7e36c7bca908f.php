<?php

/* SistemaCPCEBundle:Ayuda:novedad.html.twig */
class __TwigTemplate_5a7e40021bb3106471d1acb91b323371edeab5b8e5c562d73cf6b80ff28dbbc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_94337ff4a7db293e38f6e756b457ffda3aeb873958dbab4f13faf2fe97b33b5c = $this->env->getExtension("native_profiler");
        $__internal_94337ff4a7db293e38f6e756b457ffda3aeb873958dbab4f13faf2fe97b33b5c->enter($__internal_94337ff4a7db293e38f6e756b457ffda3aeb873958dbab4f13faf2fe97b33b5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Ayuda:novedad.html.twig"));

        // line 1
        echo "<div class=\"panel-heading\">
    <div class=\"panel-title\">Novedades</div>
</div>
<div style=\"padding-top:30px\" class=\"panel-body\">
    <ul class=\"list-group\">
        <li class=\"list-group-item\">
            <div class=\"row toggle\" id=\"dropdown-detail-1\" data-toggle=\"detail-1\">
                <div class=\"col-xs-10\">
                    <strong><small>23/03/2016</small> > Curriculum Vitae</strong>
                </div>
                <div class=\"col-xs-2\"><i class=\"fa fa-chevron-down pull-right\"></i></div>
            </div>
            <div>
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        Se agrego la carga de Curriculum Vitae para Matriculados. Ingresando a la opción Datos del men&uacute; se encuentra en la parte inferior.<br>
                        Formatos soportados:<br>
                        - PDF - DOC - DOCX.
                    </div>
                </div>
            </div>
        </li>
        <li class=\"list-group-item\">
            <div class=\"row toggle\" id=\"dropdown-detail-1\" data-toggle=\"detail-1\">
                <div class=\"col-xs-10\">
                    <strong><small>08/03/2015</small> > Trabajos</strong>
                </div>
                <div class=\"col-xs-2\"><i class=\"fa fa-chevron-down pull-right\"></i></div>
            </div>
            <div>
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        Se agregaron campos para seguimiento del trabajo.<br>
                        Actualmente tiene 5 estados: GENERADO, PAGADO, PRESENTADO, CONFECCIONANDO REINTEGRO, REINTEGRO.<br>
                        Se actualiz&oacute; la ayuda para la generaci&oacute;n del trabajo.<br>
                        Antes de confirmar un trabajo usted puede visualizar la vista previa del mismo, para presentarlo debe si o si Confirmar el trabajo.
                    </div>
                </div>
            </div>
        </li>
        <li class=\"list-group-item\">
            <div class=\"row toggle\" id=\"dropdown-detail-1\" data-toggle=\"detail-1\">
                <div class=\"col-xs-10\">
                    <strong><small>06/11/2015</small> > Trabajos</strong>
                </div>
                <div class=\"col-xs-2\"><i class=\"fa fa-chevron-down pull-right\"></i></div>
            </div>
            <div>
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        Se agregaron campos para montos opcionales.<br>
                        Se solucionó error reportado con respecto al almacenado de la fecha del trabajo.
                    </div>
                </div>
            </div>
        </li>
        <li class=\"list-group-item\">
            <div class=\"row toggle\" id=\"dropdown-detail-1\" data-toggle=\"detail-1\">
                <div class=\"col-xs-10\">
                    <strong><small>06/10/2015</small> > Trabajos</strong>
                </div>
                <div class=\"col-xs-2\"><i class=\"fa fa-chevron-down pull-right\"></i></div>
            </div>
            <div>
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        Podr&aacute; confeccionar el formulario de trabajos.<br>
                        Recuerde imprimir y prensentarlo en su delegaci&oacute;n para dar inicio al tramite.
                    </div>
                </div>
            </div>
        </li>
        <li class=\"list-group-item\">
            <div class=\"row toggle\" id=\"dropdown-detail-2\" data-toggle=\"detail-2\">
                <div class=\"col-xs-10\">
                    <strong><small>06/10/2015</small> > Estado de Cuenta</strong>
                </div>
                <div class=\"col-xs-2\"><i class=\"fa fa-chevron-down pull-right\"></i></div>
            </div>
            <div>
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        Podr&aacute; visualizar su estado de cuenta y el detalle seg&uacute;n cuenta.
                    </div>
                </div>
            </div>
        </li>
        <li class=\"list-group-item\">
            <div class=\"row toggle\" id=\"dropdown-detail-3\" data-toggle=\"detail-3\">
                <div class=\"col-xs-10\">
                    <strong><small>06/10/2015</small> > Datos</strong>
                </div>
                <div class=\"col-xs-2\"><i class=\"fa fa-chevron-down pull-right\"></i></div>
            </div>
            <div>
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        Visualizaci&oacute;n de sus datos personales.
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
";
        
        $__internal_94337ff4a7db293e38f6e756b457ffda3aeb873958dbab4f13faf2fe97b33b5c->leave($__internal_94337ff4a7db293e38f6e756b457ffda3aeb873958dbab4f13faf2fe97b33b5c_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Ayuda:novedad.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div class="panel-heading">*/
/*     <div class="panel-title">Novedades</div>*/
/* </div>*/
/* <div style="padding-top:30px" class="panel-body">*/
/*     <ul class="list-group">*/
/*         <li class="list-group-item">*/
/*             <div class="row toggle" id="dropdown-detail-1" data-toggle="detail-1">*/
/*                 <div class="col-xs-10">*/
/*                     <strong><small>23/03/2016</small> > Curriculum Vitae</strong>*/
/*                 </div>*/
/*                 <div class="col-xs-2"><i class="fa fa-chevron-down pull-right"></i></div>*/
/*             </div>*/
/*             <div>*/
/*                 <div class="row">*/
/*                     <div class="col-xs-12">*/
/*                         Se agrego la carga de Curriculum Vitae para Matriculados. Ingresando a la opción Datos del men&uacute; se encuentra en la parte inferior.<br>*/
/*                         Formatos soportados:<br>*/
/*                         - PDF - DOC - DOCX.*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <div class="row toggle" id="dropdown-detail-1" data-toggle="detail-1">*/
/*                 <div class="col-xs-10">*/
/*                     <strong><small>08/03/2015</small> > Trabajos</strong>*/
/*                 </div>*/
/*                 <div class="col-xs-2"><i class="fa fa-chevron-down pull-right"></i></div>*/
/*             </div>*/
/*             <div>*/
/*                 <div class="row">*/
/*                     <div class="col-xs-12">*/
/*                         Se agregaron campos para seguimiento del trabajo.<br>*/
/*                         Actualmente tiene 5 estados: GENERADO, PAGADO, PRESENTADO, CONFECCIONANDO REINTEGRO, REINTEGRO.<br>*/
/*                         Se actualiz&oacute; la ayuda para la generaci&oacute;n del trabajo.<br>*/
/*                         Antes de confirmar un trabajo usted puede visualizar la vista previa del mismo, para presentarlo debe si o si Confirmar el trabajo.*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <div class="row toggle" id="dropdown-detail-1" data-toggle="detail-1">*/
/*                 <div class="col-xs-10">*/
/*                     <strong><small>06/11/2015</small> > Trabajos</strong>*/
/*                 </div>*/
/*                 <div class="col-xs-2"><i class="fa fa-chevron-down pull-right"></i></div>*/
/*             </div>*/
/*             <div>*/
/*                 <div class="row">*/
/*                     <div class="col-xs-12">*/
/*                         Se agregaron campos para montos opcionales.<br>*/
/*                         Se solucionó error reportado con respecto al almacenado de la fecha del trabajo.*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <div class="row toggle" id="dropdown-detail-1" data-toggle="detail-1">*/
/*                 <div class="col-xs-10">*/
/*                     <strong><small>06/10/2015</small> > Trabajos</strong>*/
/*                 </div>*/
/*                 <div class="col-xs-2"><i class="fa fa-chevron-down pull-right"></i></div>*/
/*             </div>*/
/*             <div>*/
/*                 <div class="row">*/
/*                     <div class="col-xs-12">*/
/*                         Podr&aacute; confeccionar el formulario de trabajos.<br>*/
/*                         Recuerde imprimir y prensentarlo en su delegaci&oacute;n para dar inicio al tramite.*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <div class="row toggle" id="dropdown-detail-2" data-toggle="detail-2">*/
/*                 <div class="col-xs-10">*/
/*                     <strong><small>06/10/2015</small> > Estado de Cuenta</strong>*/
/*                 </div>*/
/*                 <div class="col-xs-2"><i class="fa fa-chevron-down pull-right"></i></div>*/
/*             </div>*/
/*             <div>*/
/*                 <div class="row">*/
/*                     <div class="col-xs-12">*/
/*                         Podr&aacute; visualizar su estado de cuenta y el detalle seg&uacute;n cuenta.*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <div class="row toggle" id="dropdown-detail-3" data-toggle="detail-3">*/
/*                 <div class="col-xs-10">*/
/*                     <strong><small>06/10/2015</small> > Datos</strong>*/
/*                 </div>*/
/*                 <div class="col-xs-2"><i class="fa fa-chevron-down pull-right"></i></div>*/
/*             </div>*/
/*             <div>*/
/*                 <div class="row">*/
/*                     <div class="col-xs-12">*/
/*                         Visualizaci&oacute;n de sus datos personales.*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </li>*/
/*     </ul>*/
/* </div>*/
/* */
