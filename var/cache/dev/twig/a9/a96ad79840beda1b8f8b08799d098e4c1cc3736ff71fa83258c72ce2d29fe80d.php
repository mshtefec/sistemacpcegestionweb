<?php

/* SistemaCPCEBundle:Ayuda:sugeridosIndex.html.twig */
class __TwigTemplate_43f9e032083f978f891c218ef86c1cfe99b51c7ad46b6646245b8903f50e7790 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaUserBundle::layout.html.twig", "SistemaCPCEBundle:Ayuda:sugeridosIndex.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f13aa26a997dcb35782d7e013f62e40ed9ee71c75b5e2b3853a9bf7d032d23c = $this->env->getExtension("native_profiler");
        $__internal_6f13aa26a997dcb35782d7e013f62e40ed9ee71c75b5e2b3853a9bf7d032d23c->enter($__internal_6f13aa26a997dcb35782d7e013f62e40ed9ee71c75b5e2b3853a9bf7d032d23c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Ayuda:sugeridosIndex.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f13aa26a997dcb35782d7e013f62e40ed9ee71c75b5e2b3853a9bf7d032d23c->leave($__internal_6f13aa26a997dcb35782d7e013f62e40ed9ee71c75b5e2b3853a9bf7d032d23c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_92111c04bba5e4e576b21a587bf9614eebf955986da3c9388ea1e2d8ec83820e = $this->env->getExtension("native_profiler");
        $__internal_92111c04bba5e4e576b21a587bf9614eebf955986da3c9388ea1e2d8ec83820e->enter($__internal_92111c04bba5e4e576b21a587bf9614eebf955986da3c9388ea1e2d8ec83820e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    <div class=\"panel-heading\">
    <div class=\"panel-title\">Sugeridos</div>
\t</div>
\t<div style=\"padding-top:30px\" class=\"panel-body\">
\t    <ul class=\"list-group\">
\t        <li class=\"list-group-item\">
\t            <div class=\"row toggle\" id=\"dropdown-detail-1\" data-toggle=\"detail-1\">
\t                <div class=\"col-xs-10\">
\t                    <strong><small>19/04/2016</small> > Sugerido Honorarios Mínimos</strong>
\t                </div>
\t                <div class=\"col-xs-2\"><i class=\"fa fa-chevron-down pull-right\"></i></div>
\t            </div>
\t            <div>
\t            \tHaga Click para descargar Archivo Adjunto.
\t            \t<div class=\"row\">
\t                \t<div class=\"col-xs-3\">
\t                    \t<form id=\"form\" action=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("ayuda_getArchive");
        echo "\" method=\"post\" name=\"form\">
\t                    \t\t<button id=\"form_archive\" class=\"form-control btn-success\" col=\"col-lg-2\" name=\"button\" type=\"submit\"><span class=\"glyphicon glyphicon-save-file\"></span> Descargar</button>
\t                    \t\t<input id=\"form_archivo\" type=\"hidden\" value=\"/varios/TABLADESUGERIDOSHONORARIOSMINIMOS.pdf\" name=\"dirname\">
\t                    \t</form>
\t                    </div>
\t                </div>
\t            </div>
\t        </li>
\t    </ul>
\t</div>
";
        
        $__internal_92111c04bba5e4e576b21a587bf9614eebf955986da3c9388ea1e2d8ec83820e->leave($__internal_92111c04bba5e4e576b21a587bf9614eebf955986da3c9388ea1e2d8ec83820e_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Ayuda:sugeridosIndex.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "SistemaUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="panel-heading">*/
/*     <div class="panel-title">Sugeridos</div>*/
/* 	</div>*/
/* 	<div style="padding-top:30px" class="panel-body">*/
/* 	    <ul class="list-group">*/
/* 	        <li class="list-group-item">*/
/* 	            <div class="row toggle" id="dropdown-detail-1" data-toggle="detail-1">*/
/* 	                <div class="col-xs-10">*/
/* 	                    <strong><small>19/04/2016</small> > Sugerido Honorarios Mínimos</strong>*/
/* 	                </div>*/
/* 	                <div class="col-xs-2"><i class="fa fa-chevron-down pull-right"></i></div>*/
/* 	            </div>*/
/* 	            <div>*/
/* 	            	Haga Click para descargar Archivo Adjunto.*/
/* 	            	<div class="row">*/
/* 	                	<div class="col-xs-3">*/
/* 	                    	<form id="form" action="{{ path('ayuda_getArchive') }}" method="post" name="form">*/
/* 	                    		<button id="form_archive" class="form-control btn-success" col="col-lg-2" name="button" type="submit"><span class="glyphicon glyphicon-save-file"></span> Descargar</button>*/
/* 	                    		<input id="form_archivo" type="hidden" value="/varios/TABLADESUGERIDOSHONORARIOSMINIMOS.pdf" name="dirname">*/
/* 	                    	</form>*/
/* 	                    </div>*/
/* 	                </div>*/
/* 	            </div>*/
/* 	        </li>*/
/* 	    </ul>*/
/* 	</div>*/
/* {% endblock fos_user_content %}*/
