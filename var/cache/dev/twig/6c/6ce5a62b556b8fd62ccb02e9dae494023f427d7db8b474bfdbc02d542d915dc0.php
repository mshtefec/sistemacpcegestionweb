<?php

/* MWSimpleAdminCrudBundle:Form:duallist_field.html.twig */
class __TwigTemplate_1281523c7c6c34063e9453d8f4dbe1b207e5a0221be131699397195a6fdfbb6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'duallist_widget' => array($this, 'block_duallist_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0edb2a69fe3f316a5991ccf94f4b6bed691892ccd2e9bd829580a4a016017837 = $this->env->getExtension("native_profiler");
        $__internal_0edb2a69fe3f316a5991ccf94f4b6bed691892ccd2e9bd829580a4a016017837->enter($__internal_0edb2a69fe3f316a5991ccf94f4b6bed691892ccd2e9bd829580a4a016017837_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle:Form:duallist_field.html.twig"));

        // line 1
        $this->displayBlock('duallist_widget', $context, $blocks);
        
        $__internal_0edb2a69fe3f316a5991ccf94f4b6bed691892ccd2e9bd829580a4a016017837->leave($__internal_0edb2a69fe3f316a5991ccf94f4b6bed691892ccd2e9bd829580a4a016017837_prof);

    }

    public function block_duallist_widget($context, array $blocks = array())
    {
        $__internal_10b3c578181c0607043cc7e5ef732abcd425686cdb3f5a4460a3e7e5a00650aa = $this->env->getExtension("native_profiler");
        $__internal_10b3c578181c0607043cc7e5ef732abcd425686cdb3f5a4460a3e7e5a00650aa->enter($__internal_10b3c578181c0607043cc7e5ef732abcd425686cdb3f5a4460a3e7e5a00650aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "duallist_widget"));

        // line 2
        echo "<style type=\"text/css\">
        ";
        // line 3
        echo twig_escape_filter($this->env, (".dual-list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (".list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " {
            margin-top: 8px;
        }
        ";
        // line 6
        echo twig_escape_filter($this->env, (".list-left-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " li, ";
        echo twig_escape_filter($this->env, (".list-right-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " li {
            cursor: pointer;
        }
        ";
        // line 9
        echo twig_escape_filter($this->env, (".list-arrows-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " {
            padding-top: 100px;
        }
        ";
        // line 12
        echo twig_escape_filter($this->env, (".list-arrows-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " button {
            margin-bottom: 20px;
        }
    </style>

    <br />
    <div ";
        // line 18
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        <div class=\"";
        // line 19
        echo twig_escape_filter($this->env, ("dual-list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ("list-left-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " col-md-5\">
            <div class=\"well text-right\">
                <div class=\"row\">
                    <div class=\"col-md-10\">
                        <div class=\"input-group\">
                            <span class=\"input-group-addon glyphicon glyphicon-search\"></span>
                            <input type=\"text\" name=\"";
        // line 25
        echo twig_escape_filter($this->env, ("SearchDualList-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\" class=\"form-control\" placeholder=\"search\" />
                        </div>
                    </div>
                    <div class=\"col-md-2\">
                        <div class=\"btn-group\">
                            <a class=\"btn btn-default selector\" title=\"select all\"><i class=\"glyphicon glyphicon-unchecked\"></i></a>
                        </div>
                    </div>
                </div>
                <ul class=\"";
        // line 34
        echo twig_escape_filter($this->env, ("list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\">";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            if ( !$this->getAttribute($this->getAttribute($context["child"], "vars", array()), "checked", array())) {
                // line 36
                echo "                        <li class=\"list-group-item\" data-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "value", array()), "html", null, true);
                echo "\">
                            ";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()), "html", null, true);
                echo "
                            <input type=\"checkbox\" id=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "id", array()), "html", null, true);
                echo "\" name=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "full_name", array()), "html", null, true);
                echo "\" ";
                if ($this->getAttribute($this->getAttribute($context["child"], "vars", array(), "any", false, true), "value", array(), "any", true, true)) {
                    echo "value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "value", array()), "html", null, true);
                    echo "\"";
                }
                if ($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "checked", array())) {
                    echo " checked=\"checked\"";
                }
                echo " style=\"display: none;\" />
                        </li>
                    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "</ul>
            </div>
        </div>

        <div class=\"";
        // line 45
        echo twig_escape_filter($this->env, ("list-arrows-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " col-md-1 text-center\">
            <button class=\"btn btn-default btn-sm ";
        // line 46
        echo twig_escape_filter($this->env, ("move-left-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " btn-not-validate\">
                <span class=\"glyphicon glyphicon-chevron-left\"></span>
            </button>

            <button class=\"btn btn-default btn-sm ";
        // line 50
        echo twig_escape_filter($this->env, ("move-right-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " btn-not-validate\">
                <span class=\"glyphicon glyphicon-chevron-right\"></span>
            </button>
        </div>

        <div class=\"";
        // line 55
        echo twig_escape_filter($this->env, ("dual-list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ("list-right-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " col-md-5\">
            <div class=\"well\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <div class=\"btn-group\">
                            <a class=\"btn btn-default selector\" title=\"select all\"><i class=\"glyphicon glyphicon-unchecked\"></i></a>
                        </div>
                    </div>
                    <div class=\"col-md-10\">
                        <div class=\"input-group\">
                            <input type=\"text\" name=\"";
        // line 65
        echo twig_escape_filter($this->env, ("SearchDualList-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\" class=\"form-control\" placeholder=\"search\" />
                            <span class=\"input-group-addon glyphicon glyphicon-search\"></span>
                        </div>
                    </div>
                </div>
                <ul class=\"";
        // line 70
        echo twig_escape_filter($this->env, ("list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\">";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            if ($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "checked", array())) {
                // line 72
                echo "                        <li class=\"list-group-item\" data-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "value", array()), "html", null, true);
                echo "\">
                            ";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()), "html", null, true);
                echo "
                            <input type=\"checkbox\" id=\"";
                // line 74
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "id", array()), "html", null, true);
                echo "\" name=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "full_name", array()), "html", null, true);
                echo "\" ";
                if ($this->getAttribute($this->getAttribute($context["child"], "vars", array(), "any", false, true), "value", array(), "any", true, true)) {
                    echo "value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "value", array()), "html", null, true);
                    echo "\"";
                }
                if ($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "checked", array())) {
                    echo " checked=\"checked\"";
                }
                echo " style=\"display: none;\" />
                        </li>
                    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "</ul>
            </div>
        </div>
    </div>

    <script type=\"text/javascript\">
        \$(\".btn-not-validate\").click(function() {
            return false;
        });
        \$(function () {
            \$('body').on('click', \"";
        // line 87
        echo twig_escape_filter($this->env, (".list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " .list-group-item\", function () {
                \$(this).toggleClass('active');
            });
            \$(\"";
        // line 90
        echo twig_escape_filter($this->env, (".list-arrows-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " button\").click(function () {
                var \$button = \$(this), actives = '';
                if (\$button.hasClass(\"";
        // line 92
        echo twig_escape_filter($this->env, ("move-left-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\")) {
                    \$(\"";
        // line 93
        echo twig_escape_filter($this->env, (".list-right-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ul li.active\").children().prop('checked', false);
                    actives = \$(\"";
        // line 94
        echo twig_escape_filter($this->env, (".list-right-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ul li.active\");
                    actives.clone().appendTo(\"";
        // line 95
        echo twig_escape_filter($this->env, (".list-left-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ul\");
                    actives.remove();
                } else if (\$button.hasClass(\"";
        // line 97
        echo twig_escape_filter($this->env, ("move-right-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\")) {
                    \$(\"";
        // line 98
        echo twig_escape_filter($this->env, (".list-left-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ul li.active\").children().prop('checked', true);
                    actives = \$(\"";
        // line 99
        echo twig_escape_filter($this->env, (".list-left-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ul li.active\");
                    actives.clone().appendTo(\"";
        // line 100
        echo twig_escape_filter($this->env, (".list-right-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " ul\");
                    actives.remove();
                }
            });
            \$(\"";
        // line 104
        echo twig_escape_filter($this->env, (".dual-list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " .selector\").click(function () {
                var \$checkBox = \$(this);
                if (!\$checkBox.hasClass('selected')) {
                    \$checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
                    \$checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                } else {
                    \$checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
                    \$checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                }
            });
            \$('[name=\"";
        // line 114
        echo twig_escape_filter($this->env, ("SearchDualList-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\"]').keyup(function (e) {
                var code = e.keyCode || e.which;
                if (code == '9') return;
                if (code == '27') \$(this).val(null);
                var \$rows = \$(this).closest(\"";
        // line 118
        echo twig_escape_filter($this->env, (".dual-list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo "\").find(\"";
        echo twig_escape_filter($this->env, (".list-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array())), "html", null, true);
        echo " li\");
                var val = \$.trim(\$(this).val()).replace(/ +/g, ' ').toLowerCase();
                \$rows.show().filter(function () {
                    var text = \$(this).text().replace(/\\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();
            });
        });
    </script>";
        
        $__internal_10b3c578181c0607043cc7e5ef732abcd425686cdb3f5a4460a3e7e5a00650aa->leave($__internal_10b3c578181c0607043cc7e5ef732abcd425686cdb3f5a4460a3e7e5a00650aa_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle:Form:duallist_field.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  299 => 118,  292 => 114,  279 => 104,  272 => 100,  268 => 99,  264 => 98,  260 => 97,  255 => 95,  251 => 94,  247 => 93,  243 => 92,  238 => 90,  232 => 87,  220 => 77,  199 => 74,  195 => 73,  190 => 72,  185 => 71,  182 => 70,  174 => 65,  159 => 55,  151 => 50,  144 => 46,  140 => 45,  134 => 41,  113 => 38,  109 => 37,  104 => 36,  99 => 35,  96 => 34,  84 => 25,  73 => 19,  69 => 18,  60 => 12,  54 => 9,  46 => 6,  38 => 3,  35 => 2,  23 => 1,);
    }
}
/* {%- block duallist_widget -%}*/
/*     <style type="text/css">*/
/*         {{ '.dual-list-' ~ form.vars.name }} {{ '.list-' ~ form.vars.name }} {*/
/*             margin-top: 8px;*/
/*         }*/
/*         {{ '.list-left-' ~ form.vars.name }} li, {{ '.list-right-' ~ form.vars.name }} li {*/
/*             cursor: pointer;*/
/*         }*/
/*         {{ '.list-arrows-' ~ form.vars.name }} {*/
/*             padding-top: 100px;*/
/*         }*/
/*         {{ '.list-arrows-' ~ form.vars.name }} button {*/
/*             margin-bottom: 20px;*/
/*         }*/
/*     </style>*/
/* */
/*     <br />*/
/*     <div {{ block('widget_container_attributes') }}>*/
/*         <div class="{{ 'dual-list-' ~ form.vars.name }} {{ 'list-left-' ~ form.vars.name }} col-md-5">*/
/*             <div class="well text-right">*/
/*                 <div class="row">*/
/*                     <div class="col-md-10">*/
/*                         <div class="input-group">*/
/*                             <span class="input-group-addon glyphicon glyphicon-search"></span>*/
/*                             <input type="text" name="{{ 'SearchDualList-' ~ form.vars.name }}" class="form-control" placeholder="search" />*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-2">*/
/*                         <div class="btn-group">*/
/*                             <a class="btn btn-default selector" title="select all"><i class="glyphicon glyphicon-unchecked"></i></a>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <ul class="{{ 'list-' ~ form.vars.name }}">*/
/*                     {%- for child in form if not child.vars.checked %}*/
/*                         <li class="list-group-item" data-id="{{ child.vars.value }}">*/
/*                             {{ child.vars.label }}*/
/*                             <input type="checkbox" id="{{ child.vars.id }}" name="{{ child.vars.full_name }}" {% if child.vars.value is defined %}value="{{ child.vars.value }}"{% endif %}{% if child.vars.checked %} checked="checked"{% endif %} style="display: none;" />*/
/*                         </li>*/
/*                     {% endfor -%}*/
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="{{ 'list-arrows-' ~ form.vars.name }} col-md-1 text-center">*/
/*             <button class="btn btn-default btn-sm {{ 'move-left-' ~ form.vars.name }} btn-not-validate">*/
/*                 <span class="glyphicon glyphicon-chevron-left"></span>*/
/*             </button>*/
/* */
/*             <button class="btn btn-default btn-sm {{ 'move-right-' ~ form.vars.name }} btn-not-validate">*/
/*                 <span class="glyphicon glyphicon-chevron-right"></span>*/
/*             </button>*/
/*         </div>*/
/* */
/*         <div class="{{ 'dual-list-' ~ form.vars.name }} {{ 'list-right-' ~ form.vars.name }} col-md-5">*/
/*             <div class="well">*/
/*                 <div class="row">*/
/*                     <div class="col-md-2">*/
/*                         <div class="btn-group">*/
/*                             <a class="btn btn-default selector" title="select all"><i class="glyphicon glyphicon-unchecked"></i></a>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-10">*/
/*                         <div class="input-group">*/
/*                             <input type="text" name="{{ 'SearchDualList-' ~ form.vars.name }}" class="form-control" placeholder="search" />*/
/*                             <span class="input-group-addon glyphicon glyphicon-search"></span>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <ul class="{{ 'list-' ~ form.vars.name }}">*/
/*                     {%- for child in form if child.vars.checked %}*/
/*                         <li class="list-group-item" data-id="{{ child.vars.value }}">*/
/*                             {{ child.vars.label }}*/
/*                             <input type="checkbox" id="{{ child.vars.id }}" name="{{ child.vars.full_name }}" {% if child.vars.value is defined %}value="{{ child.vars.value }}"{% endif %}{% if child.vars.checked %} checked="checked"{% endif %} style="display: none;" />*/
/*                         </li>*/
/*                     {% endfor -%}*/
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <script type="text/javascript">*/
/*         $(".btn-not-validate").click(function() {*/
/*             return false;*/
/*         });*/
/*         $(function () {*/
/*             $('body').on('click', "{{ '.list-' ~ form.vars.name }} .list-group-item", function () {*/
/*                 $(this).toggleClass('active');*/
/*             });*/
/*             $("{{ '.list-arrows-' ~ form.vars.name }} button").click(function () {*/
/*                 var $button = $(this), actives = '';*/
/*                 if ($button.hasClass("{{ 'move-left-' ~ form.vars.name }}")) {*/
/*                     $("{{ '.list-right-' ~ form.vars.name }} ul li.active").children().prop('checked', false);*/
/*                     actives = $("{{ '.list-right-' ~ form.vars.name }} ul li.active");*/
/*                     actives.clone().appendTo("{{ '.list-left-' ~ form.vars.name }} ul");*/
/*                     actives.remove();*/
/*                 } else if ($button.hasClass("{{ 'move-right-' ~ form.vars.name }}")) {*/
/*                     $("{{ '.list-left-' ~ form.vars.name }} ul li.active").children().prop('checked', true);*/
/*                     actives = $("{{ '.list-left-' ~ form.vars.name }} ul li.active");*/
/*                     actives.clone().appendTo("{{ '.list-right-' ~ form.vars.name }} ul");*/
/*                     actives.remove();*/
/*                 }*/
/*             });*/
/*             $("{{ '.dual-list-' ~ form.vars.name }} .selector").click(function () {*/
/*                 var $checkBox = $(this);*/
/*                 if (!$checkBox.hasClass('selected')) {*/
/*                     $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');*/
/*                     $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');*/
/*                 } else {*/
/*                     $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');*/
/*                     $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');*/
/*                 }*/
/*             });*/
/*             $('[name="{{ 'SearchDualList-' ~ form.vars.name }}"]').keyup(function (e) {*/
/*                 var code = e.keyCode || e.which;*/
/*                 if (code == '9') return;*/
/*                 if (code == '27') $(this).val(null);*/
/*                 var $rows = $(this).closest("{{ '.dual-list-' ~ form.vars.name }}").find("{{ '.list-' ~ form.vars.name }} li");*/
/*                 var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();*/
/*                 $rows.show().filter(function () {*/
/*                     var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();*/
/*                     return !~text.indexOf(val);*/
/*                 }).hide();*/
/*             });*/
/*         });*/
/*     </script>*/
/* {%- endblock duallist_widget -%}*/
