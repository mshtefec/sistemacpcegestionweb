<?php

/* SistemaCPCEBundle:Dato:datosPdf.html.twig */
class __TwigTemplate_598666963026b5a45f2fc2005ace43c0fe4439dbd78a9d50695f7a15b197728b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee89b2e90e927e4fed0dabca43590b3ea5de789aabbba3759bcf02814f78137b = $this->env->getExtension("native_profiler");
        $__internal_ee89b2e90e927e4fed0dabca43590b3ea5de789aabbba3759bcf02814f78137b->enter($__internal_ee89b2e90e927e4fed0dabca43590b3ea5de789aabbba3759bcf02814f78137b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Dato:datosPdf.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <style type=\"text/css\">
            p {
                margin-top: 10px;
                height: 0px;
                font-size: 22px;
            }
            th p {
                float: left;
            }
            .content {
                background-color: #fff;
                margin: 0 1em;
                padding-left: 1em;
            }
            .right {
                float: right;
                font-size: 22px;    
            }
            .centrar {
                text-align: center !important;
                float: none !important;
            }
            .text-right {
                text-align: right;
            }
            .text-long {
                font-size: 20px;
                font-weight: bold;
            }
        </style>
    </head>
    <body class=\"focusedform\">
        <div class=\"right\">";
        // line 37
        echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, (isset($context["fecha"]) ? $context["fecha"] : $this->getContext($context, "fecha")), "full", "none"), "html", null, true);
        echo "</div>
        <br>
        <div class=\"content\">
            <div class=\"centrar\">
                <h1 style=\"color: #4285F4; font-size: 43px\">CONSEJO PROFESIONAL DE CIENCIAS ECONÓMICAS</h1>
                <img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/images/logo_grande.png", null, true), "html", null, true);
        echo "\" alt=\"\" />
                <div style=\"width: 15em; margin: 0 auto;\">
                    ";
        // line 44
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('barcode')->getCallable(), array(array("code" => (("" . $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "afiTitulo", array())) . $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "afiMatricula", array())), "type" => "qrcode", "format" => "html", "width" => 12, "height" => 12))), "html", null, true);
        echo "
                </div>
            </div>
            <div>
                <h1>PROFESIONAL MATRICULADO:</h1>
                <hr>
                <table style=\"width:100%\" border=\"0\">
                    <tr>
                        <td colspan=\"4\"><p><strong>";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "afiNombre", array()), "html", null, true);
        echo "</strong></p></td>
                    </tr>
                    <tr>
                        <td colspan=\"4\"><p><strong>";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "afiTitulo", array()), "html", null, true);
        echo "</strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "afiMatricula", array()), "html", null, true);
        echo " Delegación: ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getAfiZonaDelegacion", array()), "html", null, true);
        echo "</p></td>
                    </tr>
                    <tr>
                        <td colspan=\"4\"><p>";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "afiDireccion", array()), "html", null, true);
        echo "</p></td>
                    </tr>
                    <tr>
                        <td colspan=\"4\"><p>";
        // line 61
        echo twig_escape_filter($this->env, ((((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "Afilocalidad", array()) . ", ") . $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "Afiprovincia", array())) . " (") . $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "Aficodpos", array())) . ")"), "html", null, true);
        echo "</p></td>
                    </tr>
                    <tr>
                        <td colspan=\"4\"><p><strong>Correo:</strong> ";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "afiMail", array()), "html", null, true);
        echo "</p></td>
                    </tr>
                </table>  
            </div>
            <hr>
            <h1 class=\"centrar\">Honorarios regulados y sugeridos por el Consejo Profesional de Ciencias Económicas</h1>
        </div>    
    </body>
</html>";
        
        $__internal_ee89b2e90e927e4fed0dabca43590b3ea5de789aabbba3759bcf02814f78137b->leave($__internal_ee89b2e90e927e4fed0dabca43590b3ea5de789aabbba3759bcf02814f78137b_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Dato:datosPdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 64,  106 => 61,  100 => 58,  90 => 55,  84 => 52,  73 => 44,  68 => 42,  60 => 37,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         <style type="text/css">*/
/*             p {*/
/*                 margin-top: 10px;*/
/*                 height: 0px;*/
/*                 font-size: 22px;*/
/*             }*/
/*             th p {*/
/*                 float: left;*/
/*             }*/
/*             .content {*/
/*                 background-color: #fff;*/
/*                 margin: 0 1em;*/
/*                 padding-left: 1em;*/
/*             }*/
/*             .right {*/
/*                 float: right;*/
/*                 font-size: 22px;    */
/*             }*/
/*             .centrar {*/
/*                 text-align: center !important;*/
/*                 float: none !important;*/
/*             }*/
/*             .text-right {*/
/*                 text-align: right;*/
/*             }*/
/*             .text-long {*/
/*                 font-size: 20px;*/
/*                 font-weight: bold;*/
/*             }*/
/*         </style>*/
/*     </head>*/
/*     <body class="focusedform">*/
/*         <div class="right">{{ fecha|localizeddate('full', 'none') }}</div>*/
/*         <br>*/
/*         <div class="content">*/
/*             <div class="centrar">*/
/*                 <h1 style="color: #4285F4; font-size: 43px">CONSEJO PROFESIONAL DE CIENCIAS ECONÓMICAS</h1>*/
/*                 <img src="{{ asset('bundles/sistemacpce/images/logo_grande.png', absolute=true) }}" alt="" />*/
/*                 <div style="width: 15em; margin: 0 auto;">*/
/*                     {{ barcode({code: '' ~ entity.afiTitulo ~ entity.afiMatricula, type: 'qrcode', format: 'html', width: 12, height: 12}) }}*/
/*                 </div>*/
/*             </div>*/
/*             <div>*/
/*                 <h1>PROFESIONAL MATRICULADO:</h1>*/
/*                 <hr>*/
/*                 <table style="width:100%" border="0">*/
/*                     <tr>*/
/*                         <td colspan="4"><p><strong>{{ entity.afiNombre }}</strong></p></td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td colspan="4"><p><strong>{{ entity.afiTitulo }}</strong>{{ entity.afiMatricula }} Delegación: {{ entity.getAfiZonaDelegacion }}</p></td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td colspan="4"><p>{{ entity.afiDireccion }}</p></td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td colspan="4"><p>{{ entity.Afilocalidad ~ ', ' ~ entity.Afiprovincia ~ ' (' ~ entity.Aficodpos ~ ')' }}</p></td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td colspan="4"><p><strong>Correo:</strong> {{ entity.afiMail }}</p></td>*/
/*                     </tr>*/
/*                 </table>  */
/*             </div>*/
/*             <hr>*/
/*             <h1 class="centrar">Honorarios regulados y sugeridos por el Consejo Profesional de Ciencias Económicas</h1>*/
/*         </div>    */
/*     </body>*/
/* </html>*/
