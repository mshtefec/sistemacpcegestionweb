<?php

/* SistemaCPCEBundle:Default:index.html.twig */
class __TwigTemplate_6d80ba695a1de98f6c5a6d38ab4081d30aa2a83d0ed601a63cdc965ab4fbd0ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaUserBundle::layout.html.twig", "SistemaCPCEBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'fos_user_panel' => array($this, 'block_fos_user_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b330a0a67816f08fcd552848e312f58fb429ca65a7d5402bd79447ce45d6f58d = $this->env->getExtension("native_profiler");
        $__internal_b330a0a67816f08fcd552848e312f58fb429ca65a7d5402bd79447ce45d6f58d->enter($__internal_b330a0a67816f08fcd552848e312f58fb429ca65a7d5402bd79447ce45d6f58d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b330a0a67816f08fcd552848e312f58fb429ca65a7d5402bd79447ce45d6f58d->leave($__internal_b330a0a67816f08fcd552848e312f58fb429ca65a7d5402bd79447ce45d6f58d_prof);

    }

    // line 3
    public function block_fos_user_panel($context, array $blocks = array())
    {
        $__internal_5801173b03e07897a0e4fab7c80b0690a718bb4520f7163940be6483c94465b5 = $this->env->getExtension("native_profiler");
        $__internal_5801173b03e07897a0e4fab7c80b0690a718bb4520f7163940be6483c94465b5->enter($__internal_5801173b03e07897a0e4fab7c80b0690a718bb4520f7163940be6483c94465b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_panel"));

        // line 4
        echo "<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <div class=\"panel-title\">Sistema Web de Gesti&oacute;n para Matriculados.</div>
    </div>
    <div style=\"padding-top:30px\" class=\"panel-body\">
    \t<a class=\"btn btn-info\" href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("front_dato");
        echo "\">Datos</a>
    \t<a class=\"btn btn-info\" href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("front_cuenta");
        echo "\">Estado de Cuenta</a>
        <a class=\"btn btn-info\" href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("front_trabajo");
        echo "\">Listado de trabajos</a>
        <a class=\"btn btn-success\" href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("front_trabajo_new");
        echo "\">Confeccionar trabajo</a>
    </div>
</div>
<div class=\"panel panel-primary\">
    ";
        // line 16
        $this->loadTemplate("SistemaCPCEBundle:Ayuda:novedad.html.twig", "SistemaCPCEBundle:Default:index.html.twig", 16)->display(array());
        // line 17
        echo "</div>
";
        
        $__internal_5801173b03e07897a0e4fab7c80b0690a718bb4520f7163940be6483c94465b5->leave($__internal_5801173b03e07897a0e4fab7c80b0690a718bb4520f7163940be6483c94465b5_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 17,  66 => 16,  59 => 12,  55 => 11,  51 => 10,  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "SistemaUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_panel %}*/
/* <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/*         <div class="panel-title">Sistema Web de Gesti&oacute;n para Matriculados.</div>*/
/*     </div>*/
/*     <div style="padding-top:30px" class="panel-body">*/
/*     	<a class="btn btn-info" href="{{ path('front_dato') }}">Datos</a>*/
/*     	<a class="btn btn-info" href="{{ path('front_cuenta') }}">Estado de Cuenta</a>*/
/*         <a class="btn btn-info" href="{{ path('front_trabajo') }}">Listado de trabajos</a>*/
/*         <a class="btn btn-success" href="{{ path('front_trabajo_new') }}">Confeccionar trabajo</a>*/
/*     </div>*/
/* </div>*/
/* <div class="panel panel-primary">*/
/*     {% include 'SistemaCPCEBundle:Ayuda:novedad.html.twig' only %}*/
/* </div>*/
/* {% endblock fos_user_panel %}*/
