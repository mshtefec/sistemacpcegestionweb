<?php

/* SistemaCPCEBundle::layout.html.twig */
class __TwigTemplate_6f939a69ccefb48f7a9bb41a5c430a54123e1ec41b38dd2d489bfc92fcdcc8d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaUserBundle::layout.html.twig", "SistemaCPCEBundle::layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'messages_error' => array($this, 'block_messages_error'),
            'fos_user_panel' => array($this, 'block_fos_user_panel'),
            'paneltitle' => array($this, 'block_paneltitle'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'page' => array($this, 'block_page'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c016b67e2824394cd531ba744a49e3194eab7e2820129c30be925909df332b10 = $this->env->getExtension("native_profiler");
        $__internal_c016b67e2824394cd531ba744a49e3194eab7e2820129c30be925909df332b10->enter($__internal_c016b67e2824394cd531ba744a49e3194eab7e2820129c30be925909df332b10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c016b67e2824394cd531ba744a49e3194eab7e2820129c30be925909df332b10->leave($__internal_c016b67e2824394cd531ba744a49e3194eab7e2820129c30be925909df332b10_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_83ddb78850ba1558690b54587e344db3741beb71c4481d6fdd0c09fb7f6b2607 = $this->env->getExtension("native_profiler");
        $__internal_83ddb78850ba1558690b54587e344db3741beb71c4481d6fdd0c09fb7f6b2607->enter($__internal_83ddb78850ba1558690b54587e344db3741beb71c4481d6fdd0c09fb7f6b2607_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.layout.bannersadmin", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        
        $__internal_83ddb78850ba1558690b54587e344db3741beb71c4481d6fdd0c09fb7f6b2607->leave($__internal_83ddb78850ba1558690b54587e344db3741beb71c4481d6fdd0c09fb7f6b2607_prof);

    }

    // line 5
    public function block_messages_error($context, array $blocks = array())
    {
        $__internal_92ae44c29d932e810c4fb6d11540eb90b749c961646266ce4f26fff715414950 = $this->env->getExtension("native_profiler");
        $__internal_92ae44c29d932e810c4fb6d11540eb90b749c961646266ce4f26fff715414950->enter($__internal_92ae44c29d932e810c4fb6d11540eb90b749c961646266ce4f26fff715414950_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "messages_error"));

        // line 6
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["flashMessages"]) {
            // line 7
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["flashMessages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
                // line 8
                echo "            <div class=\"alert alert-";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                ";
                // line 9
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["flashMessage"], array(), "MWSimpleAdminCrudBundle"), "html", null, true);
                echo "
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashMessages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_92ae44c29d932e810c4fb6d11540eb90b749c961646266ce4f26fff715414950->leave($__internal_92ae44c29d932e810c4fb6d11540eb90b749c961646266ce4f26fff715414950_prof);

    }

    // line 15
    public function block_fos_user_panel($context, array $blocks = array())
    {
        $__internal_2dd3db71e708f27865d0fcacb001e4627fd5c7f887b6e01a7aecac5b66ab2d84 = $this->env->getExtension("native_profiler");
        $__internal_2dd3db71e708f27865d0fcacb001e4627fd5c7f887b6e01a7aecac5b66ab2d84->enter($__internal_2dd3db71e708f27865d0fcacb001e4627fd5c7f887b6e01a7aecac5b66ab2d84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_panel"));

        // line 16
        echo "    <div class=\"panel-heading\">
        <div class=\"panel-title\">
            ";
        // line 18
        $this->displayBlock('paneltitle', $context, $blocks);
        // line 19
        echo "        </div>
    </div>
    <div class=\"panel-body\">
        ";
        // line 22
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 23
        echo "        ";
        $this->displayBlock('page', $context, $blocks);
        // line 24
        echo "    </div>
";
        
        $__internal_2dd3db71e708f27865d0fcacb001e4627fd5c7f887b6e01a7aecac5b66ab2d84->leave($__internal_2dd3db71e708f27865d0fcacb001e4627fd5c7f887b6e01a7aecac5b66ab2d84_prof);

    }

    // line 18
    public function block_paneltitle($context, array $blocks = array())
    {
        $__internal_8965ab1c0fc255287a4aef65d1ba1c710f952489d89a0bf36c915206cea2b763 = $this->env->getExtension("native_profiler");
        $__internal_8965ab1c0fc255287a4aef65d1ba1c710f952489d89a0bf36c915206cea2b763->enter($__internal_8965ab1c0fc255287a4aef65d1ba1c710f952489d89a0bf36c915206cea2b763_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "paneltitle"));

        echo "Sistema Web de Gesti&oacute;n para Matriculados.";
        
        $__internal_8965ab1c0fc255287a4aef65d1ba1c710f952489d89a0bf36c915206cea2b763->leave($__internal_8965ab1c0fc255287a4aef65d1ba1c710f952489d89a0bf36c915206cea2b763_prof);

    }

    // line 22
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2c6348880d8048255db27c6552f3d1da237823d4019ba14dcfb9945c46d57b97 = $this->env->getExtension("native_profiler");
        $__internal_2c6348880d8048255db27c6552f3d1da237823d4019ba14dcfb9945c46d57b97->enter($__internal_2c6348880d8048255db27c6552f3d1da237823d4019ba14dcfb9945c46d57b97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_2c6348880d8048255db27c6552f3d1da237823d4019ba14dcfb9945c46d57b97->leave($__internal_2c6348880d8048255db27c6552f3d1da237823d4019ba14dcfb9945c46d57b97_prof);

    }

    // line 23
    public function block_page($context, array $blocks = array())
    {
        $__internal_cad4ef9235a8f5f52f718710d9cd91aabe6644a77fca927281c26c8978be957e = $this->env->getExtension("native_profiler");
        $__internal_cad4ef9235a8f5f52f718710d9cd91aabe6644a77fca927281c26c8978be957e->enter($__internal_cad4ef9235a8f5f52f718710d9cd91aabe6644a77fca927281c26c8978be957e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page"));

        
        $__internal_cad4ef9235a8f5f52f718710d9cd91aabe6644a77fca927281c26c8978be957e->leave($__internal_cad4ef9235a8f5f52f718710d9cd91aabe6644a77fca927281c26c8978be957e_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 23,  134 => 22,  122 => 18,  114 => 24,  111 => 23,  109 => 22,  104 => 19,  102 => 18,  98 => 16,  92 => 15,  81 => 12,  72 => 9,  67 => 8,  62 => 7,  57 => 6,  51 => 5,  39 => 3,  11 => 1,);
    }
}
/* {% extends "SistemaUserBundle::layout.html.twig" %}*/
/* */
/* {% block title %}{{ 'views.layout.bannersadmin'|trans({}, 'MWSimpleAdminCrudBundle') }}{% endblock %}*/
/* */
/* {% block messages_error %}*/
/*     {% for type, flashMessages in app.session.flashbag.all() %}*/
/*         {% for flashMessage in flashMessages %}*/
/*             <div class="alert alert-{{ type }}">*/
/*                 {{ flashMessage|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*             </div>*/
/*         {% endfor %}*/
/*     {% endfor %}*/
/* {% endblock messages_error %}*/
/* */
/* {% block fos_user_panel %}*/
/*     <div class="panel-heading">*/
/*         <div class="panel-title">*/
/*             {% block paneltitle %}Sistema Web de Gesti&oacute;n para Matriculados.{% endblock %}*/
/*         </div>*/
/*     </div>*/
/*     <div class="panel-body">*/
/*         {% block fos_user_content %}{% endblock %}*/
/*         {% block page %}{% endblock %}*/
/*     </div>*/
/* {% endblock fos_user_panel %}*/
