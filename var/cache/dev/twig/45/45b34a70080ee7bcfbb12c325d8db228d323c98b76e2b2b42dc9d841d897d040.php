<?php

/* SistemaCPCEBundle:Default:indexTrabajo.html.twig */
class __TwigTemplate_7281882fa15b3fbc9d8cdc73c068695506b9a1d5ab9e2f4d0f09c030c8bdea68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaCPCEBundle::longLayout.html.twig", "SistemaCPCEBundle:Default:indexTrabajo.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'paneltitle' => array($this, 'block_paneltitle'),
            'page' => array($this, 'block_page'),
            'actions' => array($this, 'block_actions'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaCPCEBundle::longLayout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c498c9a61a36f7c5b2f43c18cd88519aa50492be38b9a82af2b47b1df36c4e6d = $this->env->getExtension("native_profiler");
        $__internal_c498c9a61a36f7c5b2f43c18cd88519aa50492be38b9a82af2b47b1df36c4e6d->enter($__internal_c498c9a61a36f7c5b2f43c18cd88519aa50492be38b9a82af2b47b1df36c4e6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Default:indexTrabajo.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c498c9a61a36f7c5b2f43c18cd88519aa50492be38b9a82af2b47b1df36c4e6d->leave($__internal_c498c9a61a36f7c5b2f43c18cd88519aa50492be38b9a82af2b47b1df36c4e6d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_3cc1efaae21e68b1a0c2993c34ffdd73d6570fff6733170722512a98e69f6795 = $this->env->getExtension("native_profiler");
        $__internal_3cc1efaae21e68b1a0c2993c34ffdd73d6570fff6733170722512a98e69f6795->enter($__internal_3cc1efaae21e68b1a0c2993c34ffdd73d6570fff6733170722512a98e69f6795_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Listado de ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array()), "html", null, true);
        echo "s
";
        
        $__internal_3cc1efaae21e68b1a0c2993c34ffdd73d6570fff6733170722512a98e69f6795->leave($__internal_3cc1efaae21e68b1a0c2993c34ffdd73d6570fff6733170722512a98e69f6795_prof);

    }

    // line 7
    public function block_paneltitle($context, array $blocks = array())
    {
        $__internal_54ae182dce04ca824667b6b1c015eae0d6dda72274c1ec027fd90005064b46ca = $this->env->getExtension("native_profiler");
        $__internal_54ae182dce04ca824667b6b1c015eae0d6dda72274c1ec027fd90005064b46ca->enter($__internal_54ae182dce04ca824667b6b1c015eae0d6dda72274c1ec027fd90005064b46ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "paneltitle"));

        // line 8
        echo "    Listado de ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array()), "html", null, true);
        echo "s
";
        
        $__internal_54ae182dce04ca824667b6b1c015eae0d6dda72274c1ec027fd90005064b46ca->leave($__internal_54ae182dce04ca824667b6b1c015eae0d6dda72274c1ec027fd90005064b46ca_prof);

    }

    // line 11
    public function block_page($context, array $blocks = array())
    {
        $__internal_5f724bae4e90f9f5ecec07e340fd3762c3d14343591ff0876a218cfd5ed98e13 = $this->env->getExtension("native_profiler");
        $__internal_5f724bae4e90f9f5ecec07e340fd3762c3d14343591ff0876a218cfd5ed98e13->enter($__internal_5f724bae4e90f9f5ecec07e340fd3762c3d14343591ff0876a218cfd5ed98e13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page"));

        // line 12
        $this->env->getExtension('form')->renderer->setTheme((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), array(0 => "MWSimpleAdminCrudBundle:widget:fields.html.twig"));
        // line 13
        echo "<div class=\"row\">
    <div class=\"col-lg-12\">
        <div class=\"filters-right\">
            <a class=\"btn btn-success\" href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "new", array()));
        echo "\">
                Confeccionar ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array()), "html", null, true);
        echo "
            </a>
            <!-- Single button -->
            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-info dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                    ";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.export", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo " <span class=\"caret\"></span>
                </button>
              <ul class=\"dropdown-menu\" role=\"menu\">
                <li><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "xls"));
        echo "\">EXCEL</a></li>
                ";
        // line 28
        echo "              </ul>
            </div>
            <a id=\"reset_index_filters\" class=\"btn btn-danger\">
                Limpiar Filtros
            </a>
            <a class=\"btn btn-primary dropdown-toggle\" data-toggle=\"collapse\" data-target=\"#filters\">
                ";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.filters", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
                <span class=\"caret\"></span>
            </a>
        </div>
    </div>
    <div id=\"filters\" class=\"well collapse col-lg-12 col-md-12 col-sm-12\">
        ";
        // line 40
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), 'form_start');
        echo "
            ";
        // line 47
        echo "        ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), 'form_end');
        echo "
    </div>
</div>
<div class=\"row\">
    <div>
        <table class=\"table table-striped table-bordered table-condensed table-responsive\">
            <thead>
                <tr>
                    ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "fieldsindex", array()));
        foreach ($context['_seq'] as $context["key"] => $context["field"]) {
            // line 56
            echo "                        <th";
            if ($this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "isSorted", array(0 => $context["key"]), "method")) {
                echo " class=\"sorted\"";
            }
            echo ">";
            echo $this->env->getExtension('knp_pagination')->sortable($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), $this->getAttribute($context["field"], "label", array()), $context["key"]);
            echo "</th>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                    <th>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.actions", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 62
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 63
            echo "                <tr>
                    ";
            // line 64
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "fieldsindex", array()));
            foreach ($context['_seq'] as $context["key"] => $context["field"]) {
                // line 65
                echo "                        <td>
                        ";
                // line 66
                if (twig_in_filter($this->getAttribute($context["field"], "type", array()), array(0 => "datetime", 1 => "datetimetz", 2 => "date", 3 => "time"))) {
                    // line 67
                    echo "                            ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array())), $this->getAttribute($context["field"], "date", array())), "html", null, true);
                    echo "
                        ";
                } elseif (twig_in_filter($this->getAttribute(                // line 68
$context["field"], "type", array()), array(0 => "boolean"))) {
                    // line 69
                    echo "                            ";
                    echo $this->env->getExtension('twig.extension')->isActive($this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array())));
                    echo "
                        ";
                } elseif (twig_in_filter($this->getAttribute(                // line 70
$context["field"], "type", array()), array(0 => "ONE_TO_MANY", 1 => "MANY_TO_MANY"))) {
                    // line 71
                    echo "                            ";
                    $context["many_entity"] = $this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array()));
                    // line 72
                    echo "                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["many_entity"]) ? $context["many_entity"] : $this->getContext($context, "many_entity")));
                    foreach ($context['_seq'] as $context["key"] => $context["field_many"]) {
                        // line 73
                        echo "                                ";
                        echo $context["field_many"];
                        echo "
                                ";
                        // line 74
                        if ($this->getAttribute($context["field"], "separator", array(), "any", true, true)) {
                            // line 75
                            echo "                                    ";
                            echo $this->getAttribute($context["field"], "separator", array());
                            echo "
                                ";
                        } else {
                            // line 77
                            echo "                                    |
                                ";
                        }
                        // line 79
                        echo "                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['field_many'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 80
                    echo "                        ";
                } else {
                    // line 81
                    echo "                            ";
                    echo $this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array()));
                    echo "
                        ";
                }
                // line 83
                echo "                        </td>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "                    <td>
                        ";
            // line 86
            $this->displayBlock('actions', $context, $blocks);
            // line 91
            echo "                    </td>
                </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "            </tbody>
        </table>
    </div>
    <div class=\"col-lg-4 navigation\">
        ";
        // line 98
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        echo "
    </div>
        <div class=\"col-lg-8\">
            <a class=\"btn btn-success likepaginator\" href=\"";
        // line 101
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "new", array()));
        echo "\">
                Confeccionar ";
        // line 102
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array()), "html", null, true);
        echo "
            </a>
        </div>
    </div>
</div>
";
        
        $__internal_5f724bae4e90f9f5ecec07e340fd3762c3d14343591ff0876a218cfd5ed98e13->leave($__internal_5f724bae4e90f9f5ecec07e340fd3762c3d14343591ff0876a218cfd5ed98e13_prof);

    }

    // line 86
    public function block_actions($context, array $blocks = array())
    {
        $__internal_1aa1adfa2270633430faad8442b07da192b0471e046a4d5a2fd707ce7b189455 = $this->env->getExtension("native_profiler");
        $__internal_1aa1adfa2270633430faad8442b07da192b0471e046a4d5a2fd707ce7b189455->enter($__internal_1aa1adfa2270633430faad8442b07da192b0471e046a4d5a2fd707ce7b189455_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 87
        echo "                            <a class=\"glyphicon glyphicon-search tooltips\"  href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "show", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.actions.show", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "\"></a>
                            ";
        // line 89
        echo "                            <a class=\"glyphicon glyphicon-print tooltips\"  href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "pdf", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" title=\"generar PDF\" rel=\"tooltip\" data-original-title=\"PDF\"></a>
                        ";
        
        $__internal_1aa1adfa2270633430faad8442b07da192b0471e046a4d5a2fd707ce7b189455->leave($__internal_1aa1adfa2270633430faad8442b07da192b0471e046a4d5a2fd707ce7b189455_prof);

    }

    // line 109
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_4dec0f488ac33dd3d916805a236b38e9af229cdb651e77e4c6a71b9a7e8f78c4 = $this->env->getExtension("native_profiler");
        $__internal_4dec0f488ac33dd3d916805a236b38e9af229cdb651e77e4c6a71b9a7e8f78c4->enter($__internal_4dec0f488ac33dd3d916805a236b38e9af229cdb651e77e4c6a71b9a7e8f78c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 110
        echo "    <script type=\"text/javascript\">
        \$('.tooltips').tooltip();
        \$( \"#reset_index_filters\" ).click(function() {
            \$(\".reset_submit_filters\").trigger(\"click\");
        });
    </script>
";
        
        $__internal_4dec0f488ac33dd3d916805a236b38e9af229cdb651e77e4c6a71b9a7e8f78c4->leave($__internal_4dec0f488ac33dd3d916805a236b38e9af229cdb651e77e4c6a71b9a7e8f78c4_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Default:indexTrabajo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 110,  328 => 109,  318 => 89,  311 => 87,  305 => 86,  292 => 102,  288 => 101,  282 => 98,  276 => 94,  260 => 91,  258 => 86,  255 => 85,  248 => 83,  242 => 81,  239 => 80,  233 => 79,  229 => 77,  223 => 75,  221 => 74,  216 => 73,  211 => 72,  208 => 71,  206 => 70,  201 => 69,  199 => 68,  194 => 67,  192 => 66,  189 => 65,  185 => 64,  182 => 63,  165 => 62,  157 => 58,  144 => 56,  140 => 55,  128 => 47,  124 => 40,  115 => 34,  107 => 28,  103 => 25,  97 => 22,  89 => 17,  85 => 16,  80 => 13,  78 => 12,  72 => 11,  62 => 8,  56 => 7,  44 => 4,  38 => 3,  11 => 1,);
    }
}
/* {% extends 'SistemaCPCEBundle::longLayout.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }} - Listado de {{ config.entityName }}s*/
/* {% endblock %}*/
/* */
/* {% block paneltitle %}*/
/*     Listado de {{ config.entityName }}s*/
/* {% endblock %}*/
/* */
/* {% block page %}*/
/* {% form_theme filterForm 'MWSimpleAdminCrudBundle:widget:fields.html.twig' %}*/
/* <div class="row">*/
/*     <div class="col-lg-12">*/
/*         <div class="filters-right">*/
/*             <a class="btn btn-success" href="{{ path(config.new) }}">*/
/*                 Confeccionar {{ config.entityName }}*/
/*             </a>*/
/*             <!-- Single button -->*/
/*             <div class="btn-group">*/
/*                 <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">*/
/*                     {{ 'views.index.export'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }} <span class="caret"></span>*/
/*                 </button>*/
/*               <ul class="dropdown-menu" role="menu">*/
/*                 <li><a href="{{ path(config.export, { 'format': "xls" }) }}">EXCEL</a></li>*/
/*                 {#<li><a href="{{ path(config.export, { 'format': "csv" }) }}">CSV</a></li>*/
/*                 <li><a href="{{ path(config.export, { 'format': "json" }) }}">JSON</a></li>#}*/
/*               </ul>*/
/*             </div>*/
/*             <a id="reset_index_filters" class="btn btn-danger">*/
/*                 Limpiar Filtros*/
/*             </a>*/
/*             <a class="btn btn-primary dropdown-toggle" data-toggle="collapse" data-target="#filters">*/
/*                 {{ 'views.index.filters'|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*                 <span class="caret"></span>*/
/*             </a>*/
/*         </div>*/
/*     </div>*/
/*     <div id="filters" class="well collapse col-lg-12 col-md-12 col-sm-12">*/
/*         {{ form_start(filterForm) }}*/
/*             {#{ form_label(filterForm.clienteComitente) }}*/
/*             {{ form_widget(filterForm.clienteComitente) }}*/
/*             {{ form_label(filterForm.comitenteCuit) }}*/
/*             {{ form_widget(filterForm.comitenteCuit) }}*/
/*             {{ form_label(filterForm.fechaInforme) }}*/
/*             {{ form_widget(filterForm.fechaInforme) }#}*/
/*         {{ form_end(filterForm) }}*/
/*     </div>*/
/* </div>*/
/* <div class="row">*/
/*     <div>*/
/*         <table class="table table-striped table-bordered table-condensed table-responsive">*/
/*             <thead>*/
/*                 <tr>*/
/*                     {% for key, field in config.fieldsindex %}*/
/*                         <th{% if entities.isSorted(key) %} class="sorted"{% endif %}>{{ knp_pagination_sortable(entities, field.label, key) }}</th>*/
/*                     {% endfor %}*/
/*                     <th>{{ 'views.index.actions'|trans({}, 'MWSimpleAdminCrudBundle') }}</th>*/
/*                 </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for entity in entities %}*/
/*                 <tr>*/
/*                     {% for key, field in config.fieldsindex %}*/
/*                         <td>*/
/*                         {% if field.type in ['datetime', 'datetimetz', 'date', 'time'] %}*/
/*                             {{ attribute(entity, field.name)|date(field.date) }}*/
/*                         {% elseif field.type in ['boolean'] %}*/
/*                             {{ isActive(attribute(entity, field.name)) }}*/
/*                         {% elseif field.type in ['ONE_TO_MANY', 'MANY_TO_MANY'] %}*/
/*                             {% set many_entity = attribute(entity, field.name) %}*/
/*                             {% for key, field_many in many_entity %}*/
/*                                 {{ field_many|raw }}*/
/*                                 {% if field.separator is defined %}*/
/*                                     {{ field.separator|raw }}*/
/*                                 {% else %}*/
/*                                     |*/
/*                                 {% endif %}*/
/*                             {% endfor %}*/
/*                         {% else %}*/
/*                             {{ attribute(entity, field.name)|raw }}*/
/*                         {% endif %}*/
/*                         </td>*/
/*                     {% endfor %}*/
/*                     <td>*/
/*                         {% block actions %}*/
/*                             <a class="glyphicon glyphicon-search tooltips"  href="{{ path(config.show, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.show'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>*/
/*                             {#<a class="glyphicon glyphicon-edit tooltips"  href="{{ path(config.edit, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.edit'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>#}*/
/*                             <a class="glyphicon glyphicon-print tooltips"  href="{{ path(config.pdf, { 'id': entity.id }) }}" title="generar PDF" rel="tooltip" data-original-title="PDF"></a>*/
/*                         {% endblock %}*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div>*/
/*     <div class="col-lg-4 navigation">*/
/*         {{ knp_pagination_render(entities) }}*/
/*     </div>*/
/*         <div class="col-lg-8">*/
/*             <a class="btn btn-success likepaginator" href="{{ path(config.new) }}">*/
/*                 Confeccionar {{ config.entityName }}*/
/*             </a>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* {% block javascript %}*/
/*     <script type="text/javascript">*/
/*         $('.tooltips').tooltip();*/
/*         $( "#reset_index_filters" ).click(function() {*/
/*             $(".reset_submit_filters").trigger("click");*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
