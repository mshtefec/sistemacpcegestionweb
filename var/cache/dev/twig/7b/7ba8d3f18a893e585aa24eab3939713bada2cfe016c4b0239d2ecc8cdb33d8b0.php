<?php

/* knp_menu_base.html.twig */
class __TwigTemplate_23c94f792b48edaecff9ec769fa8c40e8d9293fb7f98ecb9456fa03a1beeef34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3ff2dcc97227e46e2db124d164665d4cd2c6c8ed282963a91bc252c7e11422e = $this->env->getExtension("native_profiler");
        $__internal_a3ff2dcc97227e46e2db124d164665d4cd2c6c8ed282963a91bc252c7e11422e->enter($__internal_a3ff2dcc97227e46e2db124d164665d4cd2c6c8ed282963a91bc252c7e11422e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu_base.html.twig"));

        // line 1
        if ($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "compressed", array())) {
            $this->displayBlock("compressed_root", $context, $blocks);
        } else {
            $this->displayBlock("root", $context, $blocks);
        }
        
        $__internal_a3ff2dcc97227e46e2db124d164665d4cd2c6c8ed282963a91bc252c7e11422e->leave($__internal_a3ff2dcc97227e46e2db124d164665d4cd2c6c8ed282963a91bc252c7e11422e_prof);

    }

    public function getTemplateName()
    {
        return "knp_menu_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% if options.compressed %}{{ block('compressed_root') }}{% else %}{{ block('root') }}{% endif %}*/
/* */
