<?php

/* SistemaCPCEBundle::layout.html.twig */
class __TwigTemplate_9452a2e8a38211eda73e21f03b01f0cf9967f8c8ad0cb525c14528b9f5ab20cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaUserBundle::layout.html.twig", "SistemaCPCEBundle::layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'messages_error' => array($this, 'block_messages_error'),
            'fos_user_panel' => array($this, 'block_fos_user_panel'),
            'paneltitle' => array($this, 'block_paneltitle'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'page' => array($this, 'block_page'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ac5c0e0342db93728d20a6ebf27a6aec5de6a223a8881068f59ec7dd5d6bd82 = $this->env->getExtension("native_profiler");
        $__internal_9ac5c0e0342db93728d20a6ebf27a6aec5de6a223a8881068f59ec7dd5d6bd82->enter($__internal_9ac5c0e0342db93728d20a6ebf27a6aec5de6a223a8881068f59ec7dd5d6bd82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ac5c0e0342db93728d20a6ebf27a6aec5de6a223a8881068f59ec7dd5d6bd82->leave($__internal_9ac5c0e0342db93728d20a6ebf27a6aec5de6a223a8881068f59ec7dd5d6bd82_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b895e023db7198289987aa5dcb74f357b7577f30010f4eeadbb9feaee13322e0 = $this->env->getExtension("native_profiler");
        $__internal_b895e023db7198289987aa5dcb74f357b7577f30010f4eeadbb9feaee13322e0->enter($__internal_b895e023db7198289987aa5dcb74f357b7577f30010f4eeadbb9feaee13322e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.layout.bannersadmin", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        
        $__internal_b895e023db7198289987aa5dcb74f357b7577f30010f4eeadbb9feaee13322e0->leave($__internal_b895e023db7198289987aa5dcb74f357b7577f30010f4eeadbb9feaee13322e0_prof);

    }

    // line 5
    public function block_messages_error($context, array $blocks = array())
    {
        $__internal_396b19c030cfb621a8f4699c21989daf63cbb6b803baf66e95eec5abbbd52353 = $this->env->getExtension("native_profiler");
        $__internal_396b19c030cfb621a8f4699c21989daf63cbb6b803baf66e95eec5abbbd52353->enter($__internal_396b19c030cfb621a8f4699c21989daf63cbb6b803baf66e95eec5abbbd52353_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "messages_error"));

        // line 6
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["flashMessages"]) {
            // line 7
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["flashMessages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
                // line 8
                echo "            <div class=\"alert alert-";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                ";
                // line 9
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["flashMessage"], array(), "MWSimpleAdminCrudBundle"), "html", null, true);
                echo "
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashMessages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_396b19c030cfb621a8f4699c21989daf63cbb6b803baf66e95eec5abbbd52353->leave($__internal_396b19c030cfb621a8f4699c21989daf63cbb6b803baf66e95eec5abbbd52353_prof);

    }

    // line 15
    public function block_fos_user_panel($context, array $blocks = array())
    {
        $__internal_2799399b3397b3c748145edd253b7dd77ad56f813656a37f72005e8c1b6296d8 = $this->env->getExtension("native_profiler");
        $__internal_2799399b3397b3c748145edd253b7dd77ad56f813656a37f72005e8c1b6296d8->enter($__internal_2799399b3397b3c748145edd253b7dd77ad56f813656a37f72005e8c1b6296d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_panel"));

        // line 16
        echo "    <div class=\"panel-heading\">
        <div class=\"panel-title\">
            ";
        // line 18
        $this->displayBlock('paneltitle', $context, $blocks);
        // line 19
        echo "        </div>
    </div>
    <div class=\"panel-body\">
        ";
        // line 22
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 23
        echo "        ";
        $this->displayBlock('page', $context, $blocks);
        // line 24
        echo "    </div>
";
        
        $__internal_2799399b3397b3c748145edd253b7dd77ad56f813656a37f72005e8c1b6296d8->leave($__internal_2799399b3397b3c748145edd253b7dd77ad56f813656a37f72005e8c1b6296d8_prof);

    }

    // line 18
    public function block_paneltitle($context, array $blocks = array())
    {
        $__internal_11e968a319e75ea93e668bd1a6760575cbb6afd8d4539cedf3635036d820b100 = $this->env->getExtension("native_profiler");
        $__internal_11e968a319e75ea93e668bd1a6760575cbb6afd8d4539cedf3635036d820b100->enter($__internal_11e968a319e75ea93e668bd1a6760575cbb6afd8d4539cedf3635036d820b100_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "paneltitle"));

        echo "Sistema Web de Gesti&oacute;n para Matriculados.";
        
        $__internal_11e968a319e75ea93e668bd1a6760575cbb6afd8d4539cedf3635036d820b100->leave($__internal_11e968a319e75ea93e668bd1a6760575cbb6afd8d4539cedf3635036d820b100_prof);

    }

    // line 22
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3b5f6994b35be58d2a7b92f8f455a168f767b6114cc6dfdac94eec56722affa3 = $this->env->getExtension("native_profiler");
        $__internal_3b5f6994b35be58d2a7b92f8f455a168f767b6114cc6dfdac94eec56722affa3->enter($__internal_3b5f6994b35be58d2a7b92f8f455a168f767b6114cc6dfdac94eec56722affa3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_3b5f6994b35be58d2a7b92f8f455a168f767b6114cc6dfdac94eec56722affa3->leave($__internal_3b5f6994b35be58d2a7b92f8f455a168f767b6114cc6dfdac94eec56722affa3_prof);

    }

    // line 23
    public function block_page($context, array $blocks = array())
    {
        $__internal_d09e1763c8e5dc23fc5947a4b8f9f2c1c4bbeaa85c3d0e216891e3b84032254e = $this->env->getExtension("native_profiler");
        $__internal_d09e1763c8e5dc23fc5947a4b8f9f2c1c4bbeaa85c3d0e216891e3b84032254e->enter($__internal_d09e1763c8e5dc23fc5947a4b8f9f2c1c4bbeaa85c3d0e216891e3b84032254e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page"));

        
        $__internal_d09e1763c8e5dc23fc5947a4b8f9f2c1c4bbeaa85c3d0e216891e3b84032254e->leave($__internal_d09e1763c8e5dc23fc5947a4b8f9f2c1c4bbeaa85c3d0e216891e3b84032254e_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 23,  134 => 22,  122 => 18,  114 => 24,  111 => 23,  109 => 22,  104 => 19,  102 => 18,  98 => 16,  92 => 15,  81 => 12,  72 => 9,  67 => 8,  62 => 7,  57 => 6,  51 => 5,  39 => 3,  11 => 1,);
    }
}
/* {% extends "SistemaUserBundle::layout.html.twig" %}*/
/* */
/* {% block title %}{{ 'views.layout.bannersadmin'|trans({}, 'MWSimpleAdminCrudBundle') }}{% endblock %}*/
/* */
/* {% block messages_error %}*/
/*     {% for type, flashMessages in app.session.flashbag.all() %}*/
/*         {% for flashMessage in flashMessages %}*/
/*             <div class="alert alert-{{ type }}">*/
/*                 {{ flashMessage|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*             </div>*/
/*         {% endfor %}*/
/*     {% endfor %}*/
/* {% endblock messages_error %}*/
/* */
/* {% block fos_user_panel %}*/
/*     <div class="panel-heading">*/
/*         <div class="panel-title">*/
/*             {% block paneltitle %}Sistema Web de Gesti&oacute;n para Matriculados.{% endblock %}*/
/*         </div>*/
/*     </div>*/
/*     <div class="panel-body">*/
/*         {% block fos_user_content %}{% endblock %}*/
/*         {% block page %}{% endblock %}*/
/*     </div>*/
/* {% endblock fos_user_panel %}*/
