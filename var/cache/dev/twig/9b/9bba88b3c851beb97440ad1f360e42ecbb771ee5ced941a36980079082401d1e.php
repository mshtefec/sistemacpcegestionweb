<?php

/* SistemaCPCEBundle::longLayout.html.twig */
class __TwigTemplate_cbe63def68b354ca9eca759391cbdb41ca0c31887432628907d6594a0c429e4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaCPCEBundle::layout.html.twig", "SistemaCPCEBundle::longLayout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaCPCEBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd8661737f678ed528c4d519e48240507bbc6c69c4899b9bd1d7cdc11fc2066b = $this->env->getExtension("native_profiler");
        $__internal_cd8661737f678ed528c4d519e48240507bbc6c69c4899b9bd1d7cdc11fc2066b->enter($__internal_cd8661737f678ed528c4d519e48240507bbc6c69c4899b9bd1d7cdc11fc2066b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle::longLayout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cd8661737f678ed528c4d519e48240507bbc6c69c4899b9bd1d7cdc11fc2066b->leave($__internal_cd8661737f678ed528c4d519e48240507bbc6c69c4899b9bd1d7cdc11fc2066b_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_39536a9c0d71e560f15c4f3ceeb0c335a19d0eebaffd365f062b8ce9e947d331 = $this->env->getExtension("native_profiler");
        $__internal_39536a9c0d71e560f15c4f3ceeb0c335a19d0eebaffd365f062b8ce9e947d331->enter($__internal_39536a9c0d71e560f15c4f3ceeb0c335a19d0eebaffd365f062b8ce9e947d331_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style type=\"text/css\">
\t\t@media (min-width: 769px) {
\t\t    .content {
\t\t        margin: 0 3em;
\t\t    }
\t\t}
    </style>
";
        
        $__internal_39536a9c0d71e560f15c4f3ceeb0c335a19d0eebaffd365f062b8ce9e947d331->leave($__internal_39536a9c0d71e560f15c4f3ceeb0c335a19d0eebaffd365f062b8ce9e947d331_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle::longLayout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'SistemaCPCEBundle::layout.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <style type="text/css">*/
/* 		@media (min-width: 769px) {*/
/* 		    .content {*/
/* 		        margin: 0 3em;*/
/* 		    }*/
/* 		}*/
/*     </style>*/
/* {% endblock %}*/
