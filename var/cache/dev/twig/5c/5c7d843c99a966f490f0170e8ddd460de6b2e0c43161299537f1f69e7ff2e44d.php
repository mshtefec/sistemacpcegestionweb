<?php

/* FOSUserBundle:User:index.html.twig */
class __TwigTemplate_db46d0b498c7768293c4d51d9f29dae082f01f642d67dade20a0f77c9ac336c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MWSimpleAdminCrudBundle:Default:index.html.twig", "FOSUserBundle:User:index.html.twig", 1);
        $this->blocks = array(
            'buttons' => array($this, 'block_buttons'),
            'actions' => array($this, 'block_actions'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MWSimpleAdminCrudBundle:Default:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9984ecaee5f4cad16d985266208ee44e44541f8da88e2e66d44f23aceab2866a = $this->env->getExtension("native_profiler");
        $__internal_9984ecaee5f4cad16d985266208ee44e44541f8da88e2e66d44f23aceab2866a->enter($__internal_9984ecaee5f4cad16d985266208ee44e44541f8da88e2e66d44f23aceab2866a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:User:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9984ecaee5f4cad16d985266208ee44e44541f8da88e2e66d44f23aceab2866a->leave($__internal_9984ecaee5f4cad16d985266208ee44e44541f8da88e2e66d44f23aceab2866a_prof);

    }

    // line 3
    public function block_buttons($context, array $blocks = array())
    {
        $__internal_eeb3b45ab2d3d5779bf22294ba0d5b343e752941564f275aa8a6e10c372fd4a9 = $this->env->getExtension("native_profiler");
        $__internal_eeb3b45ab2d3d5779bf22294ba0d5b343e752941564f275aa8a6e10c372fd4a9->enter($__internal_eeb3b45ab2d3d5779bf22294ba0d5b343e752941564f275aa8a6e10c372fd4a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttons"));

        // line 4
        echo "    <a class=\"btn btn-success\" href=\"";
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "new", array()));
        echo "\">
        ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.createnew", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
    </a>
    <div class=\"btn-group\">
        <button type=\"button\" class=\"btn btn-info dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
            ";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.export", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo " <span class=\"caret\"></span>
        </button>
      <ul class=\"dropdown-menu\" role=\"menu\">
        <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "xls"));
        echo "\">EXCEL</a></li>
        <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "csv"));
        echo "\">CSV</a></li>
        <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "json"));
        echo "\">JSON</a></li>
      </ul>
    </div>
    <a id=\"reset_index_filters\" class=\"btn btn-danger\">
        ";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.reset", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
    </a>
    <a class=\"btn btn-primary dropdown-toggle\" data-toggle=\"collapse\" data-target=\"#filters\">
        ";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.filters", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
        <span class=\"caret\"></span>
    </a>
    <a class=\"btn btn-info\" href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("admin_user_correos_confirmacion");
        echo "\" onclick=\"return confirm('Esta seguro de reenviar correos a los matriculados no activos?');\">
        Reenviar Correos Matriculados No Activos
    </a>
";
        
        $__internal_eeb3b45ab2d3d5779bf22294ba0d5b343e752941564f275aa8a6e10c372fd4a9->leave($__internal_eeb3b45ab2d3d5779bf22294ba0d5b343e752941564f275aa8a6e10c372fd4a9_prof);

    }

    // line 29
    public function block_actions($context, array $blocks = array())
    {
        $__internal_d5bdb07eaeef245203b2035bde3a055cf07a8fb97356d190f0424f778626edda = $this->env->getExtension("native_profiler");
        $__internal_d5bdb07eaeef245203b2035bde3a055cf07a8fb97356d190f0424f778626edda->enter($__internal_d5bdb07eaeef245203b2035bde3a055cf07a8fb97356d190f0424f778626edda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 30
        echo "    <a class=\"glyphicon glyphicon-search tooltips\"  href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "show", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.actions.show", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "\"></a>
    <a class=\"glyphicon glyphicon-edit tooltips\"  href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "edit", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.actions.edit", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "\"></a>
    ";
        // line 32
        if (!twig_in_filter("ROLE_MATRICULADO", $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getRoles", array(), "method"))) {
            // line 33
            echo "        <a class=\"glyphicon glyphicon-ok tooltips\"  href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_activar_role", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\" title=\"Activar como matriculado\" rel=\"tooltip\" data-original-title=\"Activar como matriculado\"></a>
    ";
        }
        
        $__internal_d5bdb07eaeef245203b2035bde3a055cf07a8fb97356d190f0424f778626edda->leave($__internal_d5bdb07eaeef245203b2035bde3a055cf07a8fb97356d190f0424f778626edda_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 33,  116 => 32,  110 => 31,  103 => 30,  97 => 29,  86 => 24,  80 => 21,  74 => 18,  67 => 14,  63 => 13,  59 => 12,  53 => 9,  46 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'MWSimpleAdminCrudBundle:Default:index.html.twig' %}*/
/* */
/* {% block buttons %}*/
/*     <a class="btn btn-success" href="{{ path(config.new) }}">*/
/*         {{ 'views.index.createnew'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}*/
/*     </a>*/
/*     <div class="btn-group">*/
/*         <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">*/
/*             {{ 'views.index.export'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }} <span class="caret"></span>*/
/*         </button>*/
/*       <ul class="dropdown-menu" role="menu">*/
/*         <li><a href="{{ path(config.export, { 'format': "xls" }) }}">EXCEL</a></li>*/
/*         <li><a href="{{ path(config.export, { 'format': "csv" }) }}">CSV</a></li>*/
/*         <li><a href="{{ path(config.export, { 'format': "json" }) }}">JSON</a></li>*/
/*       </ul>*/
/*     </div>*/
/*     <a id="reset_index_filters" class="btn btn-danger">*/
/*         {{ 'views.index.reset'|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*     </a>*/
/*     <a class="btn btn-primary dropdown-toggle" data-toggle="collapse" data-target="#filters">*/
/*         {{ 'views.index.filters'|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*         <span class="caret"></span>*/
/*     </a>*/
/*     <a class="btn btn-info" href="{{ path('admin_user_correos_confirmacion') }}" onclick="return confirm('Esta seguro de reenviar correos a los matriculados no activos?');">*/
/*         Reenviar Correos Matriculados No Activos*/
/*     </a>*/
/* {% endblock %}*/
/* */
/* {% block actions %}*/
/*     <a class="glyphicon glyphicon-search tooltips"  href="{{ path(config.show, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.show'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>*/
/*     <a class="glyphicon glyphicon-edit tooltips"  href="{{ path(config.edit, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.edit'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>*/
/*     {% if 'ROLE_MATRICULADO' not in entity.getRoles() %}*/
/*         <a class="glyphicon glyphicon-ok tooltips"  href="{{ path('admin_user_activar_role', { 'id': entity.id }) }}" title="Activar como matriculado" rel="tooltip" data-original-title="Activar como matriculado"></a>*/
/*     {% endif %}*/
/* {% endblock %}*/
