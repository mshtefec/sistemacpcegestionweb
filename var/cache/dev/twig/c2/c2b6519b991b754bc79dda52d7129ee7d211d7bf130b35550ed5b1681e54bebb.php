<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_0be8dd727ef69c61f67da0b2d688cc453b92d3072bfc734d523b2997fdd9d45d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_92102f1da271d811a811c240ad73d4bff7dc85f9abea9f0ef82d5368604b6240 = $this->env->getExtension("native_profiler");
        $__internal_92102f1da271d811a811c240ad73d4bff7dc85f9abea9f0ef82d5368604b6240->enter($__internal_92102f1da271d811a811c240ad73d4bff7dc85f9abea9f0ef82d5368604b6240_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_92102f1da271d811a811c240ad73d4bff7dc85f9abea9f0ef82d5368604b6240->leave($__internal_92102f1da271d811a811c240ad73d4bff7dc85f9abea9f0ef82d5368604b6240_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_af5ca415c8297a9a4fd1cce419e40dd42662c607d65ff9de2d56d5ea19751193 = $this->env->getExtension("native_profiler");
        $__internal_af5ca415c8297a9a4fd1cce419e40dd42662c607d65ff9de2d56d5ea19751193->enter($__internal_af5ca415c8297a9a4fd1cce419e40dd42662c607d65ff9de2d56d5ea19751193_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_af5ca415c8297a9a4fd1cce419e40dd42662c607d65ff9de2d56d5ea19751193->leave($__internal_af5ca415c8297a9a4fd1cce419e40dd42662c607d65ff9de2d56d5ea19751193_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_705603a2be30fad84d2daeedfa902e8410a72f00e63398fbb01de7712d55e563 = $this->env->getExtension("native_profiler");
        $__internal_705603a2be30fad84d2daeedfa902e8410a72f00e63398fbb01de7712d55e563->enter($__internal_705603a2be30fad84d2daeedfa902e8410a72f00e63398fbb01de7712d55e563_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_705603a2be30fad84d2daeedfa902e8410a72f00e63398fbb01de7712d55e563->leave($__internal_705603a2be30fad84d2daeedfa902e8410a72f00e63398fbb01de7712d55e563_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_4af3eb4772d668f301cbec2bc1dc108e39e6957ea7a27eb13d128009efb7b0dd = $this->env->getExtension("native_profiler");
        $__internal_4af3eb4772d668f301cbec2bc1dc108e39e6957ea7a27eb13d128009efb7b0dd->enter($__internal_4af3eb4772d668f301cbec2bc1dc108e39e6957ea7a27eb13d128009efb7b0dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_4af3eb4772d668f301cbec2bc1dc108e39e6957ea7a27eb13d128009efb7b0dd->leave($__internal_4af3eb4772d668f301cbec2bc1dc108e39e6957ea7a27eb13d128009efb7b0dd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
