<?php

/* SistemaUserBundle::layout.html.twig */
class __TwigTemplate_f5cc26fec78b8dbe7379ca1b61ae1109dd1086b8e7d89c03bfa3e9881b0816f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'user_lateral' => array($this, 'block_user_lateral'),
            'menu' => array($this, 'block_menu'),
            'wrapper' => array($this, 'block_wrapper'),
            'messages_error' => array($this, 'block_messages_error'),
            'fos_user_panel' => array($this, 'block_fos_user_panel'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0dcb62716547a9c5984f66c34d9ab4bfb410d5b272f73cb5a2f9fb3dc5267958 = $this->env->getExtension("native_profiler");
        $__internal_0dcb62716547a9c5984f66c34d9ab4bfb410d5b272f73cb5a2f9fb3dc5267958->enter($__internal_0dcb62716547a9c5984f66c34d9ab4bfb410d5b272f73cb5a2f9fb3dc5267958_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaUserBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <!-- Le styles -->
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrap-checkbox.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"bootstrap-style\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/select2/select2.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrapValidator.min.css"), "html", null, true);
        echo "\">
        <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/css/bootstrap-datetimepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        
        <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/crud.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/css/crud.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
        <![endif]-->
        ";
        // line 28
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/js/bootstrap-datetimepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/js/locales/bootstrap-datetimepicker.es.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/select2/select2.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 31
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 32
        echo "        <!-- Le fav and touch icons -->
        <link rel=\"shortcut icon\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">

        <!-- Smartsupp Live Chat script -->
        <script type=\"text/javascript\">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '7d2ca3db7d98ba7a900e432e90bea4e57af41001';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
        </script>
    </head>

    <body class=\"focusedform\">
        ";
        // line 49
        $this->displayBlock('body', $context, $blocks);
        // line 111
        echo "        ";
        $this->displayBlock('footer', $context, $blocks);
        // line 122
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-checkbox.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/validator/language/es_ES.js"), "html", null, true);
        echo "\"></script>
       ";
        // line 126
        $this->displayBlock('javascript', $context, $blocks);
        // line 127
        echo "    </body>
</html>";
        
        $__internal_0dcb62716547a9c5984f66c34d9ab4bfb410d5b272f73cb5a2f9fb3dc5267958->leave($__internal_0dcb62716547a9c5984f66c34d9ab4bfb410d5b272f73cb5a2f9fb3dc5267958_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_612d799eb7ed57169f63ceec80fad15f466ff9af1b877dc8ed2fe0f6e6c0233a = $this->env->getExtension("native_profiler");
        $__internal_612d799eb7ed57169f63ceec80fad15f466ff9af1b877dc8ed2fe0f6e6c0233a->enter($__internal_612d799eb7ed57169f63ceec80fad15f466ff9af1b877dc8ed2fe0f6e6c0233a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.layout.bannersadmin", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        
        $__internal_612d799eb7ed57169f63ceec80fad15f466ff9af1b877dc8ed2fe0f6e6c0233a->leave($__internal_612d799eb7ed57169f63ceec80fad15f466ff9af1b877dc8ed2fe0f6e6c0233a_prof);

    }

    // line 31
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fd29eb3609cb7027b0d699cb23945cd46129e9ab8c1f63e4606133b378128c24 = $this->env->getExtension("native_profiler");
        $__internal_fd29eb3609cb7027b0d699cb23945cd46129e9ab8c1f63e4606133b378128c24->enter($__internal_fd29eb3609cb7027b0d699cb23945cd46129e9ab8c1f63e4606133b378128c24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_fd29eb3609cb7027b0d699cb23945cd46129e9ab8c1f63e4606133b378128c24->leave($__internal_fd29eb3609cb7027b0d699cb23945cd46129e9ab8c1f63e4606133b378128c24_prof);

    }

    // line 49
    public function block_body($context, array $blocks = array())
    {
        $__internal_005ebe3dfc5ea9b10a9088a61950881666f413c9100a84f641b7a2e7dc9e31c1 = $this->env->getExtension("native_profiler");
        $__internal_005ebe3dfc5ea9b10a9088a61950881666f413c9100a84f641b7a2e7dc9e31c1->enter($__internal_005ebe3dfc5ea9b10a9088a61950881666f413c9100a84f641b7a2e7dc9e31c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 50
        echo "            ";
        $this->displayBlock('user_lateral', $context, $blocks);
        // line 81
        echo "            ";
        $this->displayBlock('wrapper', $context, $blocks);
        // line 110
        echo "        ";
        
        $__internal_005ebe3dfc5ea9b10a9088a61950881666f413c9100a84f641b7a2e7dc9e31c1->leave($__internal_005ebe3dfc5ea9b10a9088a61950881666f413c9100a84f641b7a2e7dc9e31c1_prof);

    }

    // line 50
    public function block_user_lateral($context, array $blocks = array())
    {
        $__internal_909aed0d315104474d29223176621f9f958e8fb2d677729edf400277a69dc31d = $this->env->getExtension("native_profiler");
        $__internal_909aed0d315104474d29223176621f9f958e8fb2d677729edf400277a69dc31d->enter($__internal_909aed0d315104474d29223176621f9f958e8fb2d677729edf400277a69dc31d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_lateral"));

        // line 51
        echo "            <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\".navbar-collapse\" aria-expanded=\"false\" aria-controls=\"navbar\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"#\"><img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/images/header_logo.png"), "html", null, true);
        echo "\" alt=\"\" /></a>
                    </div>
                    ";
        // line 62
        $this->displayBlock('menu', $context, $blocks);
        // line 78
        echo "                </div>
            </nav>
            ";
        
        $__internal_909aed0d315104474d29223176621f9f958e8fb2d677729edf400277a69dc31d->leave($__internal_909aed0d315104474d29223176621f9f958e8fb2d677729edf400277a69dc31d_prof);

    }

    // line 62
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d205d809592b172fbe9197a1425c8fd03aea3f5a1025a1e08969d4fa9d90c778 = $this->env->getExtension("native_profiler");
        $__internal_d205d809592b172fbe9197a1425c8fd03aea3f5a1025a1e08969d4fa9d90c778->enter($__internal_d205d809592b172fbe9197a1425c8fd03aea3f5a1025a1e08969d4fa9d90c778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 63
        echo "                        ";
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 64
            echo "                            <div class=\"sidebar\" role=\"navigation\">
                                <div class=\"sidebar-nav navbar-collapse collapse\">
                                    <ul class=\"nav\">
                                        <li><a class=\"bg-info\">";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "</a></li>
                                    </ul>
                                    ";
            // line 69
            echo $this->env->getExtension('knp_menu')->render("SistemaCPCEBundle:Builder:AfiliadoMenu");
            echo "
                                </div>
                            </div>
                            <script>
                                smartsupp('email', '";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "email", array()), "html", null, true);
            echo "');
                                smartsupp('name', '";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
            echo "');
                            </script>
                        ";
        }
        // line 77
        echo "                    ";
        
        $__internal_d205d809592b172fbe9197a1425c8fd03aea3f5a1025a1e08969d4fa9d90c778->leave($__internal_d205d809592b172fbe9197a1425c8fd03aea3f5a1025a1e08969d4fa9d90c778_prof);

    }

    // line 81
    public function block_wrapper($context, array $blocks = array())
    {
        $__internal_ec9628ba149cba940c75e0f9320d73a0dd84ba003b02318c5a9e2a72c4b05dce = $this->env->getExtension("native_profiler");
        $__internal_ec9628ba149cba940c75e0f9320d73a0dd84ba003b02318c5a9e2a72c4b05dce->enter($__internal_ec9628ba149cba940c75e0f9320d73a0dd84ba003b02318c5a9e2a72c4b05dce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        // line 82
        echo "            <div id=\"page-wrapper\">
                <div class=\"content\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12\">
                            ";
        // line 86
        $this->displayBlock('messages_error', $context, $blocks);
        // line 97
        echo "
                            <div id=\"loginbox\" class=\"mainbox\">
                                ";
        // line 99
        $this->displayBlock('fos_user_panel', $context, $blocks);
        // line 104
        echo "                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ";
        
        $__internal_ec9628ba149cba940c75e0f9320d73a0dd84ba003b02318c5a9e2a72c4b05dce->leave($__internal_ec9628ba149cba940c75e0f9320d73a0dd84ba003b02318c5a9e2a72c4b05dce_prof);

    }

    // line 86
    public function block_messages_error($context, array $blocks = array())
    {
        $__internal_64b2f14e5584f7e35f06c6be81b20f41365c42036649ab5e2e43e91a15cc293e = $this->env->getExtension("native_profiler");
        $__internal_64b2f14e5584f7e35f06c6be81b20f41365c42036649ab5e2e43e91a15cc293e->enter($__internal_64b2f14e5584f7e35f06c6be81b20f41365c42036649ab5e2e43e91a15cc293e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "messages_error"));

        // line 87
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 88
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 89
                echo "                                        <div class=\"alert
                                        ";
                // line 90
                if (($context["type"] == "fos_user_success")) {
                    echo " alert-success ";
                } else {
                    echo " alert-danger ";
                }
                // line 91
                echo "                                        \">
                                            ";
                // line 92
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 95
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "                            ";
        
        $__internal_64b2f14e5584f7e35f06c6be81b20f41365c42036649ab5e2e43e91a15cc293e->leave($__internal_64b2f14e5584f7e35f06c6be81b20f41365c42036649ab5e2e43e91a15cc293e_prof);

    }

    // line 99
    public function block_fos_user_panel($context, array $blocks = array())
    {
        $__internal_86d24a0ecbdf981c71ca4935130438c335bb5d65a53a66fea9324ea072dcb3b4 = $this->env->getExtension("native_profiler");
        $__internal_86d24a0ecbdf981c71ca4935130438c335bb5d65a53a66fea9324ea072dcb3b4->enter($__internal_86d24a0ecbdf981c71ca4935130438c335bb5d65a53a66fea9324ea072dcb3b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_panel"));

        // line 100
        echo "                                    <div class=\"panel panel-default\">
                                        ";
        // line 101
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 102
        echo "                                    </div>
                                ";
        
        $__internal_86d24a0ecbdf981c71ca4935130438c335bb5d65a53a66fea9324ea072dcb3b4->leave($__internal_86d24a0ecbdf981c71ca4935130438c335bb5d65a53a66fea9324ea072dcb3b4_prof);

    }

    // line 101
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_13f6b7856c28795b5189924481f9712a4c82ac84e70ef937a0dc918b32be4978 = $this->env->getExtension("native_profiler");
        $__internal_13f6b7856c28795b5189924481f9712a4c82ac84e70ef937a0dc918b32be4978->enter($__internal_13f6b7856c28795b5189924481f9712a4c82ac84e70ef937a0dc918b32be4978_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_13f6b7856c28795b5189924481f9712a4c82ac84e70ef937a0dc918b32be4978->leave($__internal_13f6b7856c28795b5189924481f9712a4c82ac84e70ef937a0dc918b32be4978_prof);

    }

    // line 111
    public function block_footer($context, array $blocks = array())
    {
        $__internal_4e6fa3ddf14a04d226fba1f364f1f937dbec545fd8deb98199755f290d90b199 = $this->env->getExtension("native_profiler");
        $__internal_4e6fa3ddf14a04d226fba1f364f1f937dbec545fd8deb98199755f290d90b199->enter($__internal_4e6fa3ddf14a04d226fba1f364f1f937dbec545fd8deb98199755f290d90b199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 112
        echo "            <footer>
            <div id=\"page-wrapper\">
                <div class=\"content\" style=\"background-color: transparent;\">
                    <div class=\"footer\">
                        <p style=\"float: right;\">Desarrollo <a href=\"mailto:sistemas@cpcechaco.org.ar\">Gonzalo Alonso</a> &copy; Sistemas CPCE</p>
                    </div>
                </div>
            </div>
            </footer>
        ";
        
        $__internal_4e6fa3ddf14a04d226fba1f364f1f937dbec545fd8deb98199755f290d90b199->leave($__internal_4e6fa3ddf14a04d226fba1f364f1f937dbec545fd8deb98199755f290d90b199_prof);

    }

    // line 126
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_ceab35e7336bbdf346601e7241c221b31463341161920047251b6826d27c26bc = $this->env->getExtension("native_profiler");
        $__internal_ceab35e7336bbdf346601e7241c221b31463341161920047251b6826d27c26bc->enter($__internal_ceab35e7336bbdf346601e7241c221b31463341161920047251b6826d27c26bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        
        $__internal_ceab35e7336bbdf346601e7241c221b31463341161920047251b6826d27c26bc->leave($__internal_ceab35e7336bbdf346601e7241c221b31463341161920047251b6826d27c26bc_prof);

    }

    public function getTemplateName()
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  415 => 126,  399 => 112,  393 => 111,  382 => 101,  374 => 102,  372 => 101,  369 => 100,  363 => 99,  356 => 96,  350 => 95,  341 => 92,  338 => 91,  332 => 90,  329 => 89,  324 => 88,  319 => 87,  313 => 86,  301 => 104,  299 => 99,  295 => 97,  293 => 86,  287 => 82,  281 => 81,  274 => 77,  268 => 74,  264 => 73,  257 => 69,  252 => 67,  247 => 64,  244 => 63,  238 => 62,  229 => 78,  227 => 62,  222 => 60,  211 => 51,  205 => 50,  198 => 110,  195 => 81,  192 => 50,  186 => 49,  175 => 31,  163 => 5,  155 => 127,  153 => 126,  149 => 125,  145 => 124,  141 => 123,  136 => 122,  133 => 111,  131 => 49,  112 => 33,  109 => 32,  107 => 31,  103 => 30,  99 => 29,  94 => 28,  86 => 22,  82 => 21,  77 => 19,  73 => 18,  68 => 16,  64 => 15,  60 => 14,  56 => 13,  52 => 12,  48 => 11,  39 => 5,  33 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <title>{% block title %}{{ 'views.layout.bannersadmin'|trans({}, 'MWSimpleAdminCrudBundle') }}{% endblock %}</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <meta name="description" content="">*/
/*         <meta name="author" content="">*/
/* */
/*         <!-- Le styles -->*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrap.min.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrap-checkbox.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" rel="stylesheet" id="bootstrap-style">*/
/*         <link rel="stylesheet" href="{{ asset('bundles/mwsimpleadmincrud/select2/select2.css') }}">*/
/*         <link rel="stylesheet" href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrapValidator.min.css') }}">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/datetime/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">*/
/*         */
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/crud.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/sistemacpce/css/crud.css') }}" rel="stylesheet">*/
/* */
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/jquery.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap.min.js') }}"></script>*/
/*         <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->*/
/*         <!--[if lt IE 9]>*/
/*         <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/*         <![endif]-->*/
/*         {# JS #}*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/datetime/js/bootstrap-datetimepicker.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/datetime/js/locales/bootstrap-datetimepicker.es.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/select2/select2.min.js') }}"></script>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <!-- Le fav and touch icons -->*/
/*         <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">*/
/* */
/*         <!-- Smartsupp Live Chat script -->*/
/*         <script type="text/javascript">*/
/*         var _smartsupp = _smartsupp || {};*/
/*         _smartsupp.key = '7d2ca3db7d98ba7a900e432e90bea4e57af41001';*/
/*         window.smartsupp||(function(d) {*/
/*             var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];*/
/*             s=d.getElementsByTagName('script')[0];c=d.createElement('script');*/
/*             c.type='text/javascript';c.charset='utf-8';c.async=true;*/
/*             c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);*/
/*         })(document);*/
/*         </script>*/
/*     </head>*/
/* */
/*     <body class="focusedform">*/
/*         {% block body %}*/
/*             {% block user_lateral %}*/
/*             <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*                 <div class="container-fluid">*/
/*                     <div class="navbar-header">*/
/*                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">*/
/*                             <span class="sr-only">Toggle navigation</span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                         </button>*/
/*                         <a class="navbar-brand" href="#"><img src="{{ asset('bundles/sistemacpce/images/header_logo.png') }}" alt="" /></a>*/
/*                     </div>*/
/*                     {% block menu %}*/
/*                         {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                             <div class="sidebar" role="navigation">*/
/*                                 <div class="sidebar-nav navbar-collapse collapse">*/
/*                                     <ul class="nav">*/
/*                                         <li><a class="bg-info">{{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}</a></li>*/
/*                                     </ul>*/
/*                                     {{ knp_menu_render('SistemaCPCEBundle:Builder:AfiliadoMenu') }}*/
/*                                 </div>*/
/*                             </div>*/
/*                             <script>*/
/*                                 smartsupp('email', '{{ app.user.email }}');*/
/*                                 smartsupp('name', '{{ app.user.username }}');*/
/*                             </script>*/
/*                         {% endif %}*/
/*                     {% endblock %}*/
/*                 </div>*/
/*             </nav>*/
/*             {% endblock user_lateral %}*/
/*             {% block wrapper %}*/
/*             <div id="page-wrapper">*/
/*                 <div class="content">*/
/*                     <div class="row">*/
/*                         <div class="col-md-12 col-sm-12">*/
/*                             {% block messages_error %}*/
/*                                 {% for type, messages in app.session.flashbag.all() %}*/
/*                                     {% for message in messages %}*/
/*                                         <div class="alert*/
/*                                         {% if type == "fos_user_success" %} alert-success {% else %} alert-danger {% endif %}*/
/*                                         ">*/
/*                                             {{ message|trans({}, 'FOSUserBundle') }}*/
/*                                         </div>*/
/*                                     {% endfor %}*/
/*                                 {% endfor %}*/
/*                             {% endblock messages_error %}*/
/* */
/*                             <div id="loginbox" class="mainbox">*/
/*                                 {% block fos_user_panel %}*/
/*                                     <div class="panel panel-default">*/
/*                                         {% block fos_user_content %}{% endblock fos_user_content %}*/
/*                                     </div>*/
/*                                 {% endblock fos_user_panel %}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             {% endblock wrapper %}*/
/*         {% endblock body %}*/
/*         {% block footer %}*/
/*             <footer>*/
/*             <div id="page-wrapper">*/
/*                 <div class="content" style="background-color: transparent;">*/
/*                     <div class="footer">*/
/*                         <p style="float: right;">Desarrollo <a href="mailto:sistemas@cpcechaco.org.ar">Gonzalo Alonso</a> &copy; Sistemas CPCE</p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             </footer>*/
/*         {% endblock footer %}*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-checkbox.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/validator/language/es_ES.js') }}"></script>*/
/*        {% block javascript %}{% endblock %}*/
/*     </body>*/
/* </html>*/
