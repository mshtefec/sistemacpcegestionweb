<?php

/* SistemaCPCEBundle::Longlayout.html.twig */
class __TwigTemplate_c2c6d7ae029876d07920c296a0ec9948c16c22151abbb4323df9d5aca84fec50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaCPCEBundle::layout.html.twig", "SistemaCPCEBundle::Longlayout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaCPCEBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c0be3cc790ece84cb9bd3b078f70095c0d45b16e9159785700a3ff97c52cf67 = $this->env->getExtension("native_profiler");
        $__internal_2c0be3cc790ece84cb9bd3b078f70095c0d45b16e9159785700a3ff97c52cf67->enter($__internal_2c0be3cc790ece84cb9bd3b078f70095c0d45b16e9159785700a3ff97c52cf67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle::Longlayout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2c0be3cc790ece84cb9bd3b078f70095c0d45b16e9159785700a3ff97c52cf67->leave($__internal_2c0be3cc790ece84cb9bd3b078f70095c0d45b16e9159785700a3ff97c52cf67_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8e959afe363db370b98ed652aed8f7845a6b6df8f667b5cf57eb26ac2f780e96 = $this->env->getExtension("native_profiler");
        $__internal_8e959afe363db370b98ed652aed8f7845a6b6df8f667b5cf57eb26ac2f780e96->enter($__internal_8e959afe363db370b98ed652aed8f7845a6b6df8f667b5cf57eb26ac2f780e96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style type=\"text/css\">
\t\t@media (min-width: 769px) {
\t\t    .content {
\t\t        margin: 0 3em;
\t\t    }
\t\t}
    </style>
";
        
        $__internal_8e959afe363db370b98ed652aed8f7845a6b6df8f667b5cf57eb26ac2f780e96->leave($__internal_8e959afe363db370b98ed652aed8f7845a6b6df8f667b5cf57eb26ac2f780e96_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle::Longlayout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'SistemaCPCEBundle::layout.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <style type="text/css">*/
/* 		@media (min-width: 769px) {*/
/* 		    .content {*/
/* 		        margin: 0 3em;*/
/* 		    }*/
/* 		}*/
/*     </style>*/
/* {% endblock %}*/
