<?php

/* SistemaCPCEBundle:Dato:datos.html.twig */
class __TwigTemplate_df0b065778181896cd4f5a76ce354ff8f94b95652b787ab66072eaf18c73e3eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaCPCEBundle::longLayout.html.twig", "SistemaCPCEBundle:Dato:datos.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaCPCEBundle::longLayout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_876947f96c1fab93191deed3c69a70016b6bf757ac960a3eefd6edbb71a34ef8 = $this->env->getExtension("native_profiler");
        $__internal_876947f96c1fab93191deed3c69a70016b6bf757ac960a3eefd6edbb71a34ef8->enter($__internal_876947f96c1fab93191deed3c69a70016b6bf757ac960a3eefd6edbb71a34ef8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Dato:datos.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_876947f96c1fab93191deed3c69a70016b6bf757ac960a3eefd6edbb71a34ef8->leave($__internal_876947f96c1fab93191deed3c69a70016b6bf757ac960a3eefd6edbb71a34ef8_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_f438cafa1cf66c63b27d5c63f541dd46415d662a821b9eb9b622f172828df248 = $this->env->getExtension("native_profiler");
        $__internal_f438cafa1cf66c63b27d5c63f541dd46415d662a821b9eb9b622f172828df248->enter($__internal_f438cafa1cf66c63b27d5c63f541dd46415d662a821b9eb9b622f172828df248_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Datos
";
        
        $__internal_f438cafa1cf66c63b27d5c63f541dd46415d662a821b9eb9b622f172828df248->leave($__internal_f438cafa1cf66c63b27d5c63f541dd46415d662a821b9eb9b622f172828df248_prof);

    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7496f745e40de6e1fa4503361f8ed51a41344193ec13ea1388878c4a4b6a28b4 = $this->env->getExtension("native_profiler");
        $__internal_7496f745e40de6e1fa4503361f8ed51a41344193ec13ea1388878c4a4b6a28b4->enter($__internal_7496f745e40de6e1fa4503361f8ed51a41344193ec13ea1388878c4a4b6a28b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        echo "    <div class=\"panel panel-primary center-block\">
        <div class=\"panel-heading\">Matriculado</div>
        <div class=\"panel-body\">
            <form class=\"form-horizontal\">
                <fieldset>
                    <div class=\"form-group\">
                        <div class=\"col-lg-8\">
                            <label class=\"control-label\">Apellido y Nombres</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afinombre", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-2\">
                            <label class=\"control-label\">Tipo</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afitipdoc", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-2\">
                            <label class=\"control-label\">Documento</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afinrodoc", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-lg-8\">
                            <label class=\"control-label\">Categoría</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "catDescripcion", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-2\">
                            <label class=\"control-label\">Título</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afititulo", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-2\">
                            <label class=\"control-label\">Matrícula</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afimatricula", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-lg-3\">
                            <label class=\"control-label\">Delegación</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "getAfiZonaDelegacion", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-9\">
                            <label class=\"control-label\">Domicilio</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 78
        echo twig_escape_filter($this->env, ((((((($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afidireccion", array()) . ", ") . $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afilocalidad", array())) . ", ") . $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afiprovincia", array())) . " (") . $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Aficodpos", array())) . ")"), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-lg-3\">
                            <label class=\"control-label\">Fecha de Nacimiento</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 88
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afifecnac", array()), "d/m/Y"), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-3\">
                            <label class=\"control-label\">Sexo</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "getAfiSexoString", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-3\">
                            <label class=\"control-label\">Estado Civil</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "getAfiCivilEstado", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-lg-3\">
                            <label class=\"control-label\">Teléfono</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afitelefono1", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-3\">
                            <label class=\"control-label\">Celular</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 122
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "AfiCelular", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-6\">
                            <label class=\"control-label\">Correo</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 130
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Afimail", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-lg-6\">
                            <label class=\"control-label\">Condición IVA</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "getAfiGananciasCondicionIva", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                        <div class=\"col-lg-3\">
                            <label class=\"control-label\">CUIL</label>
                            <div class=\"form-control\">
                                <span class=\"input-xlarge uneditable-input\">
                                    ";
        // line 148
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "Aficuit", array()), "html", null, true);
        echo "
                                </span>
                            </div>
                        </div>
                    </div>
                    ";
        // line 153
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "userCurriculum", array())) {
            // line 154
            echo "                        <div class=\"form-group\">
                            <div class=\"col-lg-6\">
                                <label class=\"control-label\">Curriculum Vitae</label>
                                <a class=\"btn btn-default btn-lg glyphicon glyphicon-save\" href=\"";
            // line 157
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("matriculado_getCurriculum", array("matriculadoId" => $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "userCurriculum", array()), "id", array()))), "html", null, true);
            echo "\" download></a>
                            </div>
                        </div>
                    ";
        } else {
            // line 161
            echo "                        <div class=\"form-group\">
                            <div class=\"col-lg-6\">
                                <label class=\"control-label\">Curriculum Vitae</label>
                                <div class=\"form-control\">
                                    <span class=\"glyphicon glyphicon-remove\">
                                        No tiene curriculum cargado
                                    </span>
                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 172
        echo "                </fieldset>
            </form>
        </div>
    </div>
    
    <div class=\"panel panel-primary\">
        <div class=\"panel-heading\">Datos Familiares</div>
        <div class=\"panel-body\">
            <table class=\"table table-striped\">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Dni</th>
                        <th>Nombre</th>
                        <th>Fecha de Nacimiento</th>
                        <th>Sexo</th>
                        <th>Fil</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope=\"row\">1</th>
                        <td>";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiDoc1", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 195
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiAut1", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 196
        echo $this->env->getExtension('twig.extension_custom')->afiNac(twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiNac1", array()), "d/m/Y"));
        echo "</td>
                        <td>";
        // line 197
        echo $this->env->getExtension('twig.extension_custom')->afiSex($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiSex1", array()));
        echo "</td>
                        <td>";
        // line 198
        echo $this->env->getExtension('twig.extension_custom')->afiFil($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiFil1", array()));
        echo "</td>
                    </tr>
                    <tr>
                        <th scope=\"row\">2</th>
                        <td>";
        // line 202
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiDoc2", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 203
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiAut2", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 204
        echo $this->env->getExtension('twig.extension_custom')->afiNac(twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiNac2", array()), "d/m/Y"));
        echo "</td>
                        <td>";
        // line 205
        echo $this->env->getExtension('twig.extension_custom')->afiSex($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiSex2", array()));
        echo "</td>
                        <td>";
        // line 206
        echo $this->env->getExtension('twig.extension_custom')->afiFil($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiFil2", array()));
        echo "</td>
                    </tr>
                    <tr>
                        <th scope=\"row\">3</th>
                        <td>";
        // line 210
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiDoc3", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 211
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiAut3", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 212
        echo $this->env->getExtension('twig.extension_custom')->afiNac(twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiNac3", array()), "d/m/Y"));
        echo "</td>
                        <td>";
        // line 213
        echo $this->env->getExtension('twig.extension_custom')->afiSex($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiSex3", array()));
        echo "</td>
                        <td>";
        // line 214
        echo $this->env->getExtension('twig.extension_custom')->afiFil($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiFil3", array()));
        echo "</td>
                    </tr>
                    <tr>
                        <th scope=\"row\">4</th>
                        <td>";
        // line 218
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiDoc4", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 219
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiAut4", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 220
        echo $this->env->getExtension('twig.extension_custom')->afiNac(twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiNac4", array()), "d/m/Y"));
        echo "</td>
                        <td>";
        // line 221
        echo $this->env->getExtension('twig.extension_custom')->afiSex($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiSex4", array()));
        echo "</td>
                        <td>";
        // line 222
        echo $this->env->getExtension('twig.extension_custom')->afiFil($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiFil4", array()));
        echo "</td>
                    </tr>
                    <tr>
                        <th scope=\"row\">5</th>
                        <td>";
        // line 226
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiDoc5", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 227
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiAut5", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 228
        echo $this->env->getExtension('twig.extension_custom')->afiNac(twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiNac5", array()), "d/m/Y"));
        echo "</td>
                        <td>";
        // line 229
        echo $this->env->getExtension('twig.extension_custom')->afiSex($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiSex5", array()));
        echo "</td>
                        <td>";
        // line 230
        echo $this->env->getExtension('twig.extension_custom')->afiFil($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiFil5", array()));
        echo "</td>
                    </tr>
                    <tr>
                        <th scope=\"row\">6</th>
                        <td>";
        // line 234
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiDoc6", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 235
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiAut6", array()), "html", null, true);
        echo "</td>
                        <td>";
        // line 236
        echo $this->env->getExtension('twig.extension_custom')->afiNac(twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiNac6", array()), "d/m/Y"));
        echo "</td>
                        <td>";
        // line 237
        echo $this->env->getExtension('twig.extension_custom')->afiSex($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiSex6", array()));
        echo "</td>
                        <td>";
        // line 238
        echo $this->env->getExtension('twig.extension_custom')->afiFil($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0, array(), "array"), "afiFil6", array()));
        echo "</td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    
    <div class=\"row\">
    ";
        // line 248
        if (twig_test_empty($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "userCurriculum", array()))) {
            // line 249
            echo "        <div class=\"col-lg-5 col-md-5 col-sm-5\">
            <a class=\"btn btn-primary col-lg-12\" href=\"";
            // line 250
            echo $this->env->getExtension('routing')->getPath("matriculado_curriculum_new");
            echo "\">
                Cargar Curriculum
            </a>
        </div>
    ";
        } else {
            // line 255
            echo "    ";
            // line 256
            echo "        <div class=\"col-lg-5 col-md-5 col-sm-5\">
            <a class=\"btn btn-primary col-lg-12\" href=\"";
            // line 257
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("matriculado_curriculum_edit", array("id" => $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "userCurriculum", array()), "id", array()))), "html", null, true);
            echo "\">
                Actualizar Curriculum
            </a>
        </div>
    ";
        }
        // line 262
        echo "        <div class=\"col-lg-5 col-md-5 col-sm-5\">
            <a class=\"btn btn-primary col-lg-12\" href=\"";
        // line 263
        echo $this->env->getExtension('routing')->getPath("front_dato_editar");
        echo "\">
                Editar Datos
            </a>
        </div>
        <div class=\"col-lg-2 col-md-2 col-sm-2\">
            <a class=\"btn btn-danger col-lg-12\" href=\"";
        // line 268
        echo $this->env->getExtension('routing')->getPath("front_dato_pdf");
        echo "\">
                Exportar PDF
            </a>
        </div>
    </div>
    <br>

";
        
        $__internal_7496f745e40de6e1fa4503361f8ed51a41344193ec13ea1388878c4a4b6a28b4->leave($__internal_7496f745e40de6e1fa4503361f8ed51a41344193ec13ea1388878c4a4b6a28b4_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Dato:datos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  484 => 268,  476 => 263,  473 => 262,  465 => 257,  462 => 256,  460 => 255,  452 => 250,  449 => 249,  447 => 248,  434 => 238,  430 => 237,  426 => 236,  422 => 235,  418 => 234,  411 => 230,  407 => 229,  403 => 228,  399 => 227,  395 => 226,  388 => 222,  384 => 221,  380 => 220,  376 => 219,  372 => 218,  365 => 214,  361 => 213,  357 => 212,  353 => 211,  349 => 210,  342 => 206,  338 => 205,  334 => 204,  330 => 203,  326 => 202,  319 => 198,  315 => 197,  311 => 196,  307 => 195,  303 => 194,  279 => 172,  266 => 161,  259 => 157,  254 => 154,  252 => 153,  244 => 148,  233 => 140,  220 => 130,  209 => 122,  198 => 114,  185 => 104,  174 => 96,  163 => 88,  150 => 78,  139 => 70,  126 => 60,  115 => 52,  104 => 44,  91 => 34,  80 => 26,  69 => 18,  57 => 8,  51 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'SistemaCPCEBundle::longLayout.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }} - Datos*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="panel panel-primary center-block">*/
/*         <div class="panel-heading">Matriculado</div>*/
/*         <div class="panel-body">*/
/*             <form class="form-horizontal">*/
/*                 <fieldset>*/
/*                     <div class="form-group">*/
/*                         <div class="col-lg-8">*/
/*                             <label class="control-label">Apellido y Nombres</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afinombre }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-2">*/
/*                             <label class="control-label">Tipo</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afitipdoc }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-2">*/
/*                             <label class="control-label">Documento</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afinrodoc }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <div class="col-lg-8">*/
/*                             <label class="control-label">Categoría</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity.catDescripcion }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-2">*/
/*                             <label class="control-label">Título</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afititulo }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-2">*/
/*                             <label class="control-label">Matrícula</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afimatricula }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <div class="col-lg-3">*/
/*                             <label class="control-label">Delegación</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].getAfiZonaDelegacion }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-9">*/
/*                             <label class="control-label">Domicilio</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afidireccion ~ ', ' ~ entity[0].Afilocalidad ~ ', ' ~ entity[0].Afiprovincia ~ ' (' ~ entity[0].Aficodpos ~ ')' }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <div class="col-lg-3">*/
/*                             <label class="control-label">Fecha de Nacimiento</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afifecnac|date('d/m/Y') }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-3">*/
/*                             <label class="control-label">Sexo</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].getAfiSexoString }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-3">*/
/*                             <label class="control-label">Estado Civil</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].getAfiCivilEstado }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <div class="col-lg-3">*/
/*                             <label class="control-label">Teléfono</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afitelefono1 }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-3">*/
/*                             <label class="control-label">Celular</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].AfiCelular }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-6">*/
/*                             <label class="control-label">Correo</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Afimail }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <div class="col-lg-6">*/
/*                             <label class="control-label">Condición IVA</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].getAfiGananciasCondicionIva }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-lg-3">*/
/*                             <label class="control-label">CUIL</label>*/
/*                             <div class="form-control">*/
/*                                 <span class="input-xlarge uneditable-input">*/
/*                                     {{ entity[0].Aficuit }}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     {% if user.userCurriculum %}*/
/*                         <div class="form-group">*/
/*                             <div class="col-lg-6">*/
/*                                 <label class="control-label">Curriculum Vitae</label>*/
/*                                 <a class="btn btn-default btn-lg glyphicon glyphicon-save" href="{{ path('matriculado_getCurriculum', { 'matriculadoId': user.userCurriculum.id }) }}" download></a>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% else %}*/
/*                         <div class="form-group">*/
/*                             <div class="col-lg-6">*/
/*                                 <label class="control-label">Curriculum Vitae</label>*/
/*                                 <div class="form-control">*/
/*                                     <span class="glyphicon glyphicon-remove">*/
/*                                         No tiene curriculum cargado*/
/*                                     </span>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/*                 </fieldset>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/*     */
/*     <div class="panel panel-primary">*/
/*         <div class="panel-heading">Datos Familiares</div>*/
/*         <div class="panel-body">*/
/*             <table class="table table-striped">*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <th>#</th>*/
/*                         <th>Dni</th>*/
/*                         <th>Nombre</th>*/
/*                         <th>Fecha de Nacimiento</th>*/
/*                         <th>Sexo</th>*/
/*                         <th>Fil</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                     <tr>*/
/*                         <th scope="row">1</th>*/
/*                         <td>{{ entity[0].afiDoc1 }}</td>*/
/*                         <td>{{ entity[0].afiAut1 }}</td>*/
/*                         <td>{{ afiNac(entity[0].afiNac1|date("d/m/Y")) }}</td>*/
/*                         <td>{{ afiSex(entity[0].afiSex1) }}</td>*/
/*                         <td>{{ afiFil(entity[0].afiFil1) }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th scope="row">2</th>*/
/*                         <td>{{ entity[0].afiDoc2 }}</td>*/
/*                         <td>{{ entity[0].afiAut2 }}</td>*/
/*                         <td>{{ afiNac(entity[0].afiNac2|date("d/m/Y")) }}</td>*/
/*                         <td>{{ afiSex(entity[0].afiSex2) }}</td>*/
/*                         <td>{{ afiFil(entity[0].afiFil2) }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th scope="row">3</th>*/
/*                         <td>{{ entity[0].afiDoc3 }}</td>*/
/*                         <td>{{ entity[0].afiAut3 }}</td>*/
/*                         <td>{{ afiNac(entity[0].afiNac3|date("d/m/Y")) }}</td>*/
/*                         <td>{{ afiSex(entity[0].afiSex3) }}</td>*/
/*                         <td>{{ afiFil(entity[0].afiFil3) }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th scope="row">4</th>*/
/*                         <td>{{ entity[0].afiDoc4 }}</td>*/
/*                         <td>{{ entity[0].afiAut4 }}</td>*/
/*                         <td>{{ afiNac(entity[0].afiNac4|date("d/m/Y")) }}</td>*/
/*                         <td>{{ afiSex(entity[0].afiSex4) }}</td>*/
/*                         <td>{{ afiFil(entity[0].afiFil4) }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th scope="row">5</th>*/
/*                         <td>{{ entity[0].afiDoc5 }}</td>*/
/*                         <td>{{ entity[0].afiAut5 }}</td>*/
/*                         <td>{{ afiNac(entity[0].afiNac5|date("d/m/Y")) }}</td>*/
/*                         <td>{{ afiSex(entity[0].afiSex5) }}</td>*/
/*                         <td>{{ afiFil(entity[0].afiFil5) }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th scope="row">6</th>*/
/*                         <td>{{ entity[0].afiDoc6 }}</td>*/
/*                         <td>{{ entity[0].afiAut6 }}</td>*/
/*                         <td>{{ afiNac(entity[0].afiNac6|date("d/m/Y")) }}</td>*/
/*                         <td>{{ afiSex(entity[0].afiSex6) }}</td>*/
/*                         <td>{{ afiFil(entity[0].afiFil6) }}</td>*/
/*                     </tr>*/
/*                     <tr></tr>*/
/*                     <tr></tr>*/
/*                 </tbody>*/
/*             </table>*/
/*         </div>*/
/*     </div>*/
/*     */
/*     <div class="row">*/
/*     {% if user.userCurriculum is empty %}*/
/*         <div class="col-lg-5 col-md-5 col-sm-5">*/
/*             <a class="btn btn-primary col-lg-12" href="{{ path('matriculado_curriculum_new') }}">*/
/*                 Cargar Curriculum*/
/*             </a>*/
/*         </div>*/
/*     {% else %}*/
/*     {# ACTUALIZAR CV #}*/
/*         <div class="col-lg-5 col-md-5 col-sm-5">*/
/*             <a class="btn btn-primary col-lg-12" href="{{ path('matriculado_curriculum_edit', {'id': user.userCurriculum.id}) }}">*/
/*                 Actualizar Curriculum*/
/*             </a>*/
/*         </div>*/
/*     {% endif %}*/
/*         <div class="col-lg-5 col-md-5 col-sm-5">*/
/*             <a class="btn btn-primary col-lg-12" href="{{ path('front_dato_editar') }}">*/
/*                 Editar Datos*/
/*             </a>*/
/*         </div>*/
/*         <div class="col-lg-2 col-md-2 col-sm-2">*/
/*             <a class="btn btn-danger col-lg-12" href="{{ path('front_dato_pdf') }}">*/
/*                 Exportar PDF*/
/*             </a>*/
/*         </div>*/
/*     </div>*/
/*     <br>*/
/* */
/* {% endblock %}*/
/* */
