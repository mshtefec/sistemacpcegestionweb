<?php

/* SistemaFrontBundle:Default:index.html.twig */
class __TwigTemplate_2add6cf139a461bb8ead0fd8fb75df6192daabd983df7efef356edb920234675 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaUserBundle::layout.html.twig", "SistemaFrontBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1b05142af90d416fc771250bee24385f5193282225767aa6ab022e551e45ad6 = $this->env->getExtension("native_profiler");
        $__internal_b1b05142af90d416fc771250bee24385f5193282225767aa6ab022e551e45ad6->enter($__internal_b1b05142af90d416fc771250bee24385f5193282225767aa6ab022e551e45ad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaFrontBundle:Default:index.html.twig"));

        // line 3
        if (($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED") && $this->env->getExtension('security')->isGranted("ROLE_MATRICULADO"))) {
            // line 4
            $context["usuarioEstado"] = "matriculado";
        } else {
            // line 6
            if (($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED") &&  !$this->env->getExtension('security')->isGranted("ROLE_MATRICULADO"))) {
                // line 7
                $context["usuarioEstado"] = "registrado";
            } else {
                // line 9
                $context["usuarioEstado"] = "visitante";
            }
        }
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b1b05142af90d416fc771250bee24385f5193282225767aa6ab022e551e45ad6->leave($__internal_b1b05142af90d416fc771250bee24385f5193282225767aa6ab022e551e45ad6_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6dc308fb7d7bf8f513bd3cb7cd5b6b92fdb638efcd6584bf354256d2568d1946 = $this->env->getExtension("native_profiler");
        $__internal_6dc308fb7d7bf8f513bd3cb7cd5b6b92fdb638efcd6584bf354256d2568d1946->enter($__internal_6dc308fb7d7bf8f513bd3cb7cd5b6b92fdb638efcd6584bf354256d2568d1946_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .panel-alto {
            min-height: 120px;
        }
    </style>
";
        
        $__internal_6dc308fb7d7bf8f513bd3cb7cd5b6b92fdb638efcd6584bf354256d2568d1946->leave($__internal_6dc308fb7d7bf8f513bd3cb7cd5b6b92fdb638efcd6584bf354256d2568d1946_prof);

    }

    // line 22
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_17132a4e9d80963619018d160a69760931f1537016d3eb858f4fc9be32084795 = $this->env->getExtension("native_profiler");
        $__internal_17132a4e9d80963619018d160a69760931f1537016d3eb858f4fc9be32084795->enter($__internal_17132a4e9d80963619018d160a69760931f1537016d3eb858f4fc9be32084795_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 23
        echo "    ";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "MWSimpleAdminCrudBundle:widget:fields.html.twig"));
        // line 24
        echo "    <div class=\"panel-heading\">
        <div class=\"panel-title\"><h2>Sistema Web de Gesti&oacute;n para Matriculados.</h2></div>
    </div>
    <div style=\"padding-top:10px\" class=\"panel-body\">
        <div class=\"row\">
";
        // line 33
        echo "            <div class=\"col-lg-6 col-md-6 col-sm-12\">
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Servicios actuales</div>
                    <div class=\"panel-body panel-alto\">
                        <div class=\"list-group\">
                            ";
        // line 41
        echo "                            <a href=\"";
        echo $this->env->getExtension('routing')->getPath("front_dato");
        echo "\" class=\"list-group-item\">Datos.</a>
                            <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("front_cuenta");
        echo "\" class=\"list-group-item\">Consulta estado de cuenta.</a>
                            <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("front_trabajo_new");
        echo "\" class=\"list-group-item\">Confecci&oacute;n de trabajos.</a>
                        </div>
                    </div>
                    <div class=\"panel-footer\">
                        ";
        // line 47
        if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "visitante")) {
            // line 48
            echo "                            <a href=\"";
            echo $this->env->getExtension('routing')->getPath("afiliado");
            echo "\" class=\"btn btn-primary btn-lg\"><span class=\"glyphicon glyphicon-log-in\"></span> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                        ";
        }
        // line 50
        echo "                    </div>
                </div>
            </div>
            <div class=\"col-lg-6 col-md-6 col-sm-12\">
                <div class=\"panel panel-danger\">
                    <div class=\"panel-heading\">
                        ";
        // line 56
        if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) != "matriculado")) {
            // line 57
            echo "                            ";
            if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "registrado")) {
                // line 58
                echo "                                *Recuerde habilitar su usuario, una vez registrado puede hacerlo de las siguientes maneras:
                            ";
            } else {
                // line 60
                echo "                                Registro (por &uacute;nica vez)
                            ";
            }
            // line 62
            echo "                        ";
        }
        // line 63
        echo "                    </div>
                    <div class=\"panel-body panel-alto\">
                        ";
        // line 65
        if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "matriculado")) {
            // line 66
            echo "                            <p class=\"text-success\">Se encuentra registrado.</p>
                        ";
        } else {
            // line 68
            echo "                            ";
            if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "registrado")) {
                // line 69
                echo "                            ";
            } else {
                // line 70
                echo "                                <p class=\"text-danger\">*Recuerde habilitar su usuario, una vez registrado puede hacerlo de las siguientes maneras:</p>
                            ";
            }
            // line 72
            echo "
                            <div class=\"list-group\">
                                ";
            // line 74
            if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "registrado")) {
                // line 75
                echo "                                    <a href=\"";
                echo $this->env->getExtension('routing')->getPath("formulario_activacion");
                echo "\" class=\"list-group-item\">
                                        <strong>Click para descargar formulario,</strong><br>
                                        lo completa y presenta en su delegación
                                    </a>
                                ";
            } else {
                // line 80
                echo "                                    <a class=\"list-group-item\">
                                        <strong>Descargando el formulario,</strong><br>
                                        lo completa y presenta en su delegación
                                    </a>
                                ";
            }
            // line 85
            echo "
                                <p></p>

                                ";
            // line 88
            if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "registrado")) {
                // line 89
                echo "                                    <!-- Button trigger modal -->
                                    <button type=\"button\" class=\"list-group-item list-group-item-success\" data-toggle=\"modal\" data-target=\"#myModal\">
                                        <strong>Click para habilitar en línea,</strong><br>
                                        ingresando su matrícula
                                    </button>
                                    <!-- Modal -->
                                    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
                                      <div class=\"modal-dialog\" role=\"document\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Cerrar\"><span aria-hidden=\"true\">&times;</span></button>
                                                <h4 class=\"modal-title\" id=\"myModalLabel\">Complete los datos</h4>
                                            </div>
                                            ";
                // line 102
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
                echo "
                                                <div class=\"modal-body\">
                                                    ";
                // line 104
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
                echo "
                                                    ";
                // line 105
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titulo", array()), 'row');
                echo "
                                                    ";
                // line 106
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "matricula", array()), 'row');
                echo "
                                                </div>
                                                <div class=\"modal-footer\">
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>
                                                    ";
                // line 110
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "save", array()), 'row');
                echo "
                                                </div>
                                            ";
                // line 112
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
                echo "
                                        </div>
                                      </div>
                                    </div>
                                ";
            } else {
                // line 117
                echo "                                    <a class=\"list-group-item list-group-item-success\">
                                        <strong>Habilitando en línea,</strong><br>
                                        ingresando su matrícula
                                    </a>
                                ";
            }
            // line 122
            echo "                            </div>
                        ";
        }
        // line 124
        echo "                    </div>
                    <div class=\"panel-footer\">
                        ";
        // line 126
        if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "visitante")) {
            // line 127
            echo "                            <a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
            echo "\" class=\"btn btn-danger btn-lg\"><span class=\"glyphicon glyphicon-user\"></span> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.register", array(), "FOSUserBundle"), "html", null, true);
            echo "me</a>
                        ";
        }
        // line 129
        echo "                    </div>
                </div>
            </div>
            ";
        // line 132
        if (((isset($context["usuarioEstado"]) ? $context["usuarioEstado"] : $this->getContext($context, "usuarioEstado")) == "visitante")) {
            // line 133
            echo "                ";
            $this->loadTemplate("SistemaFrontBundle:Default:instructivo.html.twig", "SistemaFrontBundle:Default:index.html.twig", 133)->display(array_merge($context, array("descargar" => true)));
            // line 134
            echo "            ";
        }
        // line 135
        echo "        </div>
    </div>
";
        
        $__internal_17132a4e9d80963619018d160a69760931f1537016d3eb858f4fc9be32084795->leave($__internal_17132a4e9d80963619018d160a69760931f1537016d3eb858f4fc9be32084795_prof);

    }

    public function getTemplateName()
    {
        return "SistemaFrontBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  279 => 135,  276 => 134,  273 => 133,  271 => 132,  266 => 129,  258 => 127,  256 => 126,  252 => 124,  248 => 122,  241 => 117,  233 => 112,  228 => 110,  221 => 106,  217 => 105,  213 => 104,  208 => 102,  193 => 89,  191 => 88,  186 => 85,  179 => 80,  170 => 75,  168 => 74,  164 => 72,  160 => 70,  157 => 69,  154 => 68,  150 => 66,  148 => 65,  144 => 63,  141 => 62,  137 => 60,  133 => 58,  130 => 57,  128 => 56,  120 => 50,  112 => 48,  110 => 47,  103 => 43,  99 => 42,  94 => 41,  87 => 33,  80 => 24,  77 => 23,  71 => 22,  56 => 14,  50 => 13,  43 => 1,  39 => 9,  36 => 7,  34 => 6,  31 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends "SistemaUserBundle::layout.html.twig" %}*/
/* */
/* {% if is_granted("IS_AUTHENTICATED_REMEMBERED") and is_granted("ROLE_MATRICULADO") %}*/
/*     {% set usuarioEstado = "matriculado" %}*/
/* {% else %}*/
/*     {% if is_granted("IS_AUTHENTICATED_REMEMBERED") and not is_granted("ROLE_MATRICULADO") %}*/
/*         {% set usuarioEstado = "registrado" %}*/
/*     {% else %}*/
/*         {% set usuarioEstado = "visitante" %}*/
/*     {% endif %}*/
/* {% endif %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <style type="text/css">*/
/*         .panel-alto {*/
/*             min-height: 120px;*/
/*         }*/
/*     </style>*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     {% form_theme form 'MWSimpleAdminCrudBundle:widget:fields.html.twig' %}*/
/*     <div class="panel-heading">*/
/*         <div class="panel-title"><h2>Sistema Web de Gesti&oacute;n para Matriculados.</h2></div>*/
/*     </div>*/
/*     <div style="padding-top:10px" class="panel-body">*/
/*         <div class="row">*/
/* {#            <div class="col-lg-12 col-md-12 col-sm-12 text-center">*/
/*                 <h2>Sistema Web de Gesti&oacute;n para Matriculados.</h2>*/
/*                 <br>*/
/*             </div>#}*/
/*             <div class="col-lg-6 col-md-6 col-sm-12">*/
/*                 <div class="panel panel-primary">*/
/*                     <div class="panel-heading">Servicios actuales</div>*/
/*                     <div class="panel-body panel-alto">*/
/*                         <div class="list-group">*/
/*                             {#<a href="#" class="list-group-item active">*/
/*                                 Cras justo odio*/
/*                             </a>#}*/
/*                             <a href="{{ path('front_dato') }}" class="list-group-item">Datos.</a>*/
/*                             <a href="{{ path('front_cuenta') }}" class="list-group-item">Consulta estado de cuenta.</a>*/
/*                             <a href="{{ path('front_trabajo_new') }}" class="list-group-item">Confecci&oacute;n de trabajos.</a>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="panel-footer">*/
/*                         {% if usuarioEstado == "visitante" %}*/
/*                             <a href="{{ path('afiliado') }}" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-log-in"></span> {{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-lg-6 col-md-6 col-sm-12">*/
/*                 <div class="panel panel-danger">*/
/*                     <div class="panel-heading">*/
/*                         {% if usuarioEstado != "matriculado" %}*/
/*                             {% if usuarioEstado == "registrado" %}*/
/*                                 *Recuerde habilitar su usuario, una vez registrado puede hacerlo de las siguientes maneras:*/
/*                             {% else %}*/
/*                                 Registro (por &uacute;nica vez)*/
/*                             {% endif %}*/
/*                         {% endif %}*/
/*                     </div>*/
/*                     <div class="panel-body panel-alto">*/
/*                         {% if usuarioEstado == "matriculado" %}*/
/*                             <p class="text-success">Se encuentra registrado.</p>*/
/*                         {% else %}*/
/*                             {% if usuarioEstado == "registrado" %}*/
/*                             {% else %}*/
/*                                 <p class="text-danger">*Recuerde habilitar su usuario, una vez registrado puede hacerlo de las siguientes maneras:</p>*/
/*                             {% endif %}*/
/* */
/*                             <div class="list-group">*/
/*                                 {% if usuarioEstado == "registrado" %}*/
/*                                     <a href="{{ path('formulario_activacion') }}" class="list-group-item">*/
/*                                         <strong>Click para descargar formulario,</strong><br>*/
/*                                         lo completa y presenta en su delegación*/
/*                                     </a>*/
/*                                 {% else %}*/
/*                                     <a class="list-group-item">*/
/*                                         <strong>Descargando el formulario,</strong><br>*/
/*                                         lo completa y presenta en su delegación*/
/*                                     </a>*/
/*                                 {% endif %}*/
/* */
/*                                 <p></p>*/
/* */
/*                                 {% if usuarioEstado == "registrado" %}*/
/*                                     <!-- Button trigger modal -->*/
/*                                     <button type="button" class="list-group-item list-group-item-success" data-toggle="modal" data-target="#myModal">*/
/*                                         <strong>Click para habilitar en línea,</strong><br>*/
/*                                         ingresando su matrícula*/
/*                                     </button>*/
/*                                     <!-- Modal -->*/
/*                                     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*                                       <div class="modal-dialog" role="document">*/
/*                                         <div class="modal-content">*/
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>*/
/*                                                 <h4 class="modal-title" id="myModalLabel">Complete los datos</h4>*/
/*                                             </div>*/
/*                                             {{ form_start(form) }}*/
/*                                                 <div class="modal-body">*/
/*                                                     {{ form_errors(form) }}*/
/*                                                     {{ form_row(form.titulo) }}*/
/*                                                     {{ form_row(form.matricula) }}*/
/*                                                 </div>*/
/*                                                 <div class="modal-footer">*/
/*                                                     <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>*/
/*                                                     {{ form_row(form.save) }}*/
/*                                                 </div>*/
/*                                             {{ form_end(form) }}*/
/*                                         </div>*/
/*                                       </div>*/
/*                                     </div>*/
/*                                 {% else %}*/
/*                                     <a class="list-group-item list-group-item-success">*/
/*                                         <strong>Habilitando en línea,</strong><br>*/
/*                                         ingresando su matrícula*/
/*                                     </a>*/
/*                                 {% endif %}*/
/*                             </div>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                     <div class="panel-footer">*/
/*                         {% if usuarioEstado == "visitante" %}*/
/*                             <a href="{{ path('fos_user_registration_register') }}" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-user"></span> {{ 'layout.register'|trans({}, 'FOSUserBundle') }}me</a>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             {% if usuarioEstado == "visitante" %}*/
/*                 {% include "SistemaFrontBundle:Default:instructivo.html.twig" with {'descargar': true} %}*/
/*             {% endif %}*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
