<?php

/* MWSimpleAdminCrudBundle:Default:index.html.twig */
class __TwigTemplate_04798c6ef3b711b5a53bab976d68a776740135928840f0af64faf49f1c72247f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MWSimpleAdminCrudBundle::layout.html.twig", "MWSimpleAdminCrudBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'page' => array($this, 'block_page'),
            'buttons' => array($this, 'block_buttons'),
            'actions' => array($this, 'block_actions'),
            'buttonsbelow' => array($this, 'block_buttonsbelow'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MWSimpleAdminCrudBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec8a8a2649b2880f16ac63b8d711b1f4ebaa12277e4a421926f840d8e7db0914 = $this->env->getExtension("native_profiler");
        $__internal_ec8a8a2649b2880f16ac63b8d711b1f4ebaa12277e4a421926f840d8e7db0914->enter($__internal_ec8a8a2649b2880f16ac63b8d711b1f4ebaa12277e4a421926f840d8e7db0914_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ec8a8a2649b2880f16ac63b8d711b1f4ebaa12277e4a421926f840d8e7db0914->leave($__internal_ec8a8a2649b2880f16ac63b8d711b1f4ebaa12277e4a421926f840d8e7db0914_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_7596992c600c2bda3e78625b4dfd3fedaa30649641d20151b4f190bbb4614f32 = $this->env->getExtension("native_profiler");
        $__internal_7596992c600c2bda3e78625b4dfd3fedaa30649641d20151b4f190bbb4614f32->enter($__internal_7596992c600c2bda3e78625b4dfd3fedaa30649641d20151b4f190bbb4614f32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        $this->displayParentBlock("title", $context, $blocks);
        echo " - ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.list", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
";
        
        $__internal_7596992c600c2bda3e78625b4dfd3fedaa30649641d20151b4f190bbb4614f32->leave($__internal_7596992c600c2bda3e78625b4dfd3fedaa30649641d20151b4f190bbb4614f32_prof);

    }

    // line 7
    public function block_page($context, array $blocks = array())
    {
        $__internal_63f0b0d050760aac781f81ea76ebd6db75f9fbf19124b073406c0bf950f9813d = $this->env->getExtension("native_profiler");
        $__internal_63f0b0d050760aac781f81ea76ebd6db75f9fbf19124b073406c0bf950f9813d->enter($__internal_63f0b0d050760aac781f81ea76ebd6db75f9fbf19124b073406c0bf950f9813d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page"));

        // line 8
        $this->env->getExtension('form')->renderer->setTheme((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), array(0 => "MWSimpleAdminCrudBundle:widget:fields.html.twig"));
        // line 9
        echo "<div class=\"row\">
    ";
        // line 10
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), "vars", array()), "errors", array())) > 0)) {
            // line 11
            echo "        <div class=\"col-lg-12 col-md-12 col-sm-6\">
            <div class=\"alert alert-block alert-danger fade in form-errors\">
                ";
            // line 13
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), 'errors');
            echo "
            </div>
            &nbsp;
        </div>
    ";
        }
        // line 18
        echo "    <div class=\"col-lg-12 col-md-12 col-sm-12\">
        <div class=\"filters-right\">
            ";
        // line 20
        $this->displayBlock('buttons', $context, $blocks);
        // line 42
        echo "        </div>
    </div>
    <div id=\"filters\" class=\"well collapse col-lg-12 col-md-12 col-sm-12\">
        ";
        // line 45
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), 'form_start');
        echo "
        ";
        // line 46
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["filterForm"]) ? $context["filterForm"] : $this->getContext($context, "filterForm")), 'form_end');
        echo "
    </div>
</div>
<div class=\"row\">
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <h3>";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.list", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "</h3>
        </div>
        <table class=\"table table-striped table-bordered table-condensed table-responsive\">
            <thead>
                <tr>
                    ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "fieldsindex", array()));
        foreach ($context['_seq'] as $context["key"] => $context["field"]) {
            // line 58
            echo "                        <th";
            if ($this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "isSorted", array(0 => $context["key"]), "method")) {
                echo " class=\"sorted\"";
            }
            echo ">";
            echo $this->env->getExtension('knp_pagination')->sortable($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), $this->getAttribute($context["field"], "label", array()), $context["key"]);
            echo "</th>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                    <th>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.actions", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 65
            echo "                <tr>
                    ";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "fieldsindex", array()));
            foreach ($context['_seq'] as $context["key"] => $context["field"]) {
                // line 67
                echo "                        <td>
                        ";
                // line 68
                if (twig_in_filter($this->getAttribute($context["field"], "type", array()), array(0 => "datetime", 1 => "datetimetz", 2 => "date", 3 => "time"))) {
                    // line 69
                    echo "                            ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array())), $this->getAttribute($context["field"], "date", array())), "html", null, true);
                    echo "
                        ";
                } elseif (twig_in_filter($this->getAttribute(                // line 70
$context["field"], "type", array()), array(0 => "boolean"))) {
                    // line 71
                    echo "                            ";
                    echo $this->env->getExtension('twig.extension')->isActive($this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array())));
                    echo "
                        ";
                } elseif (twig_in_filter($this->getAttribute(                // line 72
$context["field"], "type", array()), array(0 => "ONE_TO_MANY", 1 => "MANY_TO_MANY"))) {
                    // line 73
                    echo "                            ";
                    $context["many_entity"] = $this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array()));
                    // line 74
                    echo "                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["many_entity"]) ? $context["many_entity"] : $this->getContext($context, "many_entity")));
                    foreach ($context['_seq'] as $context["key"] => $context["field_many"]) {
                        // line 75
                        echo "                                ";
                        echo $context["field_many"];
                        echo "
                                ";
                        // line 76
                        if ($this->getAttribute($context["field"], "separator", array(), "any", true, true)) {
                            // line 77
                            echo "                                    ";
                            echo $this->getAttribute($context["field"], "separator", array());
                            echo "
                                ";
                        } else {
                            // line 79
                            echo "                                    |
                                ";
                        }
                        // line 81
                        echo "                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['field_many'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 82
                    echo "                        ";
                } else {
                    // line 83
                    echo "                            ";
                    echo $this->getAttribute($context["entity"], $this->getAttribute($context["field"], "name", array()));
                    echo "
                        ";
                }
                // line 85
                echo "                        </td>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "                    <td>
                        ";
            // line 88
            $this->displayBlock('actions', $context, $blocks);
            // line 92
            echo "                    </td>
                </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "            </tbody>
        </table>
    </div>
    <div class=\"col-lg-12 col-md-12 col-sm-12 navigation\">
        ";
        // line 99
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        echo "
    </div>
    ";
        // line 101
        $this->displayBlock('buttonsbelow', $context, $blocks);
        // line 108
        echo "</div>
";
        
        $__internal_63f0b0d050760aac781f81ea76ebd6db75f9fbf19124b073406c0bf950f9813d->leave($__internal_63f0b0d050760aac781f81ea76ebd6db75f9fbf19124b073406c0bf950f9813d_prof);

    }

    // line 20
    public function block_buttons($context, array $blocks = array())
    {
        $__internal_5f0c4a3f924443d31aedd6e90c42762976e33b13550a47656bf3c21e09fa2501 = $this->env->getExtension("native_profiler");
        $__internal_5f0c4a3f924443d31aedd6e90c42762976e33b13550a47656bf3c21e09fa2501->enter($__internal_5f0c4a3f924443d31aedd6e90c42762976e33b13550a47656bf3c21e09fa2501_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttons"));

        // line 21
        echo "                <a class=\"btn btn-success\" href=\"";
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "new", array()));
        echo "\">
                    ";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.createnew", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
                </a>
                <div class=\"btn-group\">
                    <button type=\"button\" class=\"btn btn-info dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                        ";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.export", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo " <span class=\"caret\"></span>
                    </button>
                  <ul class=\"dropdown-menu\" role=\"menu\">
                    <li><a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "xls"));
        echo "\">EXCEL</a></li>
                    <li><a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "csv"));
        echo "\">CSV</a></li>
                    <li><a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "export", array()), array("format" => "json"));
        echo "\">JSON</a></li>
                  </ul>
                </div>
                <a id=\"reset_index_filters\" class=\"btn btn-danger\">
                    ";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.reset", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
                </a>
                <a class=\"btn btn-primary dropdown-toggle\" data-toggle=\"collapse\" data-target=\"#filters\">
                    ";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.filters", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
                    <span class=\"caret\"></span>
                </a>
            ";
        
        $__internal_5f0c4a3f924443d31aedd6e90c42762976e33b13550a47656bf3c21e09fa2501->leave($__internal_5f0c4a3f924443d31aedd6e90c42762976e33b13550a47656bf3c21e09fa2501_prof);

    }

    // line 88
    public function block_actions($context, array $blocks = array())
    {
        $__internal_1570c126ce1cf218f551916f62001864de550c45995305a564805626c77cc84b = $this->env->getExtension("native_profiler");
        $__internal_1570c126ce1cf218f551916f62001864de550c45995305a564805626c77cc84b->enter($__internal_1570c126ce1cf218f551916f62001864de550c45995305a564805626c77cc84b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 89
        echo "                            <a class=\"glyphicon glyphicon-search tooltips\"  href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "show", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.actions.show", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "\"></a>
                            <a class=\"glyphicon glyphicon-edit tooltips\"  href=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "edit", array()), array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.actions.edit", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "\"></a>
                        ";
        
        $__internal_1570c126ce1cf218f551916f62001864de550c45995305a564805626c77cc84b->leave($__internal_1570c126ce1cf218f551916f62001864de550c45995305a564805626c77cc84b_prof);

    }

    // line 101
    public function block_buttonsbelow($context, array $blocks = array())
    {
        $__internal_6533466e80bbaf2479dd5397b967b5b5000cf148268cd2e6785a8801ba8fe23a = $this->env->getExtension("native_profiler");
        $__internal_6533466e80bbaf2479dd5397b967b5b5000cf148268cd2e6785a8801ba8fe23a->enter($__internal_6533466e80bbaf2479dd5397b967b5b5000cf148268cd2e6785a8801ba8fe23a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonsbelow"));

        // line 102
        echo "        <div class=\"col-lg-8 col-md-8 col-sm-8\">
            <a class=\"btn btn-success likepaginator\" href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "new", array()));
        echo "\">
                ";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.createnew", array("%entity%" => $this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "entityName", array())), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "
            </a>
        </div>
    ";
        
        $__internal_6533466e80bbaf2479dd5397b967b5b5000cf148268cd2e6785a8801ba8fe23a->leave($__internal_6533466e80bbaf2479dd5397b967b5b5000cf148268cd2e6785a8801ba8fe23a_prof);

    }

    // line 111
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_ec8f971d0f6a9dc5493b371ebf966a3113572169daea60d0c9ada1b4ddfa2bb7 = $this->env->getExtension("native_profiler");
        $__internal_ec8f971d0f6a9dc5493b371ebf966a3113572169daea60d0c9ada1b4ddfa2bb7->enter($__internal_ec8f971d0f6a9dc5493b371ebf966a3113572169daea60d0c9ada1b4ddfa2bb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 112
        echo "    <script type=\"text/javascript\">
        \$('.tooltips').tooltip();
        \$( \"#reset_index_filters\" ).click(function() {
            \$(\".reset_submit_filters\").trigger(\"click\");
        });
    </script>
";
        
        $__internal_ec8f971d0f6a9dc5493b371ebf966a3113572169daea60d0c9ada1b4ddfa2bb7->leave($__internal_ec8f971d0f6a9dc5493b371ebf966a3113572169daea60d0c9ada1b4ddfa2bb7_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  380 => 112,  374 => 111,  363 => 104,  359 => 103,  356 => 102,  350 => 101,  339 => 90,  332 => 89,  326 => 88,  315 => 38,  309 => 35,  302 => 31,  298 => 30,  294 => 29,  288 => 26,  281 => 22,  276 => 21,  270 => 20,  262 => 108,  260 => 101,  255 => 99,  249 => 95,  233 => 92,  231 => 88,  228 => 87,  221 => 85,  215 => 83,  212 => 82,  206 => 81,  202 => 79,  196 => 77,  194 => 76,  189 => 75,  184 => 74,  181 => 73,  179 => 72,  174 => 71,  172 => 70,  167 => 69,  165 => 68,  162 => 67,  158 => 66,  155 => 65,  138 => 64,  130 => 60,  117 => 58,  113 => 57,  105 => 52,  96 => 46,  92 => 45,  87 => 42,  85 => 20,  81 => 18,  73 => 13,  69 => 11,  67 => 10,  64 => 9,  62 => 8,  56 => 7,  45 => 4,  39 => 3,  11 => 1,);
    }
}
/* {% extends 'MWSimpleAdminCrudBundle::layout.html.twig' %}*/
/* */
/* {% block title %}*/
/* {{ parent() }} - {{ 'views.index.list'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}*/
/* {% endblock %}*/
/* */
/* {% block page %}*/
/* {% form_theme filterForm 'MWSimpleAdminCrudBundle:widget:fields.html.twig' %}*/
/* <div class="row">*/
/*     {% if filterForm.vars.errors|length > 0 %}*/
/*         <div class="col-lg-12 col-md-12 col-sm-6">*/
/*             <div class="alert alert-block alert-danger fade in form-errors">*/
/*                 {{ form_errors(filterForm) }}*/
/*             </div>*/
/*             &nbsp;*/
/*         </div>*/
/*     {% endif %}*/
/*     <div class="col-lg-12 col-md-12 col-sm-12">*/
/*         <div class="filters-right">*/
/*             {% block buttons %}*/
/*                 <a class="btn btn-success" href="{{ path(config.new) }}">*/
/*                     {{ 'views.index.createnew'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}*/
/*                 </a>*/
/*                 <div class="btn-group">*/
/*                     <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">*/
/*                         {{ 'views.index.export'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }} <span class="caret"></span>*/
/*                     </button>*/
/*                   <ul class="dropdown-menu" role="menu">*/
/*                     <li><a href="{{ path(config.export, { 'format': "xls" }) }}">EXCEL</a></li>*/
/*                     <li><a href="{{ path(config.export, { 'format': "csv" }) }}">CSV</a></li>*/
/*                     <li><a href="{{ path(config.export, { 'format': "json" }) }}">JSON</a></li>*/
/*                   </ul>*/
/*                 </div>*/
/*                 <a id="reset_index_filters" class="btn btn-danger">*/
/*                     {{ 'views.index.reset'|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*                 </a>*/
/*                 <a class="btn btn-primary dropdown-toggle" data-toggle="collapse" data-target="#filters">*/
/*                     {{ 'views.index.filters'|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*                     <span class="caret"></span>*/
/*                 </a>*/
/*             {% endblock %}*/
/*         </div>*/
/*     </div>*/
/*     <div id="filters" class="well collapse col-lg-12 col-md-12 col-sm-12">*/
/*         {{ form_start(filterForm) }}*/
/*         {{ form_end(filterForm) }}*/
/*     </div>*/
/* </div>*/
/* <div class="row">*/
/*     <div class="panel panel-default">*/
/*         <div class="panel-heading">*/
/*             <h3>{{ 'views.index.list'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}</h3>*/
/*         </div>*/
/*         <table class="table table-striped table-bordered table-condensed table-responsive">*/
/*             <thead>*/
/*                 <tr>*/
/*                     {% for key, field in config.fieldsindex %}*/
/*                         <th{% if entities.isSorted(key) %} class="sorted"{% endif %}>{{ knp_pagination_sortable(entities, field.label, key) }}</th>*/
/*                     {% endfor %}*/
/*                     <th>{{ 'views.index.actions'|trans({}, 'MWSimpleAdminCrudBundle') }}</th>*/
/*                 </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for entity in entities %}*/
/*                 <tr>*/
/*                     {% for key, field in config.fieldsindex %}*/
/*                         <td>*/
/*                         {% if field.type in ['datetime', 'datetimetz', 'date', 'time'] %}*/
/*                             {{ attribute(entity, field.name)|date(field.date) }}*/
/*                         {% elseif field.type in ['boolean'] %}*/
/*                             {{ isActive(attribute(entity, field.name)) }}*/
/*                         {% elseif field.type in ['ONE_TO_MANY', 'MANY_TO_MANY'] %}*/
/*                             {% set many_entity = attribute(entity, field.name) %}*/
/*                             {% for key, field_many in many_entity %}*/
/*                                 {{ field_many|raw }}*/
/*                                 {% if field.separator is defined %}*/
/*                                     {{ field.separator|raw }}*/
/*                                 {% else %}*/
/*                                     |*/
/*                                 {% endif %}*/
/*                             {% endfor %}*/
/*                         {% else %}*/
/*                             {{ attribute(entity, field.name)|raw }}*/
/*                         {% endif %}*/
/*                         </td>*/
/*                     {% endfor %}*/
/*                     <td>*/
/*                         {% block actions %}*/
/*                             <a class="glyphicon glyphicon-search tooltips"  href="{{ path(config.show, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.show'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>*/
/*                             <a class="glyphicon glyphicon-edit tooltips"  href="{{ path(config.edit, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.edit'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>*/
/*                         {% endblock %}*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div>*/
/*     <div class="col-lg-12 col-md-12 col-sm-12 navigation">*/
/*         {{ knp_pagination_render(entities) }}*/
/*     </div>*/
/*     {% block buttonsbelow %}*/
/*         <div class="col-lg-8 col-md-8 col-sm-8">*/
/*             <a class="btn btn-success likepaginator" href="{{ path(config.new) }}">*/
/*                 {{ 'views.index.createnew'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}*/
/*             </a>*/
/*         </div>*/
/*     {% endblock %}*/
/* </div>*/
/* {% endblock %}*/
/* */
/* {% block javascript %}*/
/*     <script type="text/javascript">*/
/*         $('.tooltips').tooltip();*/
/*         $( "#reset_index_filters" ).click(function() {*/
/*             $(".reset_submit_filters").trigger("click");*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
