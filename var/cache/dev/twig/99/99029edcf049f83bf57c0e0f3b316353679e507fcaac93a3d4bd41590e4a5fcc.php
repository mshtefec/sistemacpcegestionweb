<?php

/* SistemaCPCEBundle:Dato:edit.html.twig */
class __TwigTemplate_2c05a845226af3be6776434ca5685b9edec92d532335c270e40d8e7335d5da64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaCPCEBundle::longLayout.html.twig", "SistemaCPCEBundle:Dato:edit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'buttonsbelow' => array($this, 'block_buttonsbelow'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaCPCEBundle::longLayout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a65a0bea7f3c66f18b9f2f6ef83476027dea8d280f3b5c25487824e85e52e4be = $this->env->getExtension("native_profiler");
        $__internal_a65a0bea7f3c66f18b9f2f6ef83476027dea8d280f3b5c25487824e85e52e4be->enter($__internal_a65a0bea7f3c66f18b9f2f6ef83476027dea8d280f3b5c25487824e85e52e4be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Dato:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a65a0bea7f3c66f18b9f2f6ef83476027dea8d280f3b5c25487824e85e52e4be->leave($__internal_a65a0bea7f3c66f18b9f2f6ef83476027dea8d280f3b5c25487824e85e52e4be_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_05b5546340435dcd4b193223ed6fd361a605460ff71d65103d15efffe45112ae = $this->env->getExtension("native_profiler");
        $__internal_05b5546340435dcd4b193223ed6fd361a605460ff71d65103d15efffe45112ae->enter($__internal_05b5546340435dcd4b193223ed6fd361a605460ff71d65103d15efffe45112ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Editando Datos
";
        
        $__internal_05b5546340435dcd4b193223ed6fd361a605460ff71d65103d15efffe45112ae->leave($__internal_05b5546340435dcd4b193223ed6fd361a605460ff71d65103d15efffe45112ae_prof);

    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_696886f34b76d28c80b040f095c315b80a8787fa7157cfdf939eb2d979719975 = $this->env->getExtension("native_profiler");
        $__internal_696886f34b76d28c80b040f095c315b80a8787fa7157cfdf939eb2d979719975->enter($__internal_696886f34b76d28c80b040f095c315b80a8787fa7157cfdf939eb2d979719975_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        echo "\t";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "MWSimpleAdminCrudBundle:widget:fields.html.twig"));
        // line 9
        echo "\t
\t\t\t
\t";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
\t\t<div class=\"panel panel-primary center-block\">
\t    \t<div class=\"panel-heading\">Matriculado</div>
\t\t    <div class=\"panel-body\">
\t            <div class=\"form-group\">
\t\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t\t";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiDireccion", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6\">
\t                    ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiLocalidad", array()), 'row');
        echo "
\t                </div>
\t        \t</div>
\t        \t<div class=\"form-group\">
\t\t\t\t\t<div class=\"col-lg-6\">
\t                    ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiProvincia", array()), 'row');
        echo "
\t                </div>
\t                <div class=\"col-lg-6\">
\t                    ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiCodpos", array()), 'row');
        echo "
\t                </div>
\t        \t</div>
\t        \t<div class=\"form-group\">
\t\t\t\t\t<div class=\"col-lg-4\">
\t\t\t\t\t\t";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiTelefono1", array()), 'row');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-4\">
\t                    ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiCelular", array()), 'row');
        echo "
\t                </div>
\t                <div class=\"col-lg-4\">
\t                    ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiMail", array()), 'row');
        echo "
\t                </div>
\t        \t</div>
\t\t\t</div>
\t    </div>

\t    <div class=\"panel panel-primary\">
\t        <div class=\"panel-heading\">Datos Familiares</div>
\t        <div class=\"panel-body\">
\t            <table class=\"table table-striped\">
\t                <thead>
\t                    <tr>
\t                        <th>#</th>
\t                        <th>Dni</th>
\t                        <th>Nombre</th>
\t                        <th>Fecha de Nacimiento</th>
\t                        <th>Sexo</th>
\t                        <th>Fil</th>
\t                    </tr>
\t                </thead>
\t                <tbody>
\t                    <tr>
\t                        <th scope=\"row\">1</th>
\t                        <td>";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiDoc1", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiAut1", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiNac1", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiSex1", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiFil1", array()), 'widget');
        echo "</td>
\t                    </tr>
\t                    <tr>
\t                        <th scope=\"row\">2</th>
\t                        <td>";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiDoc2", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiAut2", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiNac2", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiSex2", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiFil2", array()), 'widget');
        echo "</td>
\t                    </tr>
\t                    <tr>
\t                        <th scope=\"row\">3</th>
\t                        <td>";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiDoc3", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiAut3", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiNac3", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiSex3", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiFil3", array()), 'widget');
        echo "</td>
\t                    </tr>
\t                    <tr>
\t                        <th scope=\"row\">4</th>
\t                        <td>";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiDoc4", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiAut4", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiNac4", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiSex4", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiFil4", array()), 'widget');
        echo "</td>
\t                    </tr>
\t                    <tr>
\t                        <th scope=\"row\">5</th>
\t                        <td>";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiDoc5", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiAut5", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiNac5", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 97
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiSex5", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiFil5", array()), 'widget');
        echo "</td>
\t                    </tr>
\t                    <tr>
\t                        <th scope=\"row\">6</th>
\t                        <td>";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiDoc6", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiAut6", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiNac6", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 105
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiSex6", array()), 'widget');
        echo "</td>
\t                        <td>";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "afiFil6", array()), 'widget');
        echo "</td>
\t                    </tr>
\t                </tbody>
\t            </table>
\t        </div>
\t    </div>

        \t";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
\t";
        // line 114
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
       
    <div class=\"row\">
        ";
        // line 117
        $this->displayBlock('buttonsbelow', $context, $blocks);
        // line 128
        echo "    </div>
";
        
        $__internal_696886f34b76d28c80b040f095c315b80a8787fa7157cfdf939eb2d979719975->leave($__internal_696886f34b76d28c80b040f095c315b80a8787fa7157cfdf939eb2d979719975_prof);

    }

    // line 117
    public function block_buttonsbelow($context, array $blocks = array())
    {
        $__internal_7e224939b53422d52fbd8f8e56bc2152072dc8613c9e239de44b6eaad6d978e2 = $this->env->getExtension("native_profiler");
        $__internal_7e224939b53422d52fbd8f8e56bc2152072dc8613c9e239de44b6eaad6d978e2->enter($__internal_7e224939b53422d52fbd8f8e56bc2152072dc8613c9e239de44b6eaad6d978e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonsbelow"));

        // line 118
        echo "            <div class=\"row\">
\t\t        <div class=\"col-lg-12 col-md-12 col-sm-12\">
\t\t            <div class=\"float-left col-lg-3 col-md-3 col-sm-3\">
\t\t                <a class=\"btn btn-primary col-lg-12\" href=\"";
        // line 121
        echo $this->env->getExtension('routing')->getPath("front_dato");
        echo "\">
\t\t                    Volver
\t\t                </a>
\t\t            </div>
\t\t        </div>
\t\t    </div>
        ";
        
        $__internal_7e224939b53422d52fbd8f8e56bc2152072dc8613c9e239de44b6eaad6d978e2->leave($__internal_7e224939b53422d52fbd8f8e56bc2152072dc8613c9e239de44b6eaad6d978e2_prof);

    }

    // line 131
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_db13df0a27f74d2ea5d9480093cf72d666c95e4a1f0099fc218c3f393756edcf = $this->env->getExtension("native_profiler");
        $__internal_db13df0a27f74d2ea5d9480093cf72d666c95e4a1f0099fc218c3f393756edcf->enter($__internal_db13df0a27f74d2ea5d9480093cf72d666c95e4a1f0099fc218c3f393756edcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 132
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/jasny-bootstrap/css/jasny-bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_db13df0a27f74d2ea5d9480093cf72d666c95e4a1f0099fc218c3f393756edcf->leave($__internal_db13df0a27f74d2ea5d9480093cf72d666c95e4a1f0099fc218c3f393756edcf_prof);

    }

    // line 136
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_c602c63d580d0fdcabb7d8a2a0fcf09d1591e7bf85ddfe84c43c0ed1abf1fa7f = $this->env->getExtension("native_profiler");
        $__internal_c602c63d580d0fdcabb7d8a2a0fcf09d1591e7bf85ddfe84c43c0ed1abf1fa7f->enter($__internal_c602c63d580d0fdcabb7d8a2a0fcf09d1591e7bf85ddfe84c43c0ed1abf1fa7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 137
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "
    <script src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/widget.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/jasny-bootstrap/js/jasny-bootstrap.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_c602c63d580d0fdcabb7d8a2a0fcf09d1591e7bf85ddfe84c43c0ed1abf1fa7f->leave($__internal_c602c63d580d0fdcabb7d8a2a0fcf09d1591e7bf85ddfe84c43c0ed1abf1fa7f_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Dato:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  367 => 140,  363 => 139,  359 => 138,  354 => 137,  348 => 136,  339 => 133,  334 => 132,  328 => 131,  314 => 121,  309 => 118,  303 => 117,  295 => 128,  293 => 117,  287 => 114,  283 => 113,  273 => 106,  269 => 105,  265 => 104,  261 => 103,  257 => 102,  250 => 98,  246 => 97,  242 => 96,  238 => 95,  234 => 94,  227 => 90,  223 => 89,  219 => 88,  215 => 87,  211 => 86,  204 => 82,  200 => 81,  196 => 80,  192 => 79,  188 => 78,  181 => 74,  177 => 73,  173 => 72,  169 => 71,  165 => 70,  158 => 66,  154 => 65,  150 => 64,  146 => 63,  142 => 62,  116 => 39,  110 => 36,  104 => 33,  96 => 28,  90 => 25,  82 => 20,  76 => 17,  67 => 11,  63 => 9,  60 => 8,  54 => 7,  44 => 4,  38 => 3,  11 => 1,);
    }
}
/* {% extends 'SistemaCPCEBundle::longLayout.html.twig' %}*/
/* */
/* {% block title %}*/
/* 	{{ parent() }} - Editando Datos*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/* 	{% form_theme form 'MWSimpleAdminCrudBundle:widget:fields.html.twig' %}*/
/* 	*/
/* 			*/
/* 	{{ form_start(form) }}*/
/* 		<div class="panel panel-primary center-block">*/
/* 	    	<div class="panel-heading">Matriculado</div>*/
/* 		    <div class="panel-body">*/
/* 	            <div class="form-group">*/
/* 					<div class="col-lg-6">*/
/* 						{{ form_row(form.afiDireccion) }}*/
/* 					</div>*/
/* 					<div class="col-lg-6">*/
/* 	                    {{ form_row(form.afiLocalidad) }}*/
/* 	                </div>*/
/* 	        	</div>*/
/* 	        	<div class="form-group">*/
/* 					<div class="col-lg-6">*/
/* 	                    {{ form_row(form.afiProvincia) }}*/
/* 	                </div>*/
/* 	                <div class="col-lg-6">*/
/* 	                    {{ form_row(form.afiCodpos) }}*/
/* 	                </div>*/
/* 	        	</div>*/
/* 	        	<div class="form-group">*/
/* 					<div class="col-lg-4">*/
/* 						{{ form_row(form.afiTelefono1) }}*/
/* 					</div>*/
/* 					<div class="col-lg-4">*/
/* 	                    {{ form_row(form.afiCelular) }}*/
/* 	                </div>*/
/* 	                <div class="col-lg-4">*/
/* 	                    {{ form_row(form.afiMail) }}*/
/* 	                </div>*/
/* 	        	</div>*/
/* 			</div>*/
/* 	    </div>*/
/* */
/* 	    <div class="panel panel-primary">*/
/* 	        <div class="panel-heading">Datos Familiares</div>*/
/* 	        <div class="panel-body">*/
/* 	            <table class="table table-striped">*/
/* 	                <thead>*/
/* 	                    <tr>*/
/* 	                        <th>#</th>*/
/* 	                        <th>Dni</th>*/
/* 	                        <th>Nombre</th>*/
/* 	                        <th>Fecha de Nacimiento</th>*/
/* 	                        <th>Sexo</th>*/
/* 	                        <th>Fil</th>*/
/* 	                    </tr>*/
/* 	                </thead>*/
/* 	                <tbody>*/
/* 	                    <tr>*/
/* 	                        <th scope="row">1</th>*/
/* 	                        <td>{{ form_widget(form.afiDoc1) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiAut1) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiNac1) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiSex1) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiFil1) }}</td>*/
/* 	                    </tr>*/
/* 	                    <tr>*/
/* 	                        <th scope="row">2</th>*/
/* 	                        <td>{{ form_widget(form.afiDoc2) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiAut2) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiNac2) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiSex2) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiFil2) }}</td>*/
/* 	                    </tr>*/
/* 	                    <tr>*/
/* 	                        <th scope="row">3</th>*/
/* 	                        <td>{{ form_widget(form.afiDoc3) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiAut3) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiNac3) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiSex3) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiFil3) }}</td>*/
/* 	                    </tr>*/
/* 	                    <tr>*/
/* 	                        <th scope="row">4</th>*/
/* 	                        <td>{{ form_widget(form.afiDoc4) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiAut4) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiNac4) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiSex4) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiFil4) }}</td>*/
/* 	                    </tr>*/
/* 	                    <tr>*/
/* 	                        <th scope="row">5</th>*/
/* 	                        <td>{{ form_widget(form.afiDoc5) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiAut5) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiNac5) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiSex5) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiFil5) }}</td>*/
/* 	                    </tr>*/
/* 	                    <tr>*/
/* 	                        <th scope="row">6</th>*/
/* 	                        <td>{{ form_widget(form.afiDoc6) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiAut6) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiNac6) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiSex6) }}</td>*/
/* 	                        <td>{{ form_widget(form.afiFil6) }}</td>*/
/* 	                    </tr>*/
/* 	                </tbody>*/
/* 	            </table>*/
/* 	        </div>*/
/* 	    </div>*/
/* */
/*         	{{ form_widget(form) }}*/
/* 	{{ form_end(form) }}*/
/*        */
/*     <div class="row">*/
/*         {% block buttonsbelow %}*/
/*             <div class="row">*/
/* 		        <div class="col-lg-12 col-md-12 col-sm-12">*/
/* 		            <div class="float-left col-lg-3 col-md-3 col-sm-3">*/
/* 		                <a class="btn btn-primary col-lg-12" href="{{ path('front_dato') }}">*/
/* 		                    Volver*/
/* 		                </a>*/
/* 		            </div>*/
/* 		        </div>*/
/* 		    </div>*/
/*         {% endblock %}*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('bundles/sistemacpce/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block javascript %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('bundles/mwsimpleadmincrud/js/widget.js') }}"></script>*/
/*     <script src="{{ asset('bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js') }}"></script>*/
/*     <script src="{{ asset('bundles/sistemacpce/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>*/
/* {% endblock %}*/
