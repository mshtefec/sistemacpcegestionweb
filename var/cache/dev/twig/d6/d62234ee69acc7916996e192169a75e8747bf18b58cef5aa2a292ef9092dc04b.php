<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_424fbc7f70392ce69ffb89cfbcb7d9715f51a777dda62e75b6d358012de9d99f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_10cfa9b0fd958114bfdb35daf18db52b3f0691d88e2c48889ef28be2f7ff1a02 = $this->env->getExtension("native_profiler");
        $__internal_10cfa9b0fd958114bfdb35daf18db52b3f0691d88e2c48889ef28be2f7ff1a02->enter($__internal_10cfa9b0fd958114bfdb35daf18db52b3f0691d88e2c48889ef28be2f7ff1a02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_10cfa9b0fd958114bfdb35daf18db52b3f0691d88e2c48889ef28be2f7ff1a02->leave($__internal_10cfa9b0fd958114bfdb35daf18db52b3f0691d88e2c48889ef28be2f7ff1a02_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6fd151c746c26ed2b0f4febad1d316795babcc8579025566e89a9ad634955331 = $this->env->getExtension("native_profiler");
        $__internal_6fd151c746c26ed2b0f4febad1d316795babcc8579025566e89a9ad634955331->enter($__internal_6fd151c746c26ed2b0f4febad1d316795babcc8579025566e89a9ad634955331_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_6fd151c746c26ed2b0f4febad1d316795babcc8579025566e89a9ad634955331->leave($__internal_6fd151c746c26ed2b0f4febad1d316795babcc8579025566e89a9ad634955331_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_fdb304ba88b94b9d68dd1cfd9b73c903f1f772e31ae22331312c1c32bb2c06eb = $this->env->getExtension("native_profiler");
        $__internal_fdb304ba88b94b9d68dd1cfd9b73c903f1f772e31ae22331312c1c32bb2c06eb->enter($__internal_fdb304ba88b94b9d68dd1cfd9b73c903f1f772e31ae22331312c1c32bb2c06eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_fdb304ba88b94b9d68dd1cfd9b73c903f1f772e31ae22331312c1c32bb2c06eb->leave($__internal_fdb304ba88b94b9d68dd1cfd9b73c903f1f772e31ae22331312c1c32bb2c06eb_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_e4ed850765c0b3ededfac9ef0604578daa3f5b5b6224ee32694e8b3f378bf180 = $this->env->getExtension("native_profiler");
        $__internal_e4ed850765c0b3ededfac9ef0604578daa3f5b5b6224ee32694e8b3f378bf180->enter($__internal_e4ed850765c0b3ededfac9ef0604578daa3f5b5b6224ee32694e8b3f378bf180_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_e4ed850765c0b3ededfac9ef0604578daa3f5b5b6224ee32694e8b3f378bf180->leave($__internal_e4ed850765c0b3ededfac9ef0604578daa3f5b5b6224ee32694e8b3f378bf180_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
