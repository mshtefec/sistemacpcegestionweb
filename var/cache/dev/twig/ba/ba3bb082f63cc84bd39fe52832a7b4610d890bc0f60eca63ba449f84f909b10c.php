<?php

/* MWSimpleAdminCrudBundle:Form:field_file.html.twig */
class __TwigTemplate_bbc9a0d26f1b0a22a7e68675c7db7a55315ca68a123a02d184140193e7dd0352 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'mws_field_file_widget' => array($this, 'block_mws_field_file_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01d46a5d2633be9853101ccd7f5aa19640a7cfe5889df7e63bbb467038f7b4d0 = $this->env->getExtension("native_profiler");
        $__internal_01d46a5d2633be9853101ccd7f5aa19640a7cfe5889df7e63bbb467038f7b4d0->enter($__internal_01d46a5d2633be9853101ccd7f5aa19640a7cfe5889df7e63bbb467038f7b4d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle:Form:field_file.html.twig"));

        // line 1
        $this->displayBlock('mws_field_file_widget', $context, $blocks);
        
        $__internal_01d46a5d2633be9853101ccd7f5aa19640a7cfe5889df7e63bbb467038f7b4d0->leave($__internal_01d46a5d2633be9853101ccd7f5aa19640a7cfe5889df7e63bbb467038f7b4d0_prof);

    }

    public function block_mws_field_file_widget($context, array $blocks = array())
    {
        $__internal_e399ed0d7c695e1dba0a69cf55b8f19bda4d26517b27f1067d7a1e58614cbbe4 = $this->env->getExtension("native_profiler");
        $__internal_e399ed0d7c695e1dba0a69cf55b8f19bda4d26517b27f1067d7a1e58614cbbe4->enter($__internal_e399ed0d7c695e1dba0a69cf55b8f19bda4d26517b27f1067d7a1e58614cbbe4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mws_field_file_widget"));

        // line 2
        echo "    ";
        ob_start();
        // line 3
        echo "        <div class=\"fileupload fileupload-new\" data-provides=\"fileupload\">
                ";
        // line 4
        if ( !(null === (isset($context["preview_image"]) ? $context["preview_image"] : $this->getContext($context, "preview_image")))) {
            // line 5
            echo "                    <div class=\"fileupload-new thumbnail\" style=\"width: 200px; height: 150px;\">
                    ";
            // line 6
            if ( !(null === (isset($context["file_url"]) ? $context["file_url"] : $this->getContext($context, "file_url")))) {
                // line 7
                echo "                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl((isset($context["file_url"]) ? $context["file_url"] : $this->getContext($context, "file_url"))), "html", null, true);
                echo "\" />
                    ";
            } else {
                // line 9
                echo "                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/img/no_image.gif"), "html", null, true);
                echo "\" />
                    ";
            }
            // line 11
            echo "                    </div>
                ";
        } else {
            // line 13
            echo "                    <div class=\"fileupload-new\">
                    ";
            // line 14
            if ( !(null === (isset($context["file_url"]) ? $context["file_url"] : $this->getContext($context, "file_url")))) {
                // line 15
                echo "                        <div class=\"alert alert-success\">";
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
                echo "</div>
                    ";
            } else {
                // line 17
                echo "                        <span class=\"glyphicon glyphicon-remove\"></span>
                    ";
            }
            // line 19
            echo "                    </div>
                ";
        }
        // line 21
        echo "            <div class=\"fileupload-preview fileupload-exists thumbnail\" style=\"max-width: 200px; max-height: 150px; line-height: 20px;\">
            </div>
            ";
        // line 23
        if (( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))) && ((isset($context["file_path"]) ? $context["file_path"] : $this->getContext($context, "file_path")) == true))) {
            // line 24
            echo "                <br>/";
            echo twig_escape_filter($this->env, (isset($context["file_url"]) ? $context["file_url"] : $this->getContext($context, "file_url")), "html", null, true);
            echo "
            ";
        }
        // line 26
        echo "            <div>
                <span class=\"btn btn-primary btn-file\">
                    <span class=\"fileupload-new\">Seleccionar</span>
                    <span class=\"fileupload-exists\">Cambiar</span>
                    <input type=\"";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo "
                        ";
        // line 31
        if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        echo "/>
                </span>
                <a href=\"#\" class=\"btn btn-danger fileupload-exists\" data-dismiss=\"fileupload\">Remover</a>
            </div>
        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_e399ed0d7c695e1dba0a69cf55b8f19bda4d26517b27f1067d7a1e58614cbbe4->leave($__internal_e399ed0d7c695e1dba0a69cf55b8f19bda4d26517b27f1067d7a1e58614cbbe4_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle:Form:field_file.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  107 => 31,  101 => 30,  95 => 26,  89 => 24,  87 => 23,  83 => 21,  79 => 19,  75 => 17,  69 => 15,  67 => 14,  64 => 13,  60 => 11,  54 => 9,  48 => 7,  46 => 6,  43 => 5,  41 => 4,  38 => 3,  35 => 2,  23 => 1,);
    }
}
/* {% block mws_field_file_widget %}*/
/*     {% spaceless %}*/
/*         <div class="fileupload fileupload-new" data-provides="fileupload">*/
/*                 {% if preview_image is not null %}*/
/*                     <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">*/
/*                     {% if file_url is not null %}*/
/*                         <img src="{{ asset(file_url) }}" />*/
/*                     {% else %}*/
/*                         <img src="{{ asset('bundles/mwsimpleadmincrud/img/no_image.gif') }}" />*/
/*                     {% endif %}*/
/*                     </div>*/
/*                 {% else %}*/
/*                     <div class="fileupload-new">*/
/*                     {% if file_url is not null %}*/
/*                         <div class="alert alert-success">{{ value }}</div>*/
/*                     {% else %}*/
/*                         <span class="glyphicon glyphicon-remove"></span>*/
/*                     {% endif %}*/
/*                     </div>*/
/*                 {% endif %}*/
/*             <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">*/
/*             </div>*/
/*             {% if value is not empty and file_path == true %}*/
/*                 <br>/{{ file_url }}*/
/*             {% endif %}*/
/*             <div>*/
/*                 <span class="btn btn-primary btn-file">*/
/*                     <span class="fileupload-new">Seleccionar</span>*/
/*                     <span class="fileupload-exists">Cambiar</span>*/
/*                     <input type="{{ type }}" {{ block('widget_attributes') }}*/
/*                         {% if value is not empty %}value="{{ value }}"{% endif %}/>*/
/*                 </span>*/
/*                 <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remover</a>*/
/*             </div>*/
/*         </div>*/
/*     {% endspaceless %}*/
/* {% endblock %}*/
/* */
