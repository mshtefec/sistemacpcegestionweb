<?php

/* SistemaCPCEBundle:CuentaAdmin:index.html.twig */
class __TwigTemplate_4dd698a89cf37db64c3d4dd282a8e42082e4215caad7d63735a39ff456bf7d0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MWSimpleAdminCrudBundle:Default:index.html.twig", "SistemaCPCEBundle:CuentaAdmin:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'menu' => array($this, 'block_menu'),
            'page' => array($this, 'block_page'),
            'actions' => array($this, 'block_actions'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MWSimpleAdminCrudBundle:Default:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6670a75bcc328b183f5e1ef9faa26c51e56e9ff3687310c0d769d09cb732cee3 = $this->env->getExtension("native_profiler");
        $__internal_6670a75bcc328b183f5e1ef9faa26c51e56e9ff3687310c0d769d09cb732cee3->enter($__internal_6670a75bcc328b183f5e1ef9faa26c51e56e9ff3687310c0d769d09cb732cee3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:CuentaAdmin:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6670a75bcc328b183f5e1ef9faa26c51e56e9ff3687310c0d769d09cb732cee3->leave($__internal_6670a75bcc328b183f5e1ef9faa26c51e56e9ff3687310c0d769d09cb732cee3_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8b16e3ac97f7bada8744a9b587da61cfbdbb6be277652f49c14af7e276afec85 = $this->env->getExtension("native_profiler");
        $__internal_8b16e3ac97f7bada8744a9b587da61cfbdbb6be277652f49c14af7e276afec85->enter($__internal_8b16e3ac97f7bada8744a9b587da61cfbdbb6be277652f49c14af7e276afec85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Estado de Cuenta
";
        
        $__internal_8b16e3ac97f7bada8744a9b587da61cfbdbb6be277652f49c14af7e276afec85->leave($__internal_8b16e3ac97f7bada8744a9b587da61cfbdbb6be277652f49c14af7e276afec85_prof);

    }

    // line 7
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e505ca12a55df6c08792f00d994ba91fdcbe00acbbc14909bcd10fa48f5651e7 = $this->env->getExtension("native_profiler");
        $__internal_e505ca12a55df6c08792f00d994ba91fdcbe00acbbc14909bcd10fa48f5651e7->enter($__internal_e505ca12a55df6c08792f00d994ba91fdcbe00acbbc14909bcd10fa48f5651e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        
        $__internal_e505ca12a55df6c08792f00d994ba91fdcbe00acbbc14909bcd10fa48f5651e7->leave($__internal_e505ca12a55df6c08792f00d994ba91fdcbe00acbbc14909bcd10fa48f5651e7_prof);

    }

    // line 9
    public function block_page($context, array $blocks = array())
    {
        $__internal_1468d0c911eb232abe9aa8cc761677aff9fbbd80fa03263c023e11b4d5d21656 = $this->env->getExtension("native_profiler");
        $__internal_1468d0c911eb232abe9aa8cc761677aff9fbbd80fa03263c023e11b4d5d21656->enter($__internal_1468d0c911eb232abe9aa8cc761677aff9fbbd80fa03263c023e11b4d5d21656_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page"));

        // line 10
        echo "<div class=\"row\">
    <div class=\"col-lg-12 col-md-12 col-sm-12\">
        <div class=\"filters-right\">
        </div>
    </div>
    <div class=\"col-lg-12 col-md-12 col-sm-12\">
        <div id=\"filters\" class=\"well\">
            <form name=\"input\" action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "index", array()));
        echo "\" method=\"post\">
                Tipo
                <select name=\"tipdoc\">
                    <option value=\"DNI\">DNI</option>
                    <option value=\"LE\">LE</option>
                    <option value=\"LC\">LC</option>
                    <option value=\"CI\">CI</option>
                </select>
                N&uacute;mero:
                <input type=\"number\" name=\"nrodoc\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["nrodoc"]) ? $context["nrodoc"] : $this->getContext($context, "nrodoc")), "html", null, true);
        echo "\" required/>
                <input class=\"btn btn-success\" type=\"submit\" name=\"submit\" value=\"Consultar\">
            </form>
        </div>
    </div>
</div>
<div class=\"row\">
    <div class=\"panel panel-primary\">
        <div class=\"panel-heading\">
            <div class=\"panel-title\">
                ";
        // line 36
        if (twig_test_empty((isset($context["afiliado"]) ? $context["afiliado"] : $this->getContext($context, "afiliado")))) {
            // line 37
            echo "                    <strong>Ingrese número de documento</strong>
                ";
        } else {
            // line 39
            echo "                    <strong>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["afiliado"]) ? $context["afiliado"] : $this->getContext($context, "afiliado")), "afiliado", array()), "html", null, true);
            echo "</strong>
                    ";
            // line 40
            if ((twig_slice($this->env, $this->getAttribute((isset($context["afiliado"]) ? $context["afiliado"] : $this->getContext($context, "afiliado")), "categoria", array()), 0, 2) == 22)) {
                // line 41
                echo "                        ";
                $context["color"] = "darkred";
                // line 42
                echo "                    ";
            } else {
                // line 43
                echo "                        ";
                $context["color"] = "white";
                // line 44
                echo "                    ";
            }
            // line 45
            echo "                    <div class=\"pull-right\">Categoría: <strong style=\"color: ";
            echo twig_escape_filter($this->env, (isset($context["color"]) ? $context["color"] : $this->getContext($context, "color")), "html", null, true);
            echo "\">";
            echo $this->getAttribute((isset($context["afiliado"]) ? $context["afiliado"] : $this->getContext($context, "afiliado")), "catDescripcion", array());
            echo "</strong></div>
                    ";
            // line 46
            if (((isset($context["color"]) ? $context["color"] : $this->getContext($context, "color")) == "white")) {
                // line 47
                echo "                        <form name=\"input\" action=\"";
                echo $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "pdflistado", array()));
                echo "\" method=\"post\">
                            <input type=\"hidden\" name=\"tipdoc\" value=\"";
                // line 48
                echo twig_escape_filter($this->env, (isset($context["tipdoc"]) ? $context["tipdoc"] : $this->getContext($context, "tipdoc")), "html", null, true);
                echo "\"/>
                            <input type=\"hidden\" name=\"nrodoc\" value=\"";
                // line 49
                echo twig_escape_filter($this->env, (isset($context["nrodoc"]) ? $context["nrodoc"] : $this->getContext($context, "nrodoc")), "html", null, true);
                echo "\"/>
                            <input class=\"btn btn-success likepaginator\" type=\"submit\" name=\"submit\" value=\"Imprimir\">
                        </form>
                    ";
            }
            // line 53
            echo "                ";
        }
        // line 54
        echo "            </div>
        </div>
        <div class=\"panel-body\">
            <table class=\"table table-striped table-bordered table-condensed table-responsive\">
                <thead>
                    <tr>
                        ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "fieldsindex", array()));
        foreach ($context['_seq'] as $context["key"] => $context["field"]) {
            // line 61
            echo "                            <th>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "label", array()), "html", null, true);
            echo "</th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "                        <th>Saldo total</th>
                        <th>";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.index.actions", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "</th>
                    </tr>
                </thead>
                <tbody>
                ";
        // line 68
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
            // line 69
            echo "                    <tr>
                        <td>";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "servicio", array()), "html", null, true);
            echo "</td>
                        <td style=";
            // line 71
            if (($this->getAttribute($context["entity"], "saldo", array()) > 0)) {
                echo "\"color:red;\"";
            } else {
                echo "\"color:green;\"";
            }
            echo ">
                            \$";
            // line 72
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, abs($this->getAttribute($context["entity"], "saldo", array())), 2, ",", "."), "html", null, true);
            echo "
                        </td>
                        <td style=";
            // line 74
            if (($this->getAttribute($this->getAttribute((isset($context["saldosTotales"]) ? $context["saldosTotales"] : $this->getContext($context, "saldosTotales")), $context["key"], array(), "array"), "saldo", array()) > 0)) {
                echo "\"color:red;\"";
            } else {
                echo "\"color:green;\"";
            }
            echo ">
                            \$";
            // line 75
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, abs($this->getAttribute($this->getAttribute((isset($context["saldosTotales"]) ? $context["saldosTotales"] : $this->getContext($context, "saldosTotales")), $context["key"], array(), "array"), "saldo", array())), 2, ",", "."), "html", null, true);
            echo "
                        </td>
                        <td>
                            ";
            // line 78
            $this->displayBlock('actions', $context, $blocks);
            // line 82
            echo "                        </td>
                    </tr>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "                </tbody>
            </table>
        </div>
    </div>
    <div class=\"col-lg-12 col-md-12 col-sm-12 navigation\">
        ";
        // line 91
        echo "    </div>
        <div class=\"col-lg-8 col-md-8 col-sm-8\">
            ";
        // line 96
        echo "        </div>
    </div>
";
        
        $__internal_1468d0c911eb232abe9aa8cc761677aff9fbbd80fa03263c023e11b4d5d21656->leave($__internal_1468d0c911eb232abe9aa8cc761677aff9fbbd80fa03263c023e11b4d5d21656_prof);

    }

    // line 78
    public function block_actions($context, array $blocks = array())
    {
        $__internal_d828954883ea132e9c340afc9f885f5b02a09ba9afb623a8aaf4b919794445f5 = $this->env->getExtension("native_profiler");
        $__internal_d828954883ea132e9c340afc9f885f5b02a09ba9afb623a8aaf4b919794445f5->enter($__internal_d828954883ea132e9c340afc9f885f5b02a09ba9afb623a8aaf4b919794445f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 79
        echo "                                <a class=\"glyphicon glyphicon-search tooltips\"  href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute((isset($context["config"]) ? $context["config"] : $this->getContext($context, "config")), "detalle", array()), array("cuenta" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cuenta", array()), "tipdoc" => (isset($context["tipdoc"]) ? $context["tipdoc"] : $this->getContext($context, "tipdoc")), "nrodoc" => (isset($context["nrodoc"]) ? $context["nrodoc"] : $this->getContext($context, "nrodoc")))), "html", null, true);
        echo "\" title=\"\" rel=\"tooltip\" data-original-title=\"Detalle\"></a>
                                ";
        // line 81
        echo "                            ";
        
        $__internal_d828954883ea132e9c340afc9f885f5b02a09ba9afb623a8aaf4b919794445f5->leave($__internal_d828954883ea132e9c340afc9f885f5b02a09ba9afb623a8aaf4b919794445f5_prof);

    }

    // line 100
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_3880002d2eb10624f0beb8b27d3e2ec976d31a6948d6a373c8723c423bfce8dc = $this->env->getExtension("native_profiler");
        $__internal_3880002d2eb10624f0beb8b27d3e2ec976d31a6948d6a373c8723c423bfce8dc->enter($__internal_3880002d2eb10624f0beb8b27d3e2ec976d31a6948d6a373c8723c423bfce8dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 101
        echo "    <script type=\"text/javascript\">
        \$('.tooltips').tooltip();
        \$( \"#reset_index_filters\" ).click(function() {
            \$(\".reset_submit_filters\").trigger(\"click\");
        });
    </script>
";
        
        $__internal_3880002d2eb10624f0beb8b27d3e2ec976d31a6948d6a373c8723c423bfce8dc->leave($__internal_3880002d2eb10624f0beb8b27d3e2ec976d31a6948d6a373c8723c423bfce8dc_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:CuentaAdmin:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  302 => 101,  296 => 100,  289 => 81,  284 => 79,  278 => 78,  269 => 96,  265 => 91,  258 => 85,  242 => 82,  240 => 78,  234 => 75,  226 => 74,  221 => 72,  213 => 71,  209 => 70,  206 => 69,  189 => 68,  182 => 64,  179 => 63,  170 => 61,  166 => 60,  158 => 54,  155 => 53,  148 => 49,  144 => 48,  139 => 47,  137 => 46,  130 => 45,  127 => 44,  124 => 43,  121 => 42,  118 => 41,  116 => 40,  111 => 39,  107 => 37,  105 => 36,  92 => 26,  80 => 17,  71 => 10,  65 => 9,  54 => 7,  44 => 4,  38 => 3,  11 => 1,);
    }
}
/* {% extends 'MWSimpleAdminCrudBundle:Default:index.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }} - Estado de Cuenta*/
/* {% endblock %}*/
/* */
/* {% block menu %}{% endblock %}*/
/* */
/* {% block page %}*/
/* <div class="row">*/
/*     <div class="col-lg-12 col-md-12 col-sm-12">*/
/*         <div class="filters-right">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-lg-12 col-md-12 col-sm-12">*/
/*         <div id="filters" class="well">*/
/*             <form name="input" action="{{ path(config.index) }}" method="post">*/
/*                 Tipo*/
/*                 <select name="tipdoc">*/
/*                     <option value="DNI">DNI</option>*/
/*                     <option value="LE">LE</option>*/
/*                     <option value="LC">LC</option>*/
/*                     <option value="CI">CI</option>*/
/*                 </select>*/
/*                 N&uacute;mero:*/
/*                 <input type="number" name="nrodoc" value="{{ nrodoc }}" required/>*/
/*                 <input class="btn btn-success" type="submit" name="submit" value="Consultar">*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="row">*/
/*     <div class="panel panel-primary">*/
/*         <div class="panel-heading">*/
/*             <div class="panel-title">*/
/*                 {% if afiliado is empty %}*/
/*                     <strong>Ingrese número de documento</strong>*/
/*                 {% else %}*/
/*                     <strong>{{ afiliado.afiliado }}</strong>*/
/*                     {% if afiliado.categoria[0:2] == 22 %}*/
/*                         {% set color = "darkred" %}*/
/*                     {% else %}*/
/*                         {% set color = "white" %}*/
/*                     {% endif %}*/
/*                     <div class="pull-right">Categoría: <strong style="color: {{ color }}">{{ afiliado.catDescripcion|raw }}</strong></div>*/
/*                     {% if color == "white" %}*/
/*                         <form name="input" action="{{ path(config.pdflistado) }}" method="post">*/
/*                             <input type="hidden" name="tipdoc" value="{{ tipdoc }}"/>*/
/*                             <input type="hidden" name="nrodoc" value="{{ nrodoc }}"/>*/
/*                             <input class="btn btn-success likepaginator" type="submit" name="submit" value="Imprimir">*/
/*                         </form>*/
/*                     {% endif %}*/
/*                 {% endif %}*/
/*             </div>*/
/*         </div>*/
/*         <div class="panel-body">*/
/*             <table class="table table-striped table-bordered table-condensed table-responsive">*/
/*                 <thead>*/
/*                     <tr>*/
/*                         {% for key, field in config.fieldsindex %}*/
/*                             <th>{{ field.label }}</th>*/
/*                         {% endfor %}*/
/*                         <th>Saldo total</th>*/
/*                         <th>{{ 'views.index.actions'|trans({}, 'MWSimpleAdminCrudBundle') }}</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                 {% for key, entity in entities %}*/
/*                     <tr>*/
/*                         <td>{{ entity.servicio }}</td>*/
/*                         <td style={% if entity.saldo > 0 %}"color:red;"{% else %}"color:green;"{% endif %}>*/
/*                             ${{ entity.saldo|abs|number_format(2, ',', '.') }}*/
/*                         </td>*/
/*                         <td style={% if saldosTotales[key].saldo > 0 %}"color:red;"{% else %}"color:green;"{% endif %}>*/
/*                             ${{ saldosTotales[key].saldo|abs|number_format(2, ',', '.') }}*/
/*                         </td>*/
/*                         <td>*/
/*                             {% block actions %}*/
/*                                 <a class="glyphicon glyphicon-search tooltips"  href="{{ path(config.detalle, { 'cuenta': entity.cuenta, 'tipdoc': tipdoc, 'nrodoc': nrodoc }) }}" title="" rel="tooltip" data-original-title="Detalle"></a>*/
/*                                 {#<a class="glyphicon glyphicon-edit tooltips"  href="{{ path(config.edit, { 'id': entity.id }) }}" title="" rel="tooltip" data-original-title="{{ 'views.actions.edit'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}"></a>#}*/
/*                             {% endblock %}*/
/*                         </td>*/
/*                     </tr>*/
/*                 {% endfor %}*/
/*                 </tbody>*/
/*             </table>*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-lg-12 col-md-12 col-sm-12 navigation">*/
/*         {#{ knp_pagination_render(entities) }#}*/
/*     </div>*/
/*         <div class="col-lg-8 col-md-8 col-sm-8">*/
/*             {#<a class="btn btn-success likepaginator" href="{{ path(config.new) }}">*/
/*                 {{ 'views.index.createnew'|trans({'%entity%': config.entityName}, 'MWSimpleAdminCrudBundle') }}*/
/*             </a>#}*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascript %}*/
/*     <script type="text/javascript">*/
/*         $('.tooltips').tooltip();*/
/*         $( "#reset_index_filters" ).click(function() {*/
/*             $(".reset_submit_filters").trigger("click");*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
