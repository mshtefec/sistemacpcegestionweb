<?php

/* SistemaUserBundle::layout.html.twig */
class __TwigTemplate_a386630acbd87b2a18ac7a687a65dd5a9a15b9ce554900361ff9bb897fc02530 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'user_lateral' => array($this, 'block_user_lateral'),
            'menu' => array($this, 'block_menu'),
            'wrapper' => array($this, 'block_wrapper'),
            'messages_error' => array($this, 'block_messages_error'),
            'fos_user_panel' => array($this, 'block_fos_user_panel'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e34025436ced9d4f53b02e6fb101791d61f04626f82dfc9dcc45ca572bb1336d = $this->env->getExtension("native_profiler");
        $__internal_e34025436ced9d4f53b02e6fb101791d61f04626f82dfc9dcc45ca572bb1336d->enter($__internal_e34025436ced9d4f53b02e6fb101791d61f04626f82dfc9dcc45ca572bb1336d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaUserBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <!-- Le styles -->
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrap-checkbox.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"bootstrap-style\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/select2/select2.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrapValidator.min.css"), "html", null, true);
        echo "\">
        <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/css/bootstrap-datetimepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        
        <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/crud.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/css/crud.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
        <![endif]-->
        ";
        // line 28
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/js/bootstrap-datetimepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/js/locales/bootstrap-datetimepicker.es.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/select2/select2.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 31
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 32
        echo "        <!-- Le fav and touch icons -->
        <link rel=\"shortcut icon\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">

        <!-- Smartsupp Live Chat script -->
        <script type=\"text/javascript\">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '7d2ca3db7d98ba7a900e432e90bea4e57af41001';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
        </script>
    </head>

    <body class=\"focusedform\">
        ";
        // line 49
        $this->displayBlock('body', $context, $blocks);
        // line 111
        echo "        ";
        $this->displayBlock('footer', $context, $blocks);
        // line 122
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-checkbox.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/validator/language/es_ES.js"), "html", null, true);
        echo "\"></script>
       ";
        // line 126
        $this->displayBlock('javascript', $context, $blocks);
        // line 127
        echo "    </body>
</html>";
        
        $__internal_e34025436ced9d4f53b02e6fb101791d61f04626f82dfc9dcc45ca572bb1336d->leave($__internal_e34025436ced9d4f53b02e6fb101791d61f04626f82dfc9dcc45ca572bb1336d_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_e93f21fa998b1bde9bfb946484363e90b8e92aad267288145f167f433e975091 = $this->env->getExtension("native_profiler");
        $__internal_e93f21fa998b1bde9bfb946484363e90b8e92aad267288145f167f433e975091->enter($__internal_e93f21fa998b1bde9bfb946484363e90b8e92aad267288145f167f433e975091_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.layout.bannersadmin", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        
        $__internal_e93f21fa998b1bde9bfb946484363e90b8e92aad267288145f167f433e975091->leave($__internal_e93f21fa998b1bde9bfb946484363e90b8e92aad267288145f167f433e975091_prof);

    }

    // line 31
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_08d364ab7e1681e17a06ff419a40a32f13213ba8379a2a1d6d0fb127a513519a = $this->env->getExtension("native_profiler");
        $__internal_08d364ab7e1681e17a06ff419a40a32f13213ba8379a2a1d6d0fb127a513519a->enter($__internal_08d364ab7e1681e17a06ff419a40a32f13213ba8379a2a1d6d0fb127a513519a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_08d364ab7e1681e17a06ff419a40a32f13213ba8379a2a1d6d0fb127a513519a->leave($__internal_08d364ab7e1681e17a06ff419a40a32f13213ba8379a2a1d6d0fb127a513519a_prof);

    }

    // line 49
    public function block_body($context, array $blocks = array())
    {
        $__internal_f1e1790e65a4042cd34787386158655b3422428026ac05ca8e587c89a7ce191d = $this->env->getExtension("native_profiler");
        $__internal_f1e1790e65a4042cd34787386158655b3422428026ac05ca8e587c89a7ce191d->enter($__internal_f1e1790e65a4042cd34787386158655b3422428026ac05ca8e587c89a7ce191d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 50
        echo "            ";
        $this->displayBlock('user_lateral', $context, $blocks);
        // line 81
        echo "            ";
        $this->displayBlock('wrapper', $context, $blocks);
        // line 110
        echo "        ";
        
        $__internal_f1e1790e65a4042cd34787386158655b3422428026ac05ca8e587c89a7ce191d->leave($__internal_f1e1790e65a4042cd34787386158655b3422428026ac05ca8e587c89a7ce191d_prof);

    }

    // line 50
    public function block_user_lateral($context, array $blocks = array())
    {
        $__internal_631fe51c3df942c625e6a753c0a329fe47c487a70ede7b2307e87139523d2db4 = $this->env->getExtension("native_profiler");
        $__internal_631fe51c3df942c625e6a753c0a329fe47c487a70ede7b2307e87139523d2db4->enter($__internal_631fe51c3df942c625e6a753c0a329fe47c487a70ede7b2307e87139523d2db4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_lateral"));

        // line 51
        echo "            <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\".navbar-collapse\" aria-expanded=\"false\" aria-controls=\"navbar\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"#\"><img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/images/header_logo.png"), "html", null, true);
        echo "\" alt=\"\" /></a>
                    </div>
                    ";
        // line 62
        $this->displayBlock('menu', $context, $blocks);
        // line 78
        echo "                </div>
            </nav>
            ";
        
        $__internal_631fe51c3df942c625e6a753c0a329fe47c487a70ede7b2307e87139523d2db4->leave($__internal_631fe51c3df942c625e6a753c0a329fe47c487a70ede7b2307e87139523d2db4_prof);

    }

    // line 62
    public function block_menu($context, array $blocks = array())
    {
        $__internal_36a812ec2b6b4c7dbf126093a6a7a083056373e318859bb775949bd984d3920b = $this->env->getExtension("native_profiler");
        $__internal_36a812ec2b6b4c7dbf126093a6a7a083056373e318859bb775949bd984d3920b->enter($__internal_36a812ec2b6b4c7dbf126093a6a7a083056373e318859bb775949bd984d3920b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 63
        echo "                        ";
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 64
            echo "                            <div class=\"sidebar\" role=\"navigation\">
                                <div class=\"sidebar-nav navbar-collapse collapse\">
                                    <ul class=\"nav\">
                                        <li><a class=\"bg-info\">";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "</a></li>
                                    </ul>
                                    ";
            // line 69
            echo $this->env->getExtension('knp_menu')->render("SistemaCPCEBundle:Builder:AfiliadoMenu");
            echo "
                                </div>
                            </div>
                            <script>
                                smartsupp('email', '";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "email", array()), "html", null, true);
            echo "');
                                smartsupp('name', '";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
            echo "');
                            </script>
                        ";
        }
        // line 77
        echo "                    ";
        
        $__internal_36a812ec2b6b4c7dbf126093a6a7a083056373e318859bb775949bd984d3920b->leave($__internal_36a812ec2b6b4c7dbf126093a6a7a083056373e318859bb775949bd984d3920b_prof);

    }

    // line 81
    public function block_wrapper($context, array $blocks = array())
    {
        $__internal_851fb9723e55f8c27048d8a4725dd3314d72759f49f4552e7f0e7042047fb0e2 = $this->env->getExtension("native_profiler");
        $__internal_851fb9723e55f8c27048d8a4725dd3314d72759f49f4552e7f0e7042047fb0e2->enter($__internal_851fb9723e55f8c27048d8a4725dd3314d72759f49f4552e7f0e7042047fb0e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        // line 82
        echo "            <div id=\"page-wrapper\">
                <div class=\"content\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12\">
                            ";
        // line 86
        $this->displayBlock('messages_error', $context, $blocks);
        // line 97
        echo "
                            <div id=\"loginbox\" class=\"mainbox\">
                                ";
        // line 99
        $this->displayBlock('fos_user_panel', $context, $blocks);
        // line 104
        echo "                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ";
        
        $__internal_851fb9723e55f8c27048d8a4725dd3314d72759f49f4552e7f0e7042047fb0e2->leave($__internal_851fb9723e55f8c27048d8a4725dd3314d72759f49f4552e7f0e7042047fb0e2_prof);

    }

    // line 86
    public function block_messages_error($context, array $blocks = array())
    {
        $__internal_90f2f545ebd1a533654c2d6cb8471cf0852507eb87b55938484d0c51b8c52ee9 = $this->env->getExtension("native_profiler");
        $__internal_90f2f545ebd1a533654c2d6cb8471cf0852507eb87b55938484d0c51b8c52ee9->enter($__internal_90f2f545ebd1a533654c2d6cb8471cf0852507eb87b55938484d0c51b8c52ee9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "messages_error"));

        // line 87
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 88
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 89
                echo "                                        <div class=\"alert
                                        ";
                // line 90
                if (($context["type"] == "fos_user_success")) {
                    echo " alert-success ";
                } else {
                    echo " alert-danger ";
                }
                // line 91
                echo "                                        \">
                                            ";
                // line 92
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 95
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "                            ";
        
        $__internal_90f2f545ebd1a533654c2d6cb8471cf0852507eb87b55938484d0c51b8c52ee9->leave($__internal_90f2f545ebd1a533654c2d6cb8471cf0852507eb87b55938484d0c51b8c52ee9_prof);

    }

    // line 99
    public function block_fos_user_panel($context, array $blocks = array())
    {
        $__internal_5a8bd17b1df327a8f14e6005b329844994578ae6824e9a61af0029f34b6592ed = $this->env->getExtension("native_profiler");
        $__internal_5a8bd17b1df327a8f14e6005b329844994578ae6824e9a61af0029f34b6592ed->enter($__internal_5a8bd17b1df327a8f14e6005b329844994578ae6824e9a61af0029f34b6592ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_panel"));

        // line 100
        echo "                                    <div class=\"panel panel-default\">
                                        ";
        // line 101
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 102
        echo "                                    </div>
                                ";
        
        $__internal_5a8bd17b1df327a8f14e6005b329844994578ae6824e9a61af0029f34b6592ed->leave($__internal_5a8bd17b1df327a8f14e6005b329844994578ae6824e9a61af0029f34b6592ed_prof);

    }

    // line 101
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b25a8940716fb96afe803dc76c13570b83f3ec22d438498532ded3ba5f56e25e = $this->env->getExtension("native_profiler");
        $__internal_b25a8940716fb96afe803dc76c13570b83f3ec22d438498532ded3ba5f56e25e->enter($__internal_b25a8940716fb96afe803dc76c13570b83f3ec22d438498532ded3ba5f56e25e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_b25a8940716fb96afe803dc76c13570b83f3ec22d438498532ded3ba5f56e25e->leave($__internal_b25a8940716fb96afe803dc76c13570b83f3ec22d438498532ded3ba5f56e25e_prof);

    }

    // line 111
    public function block_footer($context, array $blocks = array())
    {
        $__internal_65cca6e9b01722fe059208f38a9c24eefd9e0dbc4bebef5cd76312cdf1bc7001 = $this->env->getExtension("native_profiler");
        $__internal_65cca6e9b01722fe059208f38a9c24eefd9e0dbc4bebef5cd76312cdf1bc7001->enter($__internal_65cca6e9b01722fe059208f38a9c24eefd9e0dbc4bebef5cd76312cdf1bc7001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 112
        echo "            <footer>
            <div id=\"page-wrapper\">
                <div class=\"content\" style=\"background-color: transparent;\">
                    <div class=\"footer\">
                        <p style=\"float: right;\">Desarrollo <a href=\"mailto:sistemas@cpcechaco.org.ar\">Gonzalo Alonso</a> &copy; Sistemas CPCE</p>
                    </div>
                </div>
            </div>
            </footer>
        ";
        
        $__internal_65cca6e9b01722fe059208f38a9c24eefd9e0dbc4bebef5cd76312cdf1bc7001->leave($__internal_65cca6e9b01722fe059208f38a9c24eefd9e0dbc4bebef5cd76312cdf1bc7001_prof);

    }

    // line 126
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_6d264d0ac16bc56ebdea2e3a9ea4452250eae680629a99400c633ea9525ee730 = $this->env->getExtension("native_profiler");
        $__internal_6d264d0ac16bc56ebdea2e3a9ea4452250eae680629a99400c633ea9525ee730->enter($__internal_6d264d0ac16bc56ebdea2e3a9ea4452250eae680629a99400c633ea9525ee730_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        
        $__internal_6d264d0ac16bc56ebdea2e3a9ea4452250eae680629a99400c633ea9525ee730->leave($__internal_6d264d0ac16bc56ebdea2e3a9ea4452250eae680629a99400c633ea9525ee730_prof);

    }

    public function getTemplateName()
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  415 => 126,  399 => 112,  393 => 111,  382 => 101,  374 => 102,  372 => 101,  369 => 100,  363 => 99,  356 => 96,  350 => 95,  341 => 92,  338 => 91,  332 => 90,  329 => 89,  324 => 88,  319 => 87,  313 => 86,  301 => 104,  299 => 99,  295 => 97,  293 => 86,  287 => 82,  281 => 81,  274 => 77,  268 => 74,  264 => 73,  257 => 69,  252 => 67,  247 => 64,  244 => 63,  238 => 62,  229 => 78,  227 => 62,  222 => 60,  211 => 51,  205 => 50,  198 => 110,  195 => 81,  192 => 50,  186 => 49,  175 => 31,  163 => 5,  155 => 127,  153 => 126,  149 => 125,  145 => 124,  141 => 123,  136 => 122,  133 => 111,  131 => 49,  112 => 33,  109 => 32,  107 => 31,  103 => 30,  99 => 29,  94 => 28,  86 => 22,  82 => 21,  77 => 19,  73 => 18,  68 => 16,  64 => 15,  60 => 14,  56 => 13,  52 => 12,  48 => 11,  39 => 5,  33 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <title>{% block title %}{{ 'views.layout.bannersadmin'|trans({}, 'MWSimpleAdminCrudBundle') }}{% endblock %}</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <meta name="description" content="">*/
/*         <meta name="author" content="">*/
/* */
/*         <!-- Le styles -->*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrap.min.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrap-checkbox.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" rel="stylesheet" id="bootstrap-style">*/
/*         <link rel="stylesheet" href="{{ asset('bundles/mwsimpleadmincrud/select2/select2.css') }}">*/
/*         <link rel="stylesheet" href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrapValidator.min.css') }}">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/datetime/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">*/
/*         */
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/crud.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/sistemacpce/css/crud.css') }}" rel="stylesheet">*/
/* */
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/jquery.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap.min.js') }}"></script>*/
/*         <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->*/
/*         <!--[if lt IE 9]>*/
/*         <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/*         <![endif]-->*/
/*         {# JS #}*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/datetime/js/bootstrap-datetimepicker.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/datetime/js/locales/bootstrap-datetimepicker.es.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/select2/select2.min.js') }}"></script>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <!-- Le fav and touch icons -->*/
/*         <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">*/
/* */
/*         <!-- Smartsupp Live Chat script -->*/
/*         <script type="text/javascript">*/
/*         var _smartsupp = _smartsupp || {};*/
/*         _smartsupp.key = '7d2ca3db7d98ba7a900e432e90bea4e57af41001';*/
/*         window.smartsupp||(function(d) {*/
/*             var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];*/
/*             s=d.getElementsByTagName('script')[0];c=d.createElement('script');*/
/*             c.type='text/javascript';c.charset='utf-8';c.async=true;*/
/*             c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);*/
/*         })(document);*/
/*         </script>*/
/*     </head>*/
/* */
/*     <body class="focusedform">*/
/*         {% block body %}*/
/*             {% block user_lateral %}*/
/*             <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*                 <div class="container-fluid">*/
/*                     <div class="navbar-header">*/
/*                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">*/
/*                             <span class="sr-only">Toggle navigation</span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                         </button>*/
/*                         <a class="navbar-brand" href="#"><img src="{{ asset('bundles/sistemacpce/images/header_logo.png') }}" alt="" /></a>*/
/*                     </div>*/
/*                     {% block menu %}*/
/*                         {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                             <div class="sidebar" role="navigation">*/
/*                                 <div class="sidebar-nav navbar-collapse collapse">*/
/*                                     <ul class="nav">*/
/*                                         <li><a class="bg-info">{{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}</a></li>*/
/*                                     </ul>*/
/*                                     {{ knp_menu_render('SistemaCPCEBundle:Builder:AfiliadoMenu') }}*/
/*                                 </div>*/
/*                             </div>*/
/*                             <script>*/
/*                                 smartsupp('email', '{{ app.user.email }}');*/
/*                                 smartsupp('name', '{{ app.user.username }}');*/
/*                             </script>*/
/*                         {% endif %}*/
/*                     {% endblock %}*/
/*                 </div>*/
/*             </nav>*/
/*             {% endblock user_lateral %}*/
/*             {% block wrapper %}*/
/*             <div id="page-wrapper">*/
/*                 <div class="content">*/
/*                     <div class="row">*/
/*                         <div class="col-md-12 col-sm-12">*/
/*                             {% block messages_error %}*/
/*                                 {% for type, messages in app.session.flashbag.all() %}*/
/*                                     {% for message in messages %}*/
/*                                         <div class="alert*/
/*                                         {% if type == "fos_user_success" %} alert-success {% else %} alert-danger {% endif %}*/
/*                                         ">*/
/*                                             {{ message|trans({}, 'FOSUserBundle') }}*/
/*                                         </div>*/
/*                                     {% endfor %}*/
/*                                 {% endfor %}*/
/*                             {% endblock messages_error %}*/
/* */
/*                             <div id="loginbox" class="mainbox">*/
/*                                 {% block fos_user_panel %}*/
/*                                     <div class="panel panel-default">*/
/*                                         {% block fos_user_content %}{% endblock fos_user_content %}*/
/*                                     </div>*/
/*                                 {% endblock fos_user_panel %}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             {% endblock wrapper %}*/
/*         {% endblock body %}*/
/*         {% block footer %}*/
/*             <footer>*/
/*             <div id="page-wrapper">*/
/*                 <div class="content" style="background-color: transparent;">*/
/*                     <div class="footer">*/
/*                         <p style="float: right;">Desarrollo <a href="mailto:sistemas@cpcechaco.org.ar">Gonzalo Alonso</a> &copy; Sistemas CPCE</p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             </footer>*/
/*         {% endblock footer %}*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-checkbox.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/validator/language/es_ES.js') }}"></script>*/
/*        {% block javascript %}{% endblock %}*/
/*     </body>*/
/* </html>*/
