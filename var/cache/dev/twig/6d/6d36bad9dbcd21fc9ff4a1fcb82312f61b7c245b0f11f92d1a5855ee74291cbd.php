<?php

/* SistemaCPCEBundle:Dato:datosPdf.html.twig */
class __TwigTemplate_fd4d82d5719d315d3648f8e5d7a5ac92f33a99f064aeac917395ad54c69528bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14fecf3cac2ad418009c2af1a29386158fc65d705ddd364341eab227b456425f = $this->env->getExtension("native_profiler");
        $__internal_14fecf3cac2ad418009c2af1a29386158fc65d705ddd364341eab227b456425f->enter($__internal_14fecf3cac2ad418009c2af1a29386158fc65d705ddd364341eab227b456425f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaCPCEBundle:Dato:datosPdf.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
      <meta charset=\"UTF-8\">
      <style type=\"text/css\">
        p {
          height: 0;
        }
        th p {
          float: left;
        }
        td {
          vertical-align: bottom;
        }
        .content {
          background-color: #fff;
          margin: 0 1em;
          padding-left: 1em;
        }
        .right {
          float: right;
        }
        .centrar {
          text-align: center;
          float: none !important;
        }
        .text-right {
          text-align: right;
        }
        .text-long {
          font-size: 20px;
          font-weight: bold;
        }
      </style>
    </head>
    <body class=\"focusedform\">
        <div class=\"content\">
            <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sistemacpce/images/header_logo_agua.png", null, true), "html", null, true);
        echo "\" alt=\"\" />
            <div class=\"right\">";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["fecha"]) ? $context["fecha"] : $this->getContext($context, "fecha")), "html", null, true);
        echo "</div>
            <div class=\"centrar\">
                <div>FORMULARIO DE USO INTERNO CPCE CHACO</div>
                <small>NO VALIDO COMO FACTURA / RECIBO / NO FISCAL</small>
              <hr>
                <div style=\"width: 12em; margin: 0 auto;\">
                    ";
        // line 46
        echo "                    </div>
                <strong>";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
        echo "</strong>
              <hr>
            </div>
        </div>    
    </body>
</html>";
        
        $__internal_14fecf3cac2ad418009c2af1a29386158fc65d705ddd364341eab227b456425f->leave($__internal_14fecf3cac2ad418009c2af1a29386158fc65d705ddd364341eab227b456425f_prof);

    }

    public function getTemplateName()
    {
        return "SistemaCPCEBundle:Dato:datosPdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 47,  74 => 46,  65 => 39,  61 => 38,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*       <meta charset="UTF-8">*/
/*       <style type="text/css">*/
/*         p {*/
/*           height: 0;*/
/*         }*/
/*         th p {*/
/*           float: left;*/
/*         }*/
/*         td {*/
/*           vertical-align: bottom;*/
/*         }*/
/*         .content {*/
/*           background-color: #fff;*/
/*           margin: 0 1em;*/
/*           padding-left: 1em;*/
/*         }*/
/*         .right {*/
/*           float: right;*/
/*         }*/
/*         .centrar {*/
/*           text-align: center;*/
/*           float: none !important;*/
/*         }*/
/*         .text-right {*/
/*           text-align: right;*/
/*         }*/
/*         .text-long {*/
/*           font-size: 20px;*/
/*           font-weight: bold;*/
/*         }*/
/*       </style>*/
/*     </head>*/
/*     <body class="focusedform">*/
/*         <div class="content">*/
/*             <img src="{{ asset('bundles/sistemacpce/images/header_logo_agua.png', absolute=true) }}" alt="" />*/
/*             <div class="right">{{ fecha }}</div>*/
/*             <div class="centrar">*/
/*                 <div>FORMULARIO DE USO INTERNO CPCE CHACO</div>*/
/*                 <small>NO VALIDO COMO FACTURA / RECIBO / NO FISCAL</small>*/
/*               <hr>*/
/*                 <div style="width: 12em; margin: 0 auto;">*/
/*                     {#{ barcode({code: '' ~ entity.afiNroDoc, type: 'qrcode', format: 'html'}) }#}*/
/*                     </div>*/
/*                 <strong>{{ entity.id }}</strong>*/
/*               <hr>*/
/*             </div>*/
/*         </div>    */
/*     </body>*/
/* </html>*/
