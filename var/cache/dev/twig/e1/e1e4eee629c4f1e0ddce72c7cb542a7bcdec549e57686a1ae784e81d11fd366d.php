<?php

/* MWSimpleAdminCrudBundle::layout.html.twig */
class __TwigTemplate_5fd1a5514d309cd7006284804b89bc98e30b1efd5927d0f1980b67bf16d9714e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'menu' => array($this, 'block_menu'),
            'page' => array($this, 'block_page'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6eb48ccbf989ae9ec4c647bd3df4a922c58dbcd9e8cb3007846fc11d177e9bad = $this->env->getExtension("native_profiler");
        $__internal_6eb48ccbf989ae9ec4c647bd3df4a922c58dbcd9e8cb3007846fc11d177e9bad->enter($__internal_6eb48ccbf989ae9ec4c647bd3df4a922c58dbcd9e8cb3007846fc11d177e9bad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <!-- Le styles -->
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrap-checkbox.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"bootstrap-style\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/select2/select2.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/bootstrapValidator.min.css"), "html", null, true);
        echo "\">
        <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/css/bootstrap-datetimepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/css/crud.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
        <![endif]-->
        <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/js/bootstrap-datetimepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/datetime/js/locales/bootstrap-datetimepicker.es.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/select2/select2.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 27
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 28
        echo "        <!-- Le fav and touch icons -->
        <link rel=\"shortcut icon\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/img/favicon.ico"), "html", null, true);
        echo "\">
    </head>

    <body>
        <div id=\"wrapper\">
            <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\".navbar-collapse\" aria-expanded=\"false\" aria-controls=\"navbar\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"#\">MENU</a>
                    </div>
                    <div class=\"sidebar\" role=\"navigation\">
                        <div class=\"sidebar-nav navbar-collapse collapse\">
                            ";
        // line 47
        $this->displayBlock('menu', $context, $blocks);
        // line 50
        echo "                        </div>
                    </div>
                </div>
            </nav>
            <div id=\"page-wrapper\">
                <div class=\"row\">
                    <div class=\"col-md-12 col-sm-12\">
                        ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["flashMessages"]) {
            // line 58
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["flashMessages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
                // line 59
                echo "                                <div class=\"alert alert-";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                    ";
                // line 60
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["flashMessage"], array(), "MWSimpleAdminCrudBundle"), "html", null, true);
                echo "
                                </div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashMessages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "
                        ";
        // line 65
        $this->displayBlock('page', $context, $blocks);
        // line 66
        echo "                    </div>
                </div>
            </div>
        </div>
        <script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-checkbox.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/mwsimpleadmincrud/js/validator/language/es_ES.js"), "html", null, true);
        echo "\"></script>
       ";
        // line 74
        $this->displayBlock('javascript', $context, $blocks);
        // line 75
        echo "    </body>
</html>";
        
        $__internal_6eb48ccbf989ae9ec4c647bd3df4a922c58dbcd9e8cb3007846fc11d177e9bad->leave($__internal_6eb48ccbf989ae9ec4c647bd3df4a922c58dbcd9e8cb3007846fc11d177e9bad_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_612e8f4ee5ad49e5247dbfa4a6592f0459f420544b8ba8807691f139ce813f54 = $this->env->getExtension("native_profiler");
        $__internal_612e8f4ee5ad49e5247dbfa4a6592f0459f420544b8ba8807691f139ce813f54->enter($__internal_612e8f4ee5ad49e5247dbfa4a6592f0459f420544b8ba8807691f139ce813f54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.layout.bannersadmin", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        
        $__internal_612e8f4ee5ad49e5247dbfa4a6592f0459f420544b8ba8807691f139ce813f54->leave($__internal_612e8f4ee5ad49e5247dbfa4a6592f0459f420544b8ba8807691f139ce813f54_prof);

    }

    // line 27
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3e456cf12eedc923625dab1943cd95633f3f4ca7f903d0f13ffcde808ae74348 = $this->env->getExtension("native_profiler");
        $__internal_3e456cf12eedc923625dab1943cd95633f3f4ca7f903d0f13ffcde808ae74348->enter($__internal_3e456cf12eedc923625dab1943cd95633f3f4ca7f903d0f13ffcde808ae74348_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3e456cf12eedc923625dab1943cd95633f3f4ca7f903d0f13ffcde808ae74348->leave($__internal_3e456cf12eedc923625dab1943cd95633f3f4ca7f903d0f13ffcde808ae74348_prof);

    }

    // line 47
    public function block_menu($context, array $blocks = array())
    {
        $__internal_b4b5640e2a5edb043b5dc65a7a78e9592da3c5a0fda22be63837c92340fde434 = $this->env->getExtension("native_profiler");
        $__internal_b4b5640e2a5edb043b5dc65a7a78e9592da3c5a0fda22be63837c92340fde434->enter($__internal_b4b5640e2a5edb043b5dc65a7a78e9592da3c5a0fda22be63837c92340fde434_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 48
        echo "                                ";
        echo $this->env->getExtension('knp_menu')->render("MWSimpleAdminCrudBundle:Builder:adminMenu");
        echo "
                            ";
        
        $__internal_b4b5640e2a5edb043b5dc65a7a78e9592da3c5a0fda22be63837c92340fde434->leave($__internal_b4b5640e2a5edb043b5dc65a7a78e9592da3c5a0fda22be63837c92340fde434_prof);

    }

    // line 65
    public function block_page($context, array $blocks = array())
    {
        $__internal_e6b46d4da57d0d09ee11169bc71079f1786eadeac74e63f1af38cacb6d9feacf = $this->env->getExtension("native_profiler");
        $__internal_e6b46d4da57d0d09ee11169bc71079f1786eadeac74e63f1af38cacb6d9feacf->enter($__internal_e6b46d4da57d0d09ee11169bc71079f1786eadeac74e63f1af38cacb6d9feacf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page"));

        
        $__internal_e6b46d4da57d0d09ee11169bc71079f1786eadeac74e63f1af38cacb6d9feacf->leave($__internal_e6b46d4da57d0d09ee11169bc71079f1786eadeac74e63f1af38cacb6d9feacf_prof);

    }

    // line 74
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_4a10bb653294e083528319e7c97a029f76d204a71adc8e1f7f32ddcdd1fe2194 = $this->env->getExtension("native_profiler");
        $__internal_4a10bb653294e083528319e7c97a029f76d204a71adc8e1f7f32ddcdd1fe2194->enter($__internal_4a10bb653294e083528319e7c97a029f76d204a71adc8e1f7f32ddcdd1fe2194_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        
        $__internal_4a10bb653294e083528319e7c97a029f76d204a71adc8e1f7f32ddcdd1fe2194->leave($__internal_4a10bb653294e083528319e7c97a029f76d204a71adc8e1f7f32ddcdd1fe2194_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 74,  236 => 65,  226 => 48,  220 => 47,  209 => 27,  197 => 5,  189 => 75,  187 => 74,  183 => 73,  179 => 72,  175 => 71,  171 => 70,  165 => 66,  163 => 65,  160 => 64,  154 => 63,  145 => 60,  140 => 59,  135 => 58,  131 => 57,  122 => 50,  120 => 47,  99 => 29,  96 => 28,  94 => 27,  90 => 26,  86 => 25,  82 => 24,  74 => 19,  70 => 18,  66 => 17,  62 => 16,  58 => 15,  54 => 14,  50 => 13,  46 => 12,  42 => 11,  33 => 5,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <title>{% block title %}{{ 'views.layout.bannersadmin'|trans({}, 'MWSimpleAdminCrudBundle') }}{% endblock %}</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <meta name="description" content="">*/
/*         <meta name="author" content="">*/
/* */
/*         <!-- Le styles -->*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrap.min.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrap-checkbox.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" rel="stylesheet" id="bootstrap-style">*/
/*         <link rel="stylesheet" href="{{ asset('bundles/mwsimpleadmincrud/select2/select2.css') }}">*/
/*         <link rel="stylesheet" href="{{ asset('bundles/mwsimpleadmincrud/css/bootstrapValidator.min.css') }}">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/datetime/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/mwsimpleadmincrud/css/crud.css') }}" rel="stylesheet">*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/jquery.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap.min.js') }}"></script>*/
/*         <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->*/
/*         <!--[if lt IE 9]>*/
/*         <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/*         <![endif]-->*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/datetime/js/bootstrap-datetimepicker.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/datetime/js/locales/bootstrap-datetimepicker.es.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/select2/select2.min.js') }}"></script>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <!-- Le fav and touch icons -->*/
/*         <link rel="shortcut icon" href="{{ asset('bundles/mwsimpleadmincrud/img/favicon.ico') }}">*/
/*     </head>*/
/* */
/*     <body>*/
/*         <div id="wrapper">*/
/*             <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*                 <div class="container-fluid">*/
/*                     <div class="navbar-header">*/
/*                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">*/
/*                             <span class="sr-only">Toggle navigation</span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                         </button>*/
/*                         <a class="navbar-brand" href="#">MENU</a>*/
/*                     </div>*/
/*                     <div class="sidebar" role="navigation">*/
/*                         <div class="sidebar-nav navbar-collapse collapse">*/
/*                             {% block menu %}*/
/*                                 {{ knp_menu_render('MWSimpleAdminCrudBundle:Builder:adminMenu') }}*/
/*                             {% endblock %}*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </nav>*/
/*             <div id="page-wrapper">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12 col-sm-12">*/
/*                         {% for type, flashMessages in app.session.flashbag.all() %}*/
/*                             {% for flashMessage in flashMessages %}*/
/*                                 <div class="alert alert-{{ type }}">*/
/*                                     {{ flashMessage|trans({}, 'MWSimpleAdminCrudBundle') }}*/
/*                                 </div>*/
/*                             {% endfor %}*/
/*                         {% endfor %}*/
/* */
/*                         {% block page %}{% endblock %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-checkbox.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/validator/bootstrapValidator.min.js') }}"></script>*/
/*         <script src="{{ asset('bundles/mwsimpleadmincrud/js/validator/language/es_ES.js') }}"></script>*/
/*        {% block javascript %}{% endblock %}*/
/*     </body>*/
/* </html>*/
