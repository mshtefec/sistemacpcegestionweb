<?php

/* MWSimpleAdminCrudBundle:BackEnd:index.html.twig */
class __TwigTemplate_576f635c8087887fcb85e40c4e6311eafb6e6fc5c4996829d9a1811a39b96b6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MWSimpleAdminCrudBundle::layout.html.twig", "MWSimpleAdminCrudBundle:BackEnd:index.html.twig", 1);
        $this->blocks = array(
            'page' => array($this, 'block_page'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MWSimpleAdminCrudBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a12c73075aefeecb126caa556d4455d7d29106793ac0665abd809e1b22d908c = $this->env->getExtension("native_profiler");
        $__internal_9a12c73075aefeecb126caa556d4455d7d29106793ac0665abd809e1b22d908c->enter($__internal_9a12c73075aefeecb126caa556d4455d7d29106793ac0665abd809e1b22d908c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle:BackEnd:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9a12c73075aefeecb126caa556d4455d7d29106793ac0665abd809e1b22d908c->leave($__internal_9a12c73075aefeecb126caa556d4455d7d29106793ac0665abd809e1b22d908c_prof);

    }

    // line 3
    public function block_page($context, array $blocks = array())
    {
        $__internal_e84424b14f09a861049bcaac0da489ab5546f7f2386d0b80ff79f4861742e87e = $this->env->getExtension("native_profiler");
        $__internal_e84424b14f09a861049bcaac0da489ab5546f7f2386d0b80ff79f4861742e87e->enter($__internal_e84424b14f09a861049bcaac0da489ab5546f7f2386d0b80ff79f4861742e87e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page"));

        // line 4
        echo "\t<h1>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("views.layout.bannersadmin", array(), "MWSimpleAdminCrudBundle"), "html", null, true);
        echo "</h1>
";
        
        $__internal_e84424b14f09a861049bcaac0da489ab5546f7f2386d0b80ff79f4861742e87e->leave($__internal_e84424b14f09a861049bcaac0da489ab5546f7f2386d0b80ff79f4861742e87e_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle:BackEnd:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'MWSimpleAdminCrudBundle::layout.html.twig' %}*/
/* */
/* {% block page %}*/
/* 	<h1>{{ 'views.layout.bannersadmin'|trans({}, 'MWSimpleAdminCrudBundle') }}</h1>*/
/* {% endblock page %}*/
