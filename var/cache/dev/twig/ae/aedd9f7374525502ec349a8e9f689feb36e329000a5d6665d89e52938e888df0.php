<?php

/* SistemaUserBundle:Security:login.html.twig */
class __TwigTemplate_1d5195ecc294f7ca1d2f624e2407effd519783195bfaf1f257cdb9220999944b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SistemaUserBundle::layout.html.twig", "SistemaUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_panel' => array($this, 'block_fos_user_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SistemaUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b99e94d7c75f6312448ba8ec2b8946a90e018192a7bb24d3e233f5f5f690a0d = $this->env->getExtension("native_profiler");
        $__internal_7b99e94d7c75f6312448ba8ec2b8946a90e018192a7bb24d3e233f5f5f690a0d->enter($__internal_7b99e94d7c75f6312448ba8ec2b8946a90e018192a7bb24d3e233f5f5f690a0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SistemaUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7b99e94d7c75f6312448ba8ec2b8946a90e018192a7bb24d3e233f5f5f690a0d->leave($__internal_7b99e94d7c75f6312448ba8ec2b8946a90e018192a7bb24d3e233f5f5f690a0d_prof);

    }

    // line 5
    public function block_fos_user_panel($context, array $blocks = array())
    {
        $__internal_0ba6a1236cf067119010f5a5e4ac7932644827098e2624952181c67441de04a7 = $this->env->getExtension("native_profiler");
        $__internal_0ba6a1236cf067119010f5a5e4ac7932644827098e2624952181c67441de04a7->enter($__internal_0ba6a1236cf067119010f5a5e4ac7932644827098e2624952181c67441de04a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_panel"));

        // line 6
        echo "<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <div class=\"panel-title\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
        echo " | <a href=\"";
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.register", array(), "FOSUserBundle"), "html", null, true);
        echo "me</a></div>
    </div>     
    <div style=\"padding-top:30px\" class=\"panel-body\" >
        ";
        // line 11
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 12
            echo "            <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), array(), "FOSUserBundle"), "html", null, true);
            echo "</div>
        ";
        }
        // line 14
        echo "
        <form action=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" id=\"loginform\" role=\"form\" method=\"post\" class=\"form-horizontal\" style=\"margin-bottom: 0px !important;\">
            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />

            <div style=\"margin-bottom: 25px\" class=\"input-group\">
                <span for=\"username\" class=\"input-group-addon\"><i class=\"glyphicon glyphicon-user\"></i></span>
                <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" class=\"form-control\" placeholder=\"Correo o ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
            </div>

            <div style=\"margin-bottom: 25px\" class=\"input-group\">
                <span for=\"password\" class=\"input-group-addon\"><i class=\"glyphicon glyphicon-lock\"></i></span>
                <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" placeholder=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
            </div>
            ";
        // line 37
        echo "            <div style=\"margin-top:10px\" class=\"form-group controls\">
                <!-- Button -->
                <div class=\"col-sm-6\">
                    <input class=\"btn btn-primary\" type=\"submit\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
        echo "\">
                </div>
                <div class=\"col-sm-6\">
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "\" class=\"btn btn-warning\">Restablecer contraseña</a>
                </div>
                <div class=\"col-sm-6\">
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\" class=\"btn btn-danger\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.register", array(), "FOSUserBundle"), "html", null, true);
        echo "me</a>
                </div>
            </div>
            <div style=\"margin-top:30px\">
                <h3><div class=\"alert alert-info\" role=\"alert\">* Se recomienda utilizar <a href=\"https://www.google.com/chrome\">Google Chrome</a> o <a href=\"https://www.mozilla.org/es-AR/firefox\">Mozilla Firefox</a>.</div></h3>
            </div>
        </form>
    </div>
</div>
";
        
        $__internal_0ba6a1236cf067119010f5a5e4ac7932644827098e2624952181c67441de04a7->leave($__internal_0ba6a1236cf067119010f5a5e4ac7932644827098e2624952181c67441de04a7_prof);

    }

    public function getTemplateName()
    {
        return "SistemaUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 46,  102 => 43,  96 => 40,  91 => 37,  86 => 25,  76 => 20,  69 => 16,  65 => 15,  62 => 14,  56 => 12,  54 => 11,  44 => 8,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "SistemaUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_panel %}*/
/* <div class="panel panel-primary">*/
/*     <div class="panel-heading">*/
/*         <div class="panel-title">{{ 'layout.login'|trans({}, 'FOSUserBundle') }} | <a href="{{ path('fos_user_registration_register') }}">{{ 'layout.register'|trans }}me</a></div>*/
/*     </div>     */
/*     <div style="padding-top:30px" class="panel-body" >*/
/*         {% if error %}*/
/*             <div class="alert alert-danger">{{ error|trans({}, 'FOSUserBundle') }}</div>*/
/*         {% endif %}*/
/* */
/*         <form action="{{ path("fos_user_security_check") }}" id="loginform" role="form" method="post" class="form-horizontal" style="margin-bottom: 0px !important;">*/
/*             <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/* */
/*             <div style="margin-bottom: 25px" class="input-group">*/
/*                 <span for="username" class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>*/
/*                 <input type="text" id="username" name="_username" value="{{ last_username }}" required="required" class="form-control" placeholder="Correo o {{ 'security.login.username'|trans }}" />*/
/*             </div>*/
/* */
/*             <div style="margin-bottom: 25px" class="input-group">*/
/*                 <span for="password" class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>*/
/*                 <input type="password" id="password" name="_password" required="required" class="form-control" placeholder="{{ 'security.login.password'|trans }}" />*/
/*             </div>*/
/*             {#*/
/*             <div class="input-group">*/
/*                 <div class="checkbox">*/
/*                     <label for="remember_me">*/
/*                         <input type="checkbox" id="remember_me" name="_remember_me" value="on" style="margin-bottom: 20px">*/
/*                         {{ 'security.login.remember_me'|trans }}*/
/*                     </label>*/
/*                 </div>*/
/*             </div>*/
/*             #}*/
/*             <div style="margin-top:10px" class="form-group controls">*/
/*                 <!-- Button -->*/
/*                 <div class="col-sm-6">*/
/*                     <input class="btn btn-primary" type="submit" value="{{ 'layout.login'|trans({}, 'FOSUserBundle') }}">*/
/*                 </div>*/
/*                 <div class="col-sm-6">*/
/*                     <a href="{{ path("fos_user_resetting_request") }}" class="btn btn-warning">Restablecer contraseña</a>*/
/*                 </div>*/
/*                 <div class="col-sm-6">*/
/*                     <a href="{{ path('fos_user_registration_register') }}" class="btn btn-danger">{{ 'layout.register'|trans }}me</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="margin-top:30px">*/
/*                 <h3><div class="alert alert-info" role="alert">* Se recomienda utilizar <a href="https://www.google.com/chrome">Google Chrome</a> o <a href="https://www.mozilla.org/es-AR/firefox">Mozilla Firefox</a>.</div></h3>*/
/*             </div>*/
/*         </form>*/
/*     </div>*/
/* </div>*/
/* {% endblock fos_user_panel %}*/
