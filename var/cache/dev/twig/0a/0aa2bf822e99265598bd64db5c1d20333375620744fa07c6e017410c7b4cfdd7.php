<?php

/* knp_menu_base.html.twig */
class __TwigTemplate_18d76d8da9b708da012f190d6c44851a81941561bc5cc887e3b8d395afaa3620 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_644a028dcd200ecd4a3f84f1afe1292c7885c154ba851a80358dd0abef7ab889 = $this->env->getExtension("native_profiler");
        $__internal_644a028dcd200ecd4a3f84f1afe1292c7885c154ba851a80358dd0abef7ab889->enter($__internal_644a028dcd200ecd4a3f84f1afe1292c7885c154ba851a80358dd0abef7ab889_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu_base.html.twig"));

        // line 1
        if ($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "compressed", array())) {
            $this->displayBlock("compressed_root", $context, $blocks);
        } else {
            $this->displayBlock("root", $context, $blocks);
        }
        
        $__internal_644a028dcd200ecd4a3f84f1afe1292c7885c154ba851a80358dd0abef7ab889->leave($__internal_644a028dcd200ecd4a3f84f1afe1292c7885c154ba851a80358dd0abef7ab889_prof);

    }

    public function getTemplateName()
    {
        return "knp_menu_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% if options.compressed %}{{ block('compressed_root') }}{% else %}{{ block('root') }}{% endif %}*/
/* */
