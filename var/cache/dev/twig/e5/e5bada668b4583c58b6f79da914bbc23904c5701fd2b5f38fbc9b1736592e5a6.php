<?php

/* MWSimpleAdminCrudBundle:Form:bootstrap.datepicker.html.twig */
class __TwigTemplate_5792962831649dad6c241667938702d1475790d21de03f2639a71da834b4e9a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'bootstrapdatetime_widget' => array($this, 'block_bootstrapdatetime_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aef796ec81c3308df257f9086d803d5f65198abdbc5a7da14274e5b4cf927707 = $this->env->getExtension("native_profiler");
        $__internal_aef796ec81c3308df257f9086d803d5f65198abdbc5a7da14274e5b4cf927707->enter($__internal_aef796ec81c3308df257f9086d803d5f65198abdbc5a7da14274e5b4cf927707_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MWSimpleAdminCrudBundle:Form:bootstrap.datepicker.html.twig"));

        // line 1
        $this->displayBlock('bootstrapdatetime_widget', $context, $blocks);
        
        $__internal_aef796ec81c3308df257f9086d803d5f65198abdbc5a7da14274e5b4cf927707->leave($__internal_aef796ec81c3308df257f9086d803d5f65198abdbc5a7da14274e5b4cf927707_prof);

    }

    public function block_bootstrapdatetime_widget($context, array $blocks = array())
    {
        $__internal_bfcfa9f6c9410d7f95d88f332d1f6683a48b878481c58fef546373a3009ffc38 = $this->env->getExtension("native_profiler");
        $__internal_bfcfa9f6c9410d7f95d88f332d1f6683a48b878481c58fef546373a3009ffc38->enter($__internal_bfcfa9f6c9410d7f95d88f332d1f6683a48b878481c58fef546373a3009ffc38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bootstrapdatetime_widget"));

        // line 2
        echo "    <div class=\"input-group\">
        <div class=\"input-append date form_datetime input-group\" data-date=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtpicker\">
            ";
        // line 4
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        // line 5
        echo "            <input name=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_wd\" data-bv-field=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_wd\" ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " type=\"text\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "\"";
        if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
            echo " required=\"required\"";
        }
        echo " ";
        if ((isset($context["read_only"]) ? $context["read_only"] : $this->getContext($context, "read_only"))) {
            echo " readonly ";
        }
        echo " >
            <span class=\"input-group-addon\">
                <i class=\"glyphicon glyphicon-";
        // line 7
        if ((((isset($context["widget_type"]) ? $context["widget_type"] : $this->getContext($context, "widget_type")) == "time") || ((isset($context["widget_type"]) ? $context["widget_type"] : $this->getContext($context, "widget_type")) == "day"))) {
            echo "time";
        } else {
            echo "calendar";
        }
        echo "\"></i>
            </span>
                ";
        // line 13
        echo "        </div>
    </div>
    <input type=\"hidden\" ";
        // line 15
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        echo "/>

    <script type=\"text/javascript\">
        (function(\$) {
            var \$";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtp = \$(\"#";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtpicker\"),
                ";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtp_options = ";
        echo (isset($context["options"]) ? $context["options"] : $this->getContext($context, "options"));
        echo ";

            ";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtp_options['linkField'] = \"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\";
            //";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtp_options['linkFormat'] = \"yyyy-mm-dd hh:ii\";
            ";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtp_options['pickerPosition'] = \"bottom-left\";
            \$";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtp.datetimepicker(";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtp_options)
                .datetimepicker('show').on('changeDate', function(){
                    var \$option = \$(this).find('input').attr('name');
                    \$('form').bootstrapValidator('revalidateField', \$option);
                })
                .datetimepicker('hide');
                    //\$(\"#";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_dtpicker\").datetimepicker('show');
        })(jQuery);
    </script>
";
        
        $__internal_bfcfa9f6c9410d7f95d88f332d1f6683a48b878481c58fef546373a3009ffc38->leave($__internal_bfcfa9f6c9410d7f95d88f332d1f6683a48b878481c58fef546373a3009ffc38_prof);

    }

    public function getTemplateName()
    {
        return "MWSimpleAdminCrudBundle:Form:bootstrap.datepicker.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  140 => 31,  129 => 25,  125 => 24,  121 => 23,  115 => 22,  108 => 20,  102 => 19,  89 => 15,  85 => 13,  76 => 7,  46 => 5,  44 => 4,  38 => 3,  35 => 2,  23 => 1,);
    }
}
/* {% block bootstrapdatetime_widget %}*/
/*     <div class="input-group">*/
/*         <div class="input-append date form_datetime input-group" data-date="{{ value }}" id="{{ id }}_dtpicker">*/
/*             {% set attr = attr|merge({'class': (attr.class|default('') ~ ' form-control')|trim}) %}*/
/*             <input name="{{ id }}_wd" data-bv-field="{{ id }}_wd" {% for key, value in attr %}{{ key }}="{{ value }}"{% endfor %} type="text" value="{{ value }}"{% if required %} required="required"{% endif %} {% if read_only %} readonly {% endif %} >*/
/*             <span class="input-group-addon">*/
/*                 <i class="glyphicon glyphicon-{% if widget_type == 'time' or widget_type == 'day' %}time{% else %}calendar{% endif %}"></i>*/
/*             </span>*/
/*                 {#% if not required %}*/
/*                 <span class="add-on"><i class="icon-remove"></i></span>*/
/*                 {% endif %}*/
/*                 <span class="add-on"><i class="icon-th"></i></span>#}*/
/*         </div>*/
/*     </div>*/
/*     <input type="hidden" {{ block('widget_attributes') }} {% if value is not empty %}value="{{ value }}"{% endif %}/>*/
/* */
/*     <script type="text/javascript">*/
/*         (function($) {*/
/*             var ${{ id }}_dtp = $("#{{ id }}_dtpicker"),*/
/*                 {{ id }}_dtp_options = {{ options|raw }};*/
/* */
/*             {{ id }}_dtp_options['linkField'] = "{{ id }}";*/
/*             //{{ id }}_dtp_options['linkFormat'] = "yyyy-mm-dd hh:ii";*/
/*             {{ id }}_dtp_options['pickerPosition'] = "bottom-left";*/
/*             ${{ id }}_dtp.datetimepicker({{ id }}_dtp_options)*/
/*                 .datetimepicker('show').on('changeDate', function(){*/
/*                     var $option = $(this).find('input').attr('name');*/
/*                     $('form').bootstrapValidator('revalidateField', $option);*/
/*                 })*/
/*                 .datetimepicker('hide');*/
/*                     //$("#{{ id }}_dtpicker").datetimepicker('show');*/
/*         })(jQuery);*/
/*     </script>*/
/* {% endblock %}*/
