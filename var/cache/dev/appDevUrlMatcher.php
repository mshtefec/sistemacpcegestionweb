<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/mwsforo/foro')) {
            // mws_front_foro
            if (preg_match('#^/mwsforo/foro(?:/(?P<foro_id>[^/]++))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_mws_front_foro;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mws_front_foro')), array (  'foro_id' => NULL,  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\DefaultController::indexAction',));
            }
            not_mws_front_foro:

            // traer_foro
            if (0 === strpos($pathinfo, '/mwsforo/foro/id') && preg_match('#^/mwsforo/foro/id/(?P<foro_id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_traer_foro;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'traer_foro')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\DefaultController::foroAction',));
            }
            not_traer_foro:

            if (0 === strpos($pathinfo, '/mwsforo/foroentrada')) {
                // foro_entrada_create
                if (preg_match('#^/mwsforo/foroentrada/(?P<foro_id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_foro_entrada_create;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_entrada_create')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\EntradaController::createAction',));
                }
                not_foro_entrada_create:

                // borrar_entrada
                if (0 === strpos($pathinfo, '/mwsforo/foroentrada/id') && preg_match('#^/mwsforo/foroentrada/id/(?P<entrada_id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_borrar_entrada;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'borrar_entrada')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\EntradaController::deletedEntrada',));
                }
                not_borrar_entrada:

            }

            if (0 === strpos($pathinfo, '/mwsforo/foro/grupo')) {
                // admin_grupo_foro
                if (rtrim($pathinfo, '/') === '/mwsforo/foro/grupo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_grupo_foro;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_grupo_foro');
                    }

                    return array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::indexAction',  '_route' => 'admin_grupo_foro',);
                }
                not_admin_grupo_foro:

                // foro_grupo_create
                if ($pathinfo === '/mwsforo/foro/grupo/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_foro_grupo_create;
                    }

                    return array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::createAction',  '_route' => 'foro_grupo_create',);
                }
                not_foro_grupo_create:

                // foro_grupo_new
                if ($pathinfo === '/mwsforo/foro/grupo/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_foro_grupo_new;
                    }

                    return array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::newAction',  '_route' => 'foro_grupo_new',);
                }
                not_foro_grupo_new:

                // foro_grupo_show
                if (preg_match('#^/mwsforo/foro/grupo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_foro_grupo_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_grupo_show')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::showAction',));
                }
                not_foro_grupo_show:

                // foro_grupo_edit
                if (preg_match('#^/mwsforo/foro/grupo/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_foro_grupo_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_grupo_edit')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::editAction',));
                }
                not_foro_grupo_edit:

                // foro_grupo_update
                if (preg_match('#^/mwsforo/foro/grupo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_foro_grupo_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_grupo_update')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::updateAction',));
                }
                not_foro_grupo_update:

                // foro_grupo_delete
                if (preg_match('#^/mwsforo/foro/grupo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_foro_grupo_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_grupo_delete')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::deleteAction',));
                }
                not_foro_grupo_delete:

                // admin_grupo_foro_usersfos
                if ($pathinfo === '/mwsforo/foro/grupo/usersfos') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_grupo_foro_usersfos;
                    }

                    return array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::indexUsersFosAction',  '_route' => 'admin_grupo_foro_usersfos',);
                }
                not_admin_grupo_foro_usersfos:

                // foro_grupo_export
                if (0 === strpos($pathinfo, '/mwsforo/foro/grupo/exporter') && preg_match('#^/mwsforo/foro/grupo/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_grupo_export')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::getExporter',));
                }

                if (0 === strpos($pathinfo, '/mwsforo/foro/grupo/autocomplete-forms/get-')) {
                    // Grupo_autocomplete_miembros
                    if ($pathinfo === '/mwsforo/foro/grupo/autocomplete-forms/get-miembros') {
                        return array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::getAutocompleteUser',  '_route' => 'Grupo_autocomplete_miembros',);
                    }

                    // Grupo_autocomplete_entrada
                    if ($pathinfo === '/mwsforo/foro/grupo/autocomplete-forms/get-entrada') {
                        return array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::getAutocompleteEntrada',  '_route' => 'Grupo_autocomplete_entrada',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/mwsforo/fororespuesta')) {
                // foro_respuesta_create
                if (preg_match('#^/mwsforo/fororespuesta/(?P<entrada_id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_foro_respuesta_create;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_respuesta_create')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\RespuestaController::createAction',));
                }
                not_foro_respuesta_create:

                // borrar_respuesta
                if (0 === strpos($pathinfo, '/mwsforo/fororespuesta/id') && preg_match('#^/mwsforo/fororespuesta/id/(?P<respuesta_id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_borrar_respuesta;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'borrar_respuesta')), array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\RespuestaController::deletedRespuesta',));
                }
                not_borrar_respuesta:

            }

        }

        // admin_foro_grupo
        if ($pathinfo === '/admin/foro/grupo') {
            return array (  '_controller' => 'MWSimple\\Bundle\\ForoBundle\\Controller\\GrupoController::indexAction',  'template' => 'index.html.twig',  '_route' => 'admin_foro_grupo',);
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/usuario/formulario')) {
            // formulario_habilitar
            if ($pathinfo === '/usuario/formulario/habilitar') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_formulario_habilitar;
                }

                return array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::formularioHabilitarAction',  '_route' => 'formulario_habilitar',);
            }
            not_formulario_habilitar:

            // formulario_activacion
            if ($pathinfo === '/usuario/formulario/activacion') {
                return array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::formularioActivacionAction',  '_route' => 'formulario_activacion',);
            }

        }

        // formulario_instructivo
        if ($pathinfo === '/formulario/instructivo') {
            return array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::formularioInstructivoAction',  '_route' => 'formulario_instructivo',);
        }

        // ofertas_laborales
        if ($pathinfo === '/ofertas/laborales') {
            return array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::ofertasLaboralesAction',  '_route' => 'ofertas_laborales',);
        }

        if (0 === strpos($pathinfo, '/matriculado/oferta/laboral')) {
            // oferta_laboral_show
            if (preg_match('#^/matriculado/oferta/laboral/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'oferta_laboral_show')), array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::ofertaLaboralAction',));
            }

            // oferta_laboral_postular
            if (0 === strpos($pathinfo, '/matriculado/oferta/laboral/postular') && preg_match('#^/matriculado/oferta/laboral/postular/(?P<idOferta>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'oferta_laboral_postular')), array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::PostularAction',));
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_profile_show
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
            }

            return array (  '_controller' => 'Sistema\\FrontBundle\\Controller\\DefaultController::indexAction',  '_route' => 'fos_user_profile_show',);
        }

        // fos_js_routing_js
        if (0 === strpos($pathinfo, '/js/routing') && preg_match('#^/js/routing(?:\\.(?P<_format>js|json))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_js_routing_js')), array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',));
        }

        // mws_admin_crud_menu
        if (rtrim($pathinfo, '/') === '/admin') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_mws_admin_crud_menu;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'mws_admin_crud_menu');
            }

            return array (  '_controller' => 'MWSimple\\Bundle\\AdminCrudBundle\\Controller\\BackEndController::indexAction',  '_route' => 'mws_admin_crud_menu',);
        }
        not_mws_admin_crud_menu:

        if (0 === strpos($pathinfo, '/superadmin')) {
            if (0 === strpos($pathinfo, '/superadmin/role')) {
                // admin_role
                if (rtrim($pathinfo, '/') === '/superadmin/role') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_role;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_role');
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::indexAction',  '_route' => 'admin_role',);
                }
                not_admin_role:

                // admin_role_create
                if ($pathinfo === '/superadmin/role/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_role_create;
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::createAction',  '_route' => 'admin_role_create',);
                }
                not_admin_role_create:

                // admin_role_new
                if ($pathinfo === '/superadmin/role/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_role_new;
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::newAction',  '_route' => 'admin_role_new',);
                }
                not_admin_role_new:

                // admin_role_show
                if (preg_match('#^/superadmin/role/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_role_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_role_show')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::showAction',));
                }
                not_admin_role_show:

                // admin_role_edit
                if (preg_match('#^/superadmin/role/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_role_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_role_edit')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::editAction',));
                }
                not_admin_role_edit:

                // admin_role_update
                if (preg_match('#^/superadmin/role/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_role_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_role_update')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::updateAction',));
                }
                not_admin_role_update:

                // admin_role_delete
                if (preg_match('#^/superadmin/role/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_role_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_role_delete')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::deleteAction',));
                }
                not_admin_role_delete:

                // admin_role_export
                if (0 === strpos($pathinfo, '/superadmin/role/exporter') && preg_match('#^/superadmin/role/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_role_export')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\RoleController::getExporter',));
                }

            }

            if (0 === strpos($pathinfo, '/superadmin/user')) {
                // admin_user
                if (rtrim($pathinfo, '/') === '/superadmin/user') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_user');
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::indexAction',  '_route' => 'admin_user',);
                }
                not_admin_user:

                // admin_user_create
                if ($pathinfo === '/superadmin/user/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_user_create;
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::createAction',  '_route' => 'admin_user_create',);
                }
                not_admin_user_create:

                // admin_user_new
                if ($pathinfo === '/superadmin/user/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user_new;
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::newAction',  '_route' => 'admin_user_new',);
                }
                not_admin_user_new:

                // admin_user_show
                if (preg_match('#^/superadmin/user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_show')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::showAction',));
                }
                not_admin_user_show:

                // admin_user_edit
                if (preg_match('#^/superadmin/user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_edit')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::editAction',));
                }
                not_admin_user_edit:

                // admin_user_update
                if (preg_match('#^/superadmin/user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_user_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_update')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::updateAction',));
                }
                not_admin_user_update:

                // admin_user_delete
                if (preg_match('#^/superadmin/user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_user_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_delete')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::deleteAction',));
                }
                not_admin_user_delete:

                // User_autocomplete_user_roles
                if ($pathinfo === '/superadmin/user/autocomplete-forms/get-user_roles') {
                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::getAutocompleteRole',  '_route' => 'User_autocomplete_user_roles',);
                }

                // admin_user_export
                if (0 === strpos($pathinfo, '/superadmin/user/exporter') && preg_match('#^/superadmin/user/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_export')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::getExporter',));
                }

                // admin_user_activar_role
                if (0 === strpos($pathinfo, '/superadmin/user/activar/role') && preg_match('#^/superadmin/user/activar/role/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user_activar_role;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_activar_role')), array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::activarRoleAction',));
                }
                not_admin_user_activar_role:

                // admin_user_correos_confirmacion
                if ($pathinfo === '/superadmin/user/correos/confirmacion') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user_correos_confirmacion;
                    }

                    return array (  '_controller' => 'Sistema\\UserBundle\\Controller\\UserController::resendConfirmationEmailAction',  '_route' => 'admin_user_correos_confirmacion',);
                }
                not_admin_user_correos_confirmacion:

            }

        }

        if (0 === strpos($pathinfo, '/admin/afiliado')) {
            // admin_afiliado
            if (rtrim($pathinfo, '/') === '/admin/afiliado') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_afiliado;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_afiliado');
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::indexAction',  '_route' => 'admin_afiliado',);
            }
            not_admin_afiliado:

            // admin_afiliado_create
            if ($pathinfo === '/admin/afiliado/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_admin_afiliado_create;
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::createAction',  '_route' => 'admin_afiliado_create',);
            }
            not_admin_afiliado_create:

            // admin_afiliado_new
            if ($pathinfo === '/admin/afiliado/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_afiliado_new;
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::newAction',  '_route' => 'admin_afiliado_new',);
            }
            not_admin_afiliado_new:

            // admin_afiliado_show
            if (preg_match('#^/admin/afiliado/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_afiliado_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_afiliado_show')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::showAction',));
            }
            not_admin_afiliado_show:

            // admin_afiliado_edit
            if (preg_match('#^/admin/afiliado/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_afiliado_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_afiliado_edit')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::editAction',));
            }
            not_admin_afiliado_edit:

            // admin_afiliado_update
            if (preg_match('#^/admin/afiliado/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_admin_afiliado_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_afiliado_update')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::updateAction',));
            }
            not_admin_afiliado_update:

            // admin_afiliado_delete
            if (preg_match('#^/admin/afiliado/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_admin_afiliado_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_afiliado_delete')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::deleteAction',));
            }
            not_admin_afiliado_delete:

            if (0 === strpos($pathinfo, '/admin/afiliado/autocomplete-forms/get-afiGarante')) {
                // Afiliado_autocomplete_afiGaranteTipdoc
                if ($pathinfo === '/admin/afiliado/autocomplete-forms/get-afiGaranteTipdoc') {
                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::getAutocompleteAfiGaranteTipdoc',  '_route' => 'Afiliado_autocomplete_afiGaranteTipdoc',);
                }

                // Afiliado_autocomplete_afiGarantedeTipdoc
                if ($pathinfo === '/admin/afiliado/autocomplete-forms/get-afiGarantedeTipdoc') {
                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::getAutocompleteAfiGarantedeTipdoc',  '_route' => 'Afiliado_autocomplete_afiGarantedeTipdoc',);
                }

            }

            // admin_afiliado_export
            if (0 === strpos($pathinfo, '/admin/afiliado/exporter') && preg_match('#^/admin/afiliado/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_afiliado_export')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AfiliadoController::getExporter',));
            }

        }

        if (0 === strpos($pathinfo, '/matriculado')) {
            // ayuda_index
            if ($pathinfo === '/matriculado/ayuda') {
                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AyudaController::indexAction',  '_route' => 'ayuda_index',);
            }

            // ayuda_novedad_index
            if ($pathinfo === '/matriculado/novedades') {
                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AyudaController::novedadIndexAction',  '_route' => 'ayuda_novedad_index',);
            }

            // ayuda_sugeridos_index
            if ($pathinfo === '/matriculado/sugeridos') {
                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AyudaController::sugeridosIndexAction',  '_route' => 'ayuda_sugeridos_index',);
            }

            // ayuda_getArchive
            if ($pathinfo === '/matriculado/get-archive/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_ayuda_getArchive;
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\AyudaController::getArchive',  '_route' => 'ayuda_getArchive',);
            }
            not_ayuda_getArchive:

        }

        if (0 === strpos($pathinfo, '/admin/cuenta')) {
            // admin_cuenta
            if (rtrim($pathinfo, '/') === '/admin/cuenta') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_cuenta');
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaAdminController::indexAction',  '_route' => 'admin_cuenta',);
            }

            // admin_cuenta_export
            if (0 === strpos($pathinfo, '/admin/cuenta/exporter') && preg_match('#^/admin/cuenta/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_cuenta_export')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaAdminController::getExporter',));
            }

            // admin_cuenta_detalle
            if (preg_match('#^/admin/cuenta/(?P<cuenta>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_cuenta_detalle;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_cuenta_detalle')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaAdminController::detalleAction',));
            }
            not_admin_cuenta_detalle:

            if (0 === strpos($pathinfo, '/admin/cuenta/pdf')) {
                // admin_cuenta_pdf_listado
                if ($pathinfo === '/admin/cuenta/pdf/listado') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_cuenta_pdf_listado;
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaAdminController::listadoPdfAction',  '_route' => 'admin_cuenta_pdf_listado',);
                }
                not_admin_cuenta_pdf_listado:

                // admin_cuenta_pdf_detalle
                if (0 === strpos($pathinfo, '/admin/cuenta/pdf/detalle') && preg_match('#^/admin/cuenta/pdf/detalle/(?P<cuenta>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_cuenta_pdf_detalle;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_cuenta_pdf_detalle')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaAdminController::detallePdfAction',));
                }
                not_admin_cuenta_pdf_detalle:

            }

        }

        if (0 === strpos($pathinfo, '/matriculado')) {
            if (0 === strpos($pathinfo, '/matriculado/cuenta')) {
                // front_cuenta
                if (rtrim($pathinfo, '/') === '/matriculado/cuenta') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'front_cuenta');
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaController::indexAction',  '_route' => 'front_cuenta',);
                }

                // front_cuenta_export
                if (0 === strpos($pathinfo, '/matriculado/cuenta/exporter') && preg_match('#^/matriculado/cuenta/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_cuenta_export')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaController::getExporter',));
                }

                // front_cuenta_detalle
                if (preg_match('#^/matriculado/cuenta/(?P<cuenta>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_cuenta_detalle;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_cuenta_detalle')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaController::detalleAction',));
                }
                not_front_cuenta_detalle:

                // front_cuenta_pdf
                if (0 === strpos($pathinfo, '/matriculado/cuenta/pdf') && preg_match('#^/matriculado/cuenta/pdf/(?P<cuenta>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_cuenta_pdf;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_cuenta_pdf')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\CuentaController::cuentaPdfAction',));
                }
                not_front_cuenta_pdf:

            }

            if (0 === strpos($pathinfo, '/matriculado/datos')) {
                // front_dato
                if (rtrim($pathinfo, '/') === '/matriculado/datos') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_dato;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'front_dato');
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DatoController::datosAction',  '_route' => 'front_dato',);
                }
                not_front_dato:

                // front_dato_editar
                if ($pathinfo === '/matriculado/datos/editar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_dato_editar;
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DatoController::editarAction',  '_route' => 'front_dato_editar',);
                }
                not_front_dato_editar:

                // front_dato_pdf
                if ($pathinfo === '/matriculado/datos/pdf') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_dato_pdf;
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DatoController::datosPdfAction',  '_route' => 'front_dato_pdf',);
                }
                not_front_dato_pdf:

            }

            // afiliado
            if (rtrim($pathinfo, '/') === '/matriculado') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'afiliado');
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::indexAction',  '_route' => 'afiliado',);
            }

            if (0 === strpos($pathinfo, '/matriculado/foro')) {
                // foro_mws
                if (preg_match('#^/matriculado/foro(?:/(?P<foro_id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'foro_mws')), array (  'foro_id' => 0,  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::foroAction',));
                }

                // grupoforo_mws
                if (preg_match('#^/matriculado/foro/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grupoforo_mws')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::grupoforoAction',));
                }

            }

            if (0 === strpos($pathinfo, '/matriculado/trabajos')) {
                // front_trabajo
                if ($pathinfo === '/matriculado/trabajos') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_trabajo;
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::indexTrabajoAction',  '_route' => 'front_trabajo',);
                }
                not_front_trabajo:

                // front_trabajo_create
                if ($pathinfo === '/matriculado/trabajos/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_front_trabajo_create;
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::createAction',  '_route' => 'front_trabajo_create',);
                }
                not_front_trabajo_create:

                // front_trabajo_new
                if ($pathinfo === '/matriculado/trabajos/nuevo') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_trabajo_new;
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::trabajoNewAction',  '_route' => 'front_trabajo_new',);
                }
                not_front_trabajo_new:

                // front_trabajo_confirmar
                if ($pathinfo === '/matriculado/trabajos/confirmar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_front_trabajo_confirmar;
                    }

                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::trabajoConfirmarAction',  '_route' => 'front_trabajo_confirmar',);
                }
                not_front_trabajo_confirmar:

                // front_trabajo_show
                if (preg_match('#^/matriculado/trabajos/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_front_trabajo_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_trabajo_show')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::showAction',));
                }
                not_front_trabajo_show:

                if (0 === strpos($pathinfo, '/matriculado/trabajos/pdf')) {
                    // front_trabajo_pdf
                    if (preg_match('#^/matriculado/trabajos/pdf/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_front_trabajo_pdf;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_trabajo_pdf')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::trabajoPdfAction',));
                    }
                    not_front_trabajo_pdf:

                    // front_trabajo_pdfpreview
                    if (0 === strpos($pathinfo, '/matriculado/trabajos/pdfpreview') && preg_match('#^/matriculado/trabajos/pdfpreview(?:/(?P<form>[^/]++))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_front_trabajo_pdfpreview;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_trabajo_pdfpreview')), array (  'form' => NULL,  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::trabajoPdfpreviewAction',));
                    }
                    not_front_trabajo_pdfpreview:

                }

                // front_trabajo_export
                if (0 === strpos($pathinfo, '/matriculado/trabajos/exporter') && preg_match('#^/matriculado/trabajos/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_trabajo_export')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\DefaultController::getExporter',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/admin/trabajo')) {
            // admin_trabajo
            if (rtrim($pathinfo, '/') === '/admin/trabajo') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_trabajo;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_trabajo');
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::indexAction',  '_route' => 'admin_trabajo',);
            }
            not_admin_trabajo:

            // admin_trabajo_create
            if ($pathinfo === '/admin/trabajo/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_admin_trabajo_create;
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::createAction',  '_route' => 'admin_trabajo_create',);
            }
            not_admin_trabajo_create:

            // admin_trabajo_new
            if ($pathinfo === '/admin/trabajo/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_trabajo_new;
                }

                return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::newAction',  '_route' => 'admin_trabajo_new',);
            }
            not_admin_trabajo_new:

            // admin_trabajo_show
            if (preg_match('#^/admin/trabajo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_trabajo_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_trabajo_show')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::showAction',));
            }
            not_admin_trabajo_show:

            // admin_trabajo_edit
            if (preg_match('#^/admin/trabajo/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_trabajo_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_trabajo_edit')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::editAction',));
            }
            not_admin_trabajo_edit:

            // admin_trabajo_update
            if (preg_match('#^/admin/trabajo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_admin_trabajo_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_trabajo_update')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::updateAction',));
            }
            not_admin_trabajo_update:

            // admin_trabajo_delete
            if (preg_match('#^/admin/trabajo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_admin_trabajo_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_trabajo_delete')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::deleteAction',));
            }
            not_admin_trabajo_delete:

            // admin_trabajo_export
            if (0 === strpos($pathinfo, '/admin/trabajo/exporter') && preg_match('#^/admin/trabajo/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_trabajo_export')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::getExporter',));
            }

            if (0 === strpos($pathinfo, '/admin/trabajo/autocomplete-forms/get-')) {
                // Trabajo_autocomplete_user
                if ($pathinfo === '/admin/trabajo/autocomplete-forms/get-user') {
                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::getAutocompleteUser',  '_route' => 'Trabajo_autocomplete_user',);
                }

                // Trabajo_autocomplete_servicio
                if ($pathinfo === '/admin/trabajo/autocomplete-forms/get-servicio') {
                    return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::getAutocompleteTareas',  '_route' => 'Trabajo_autocomplete_servicio',);
                }

            }

            // admin_trabajo_revisar
            if (0 === strpos($pathinfo, '/admin/trabajo/revisar') && preg_match('#^/admin/trabajo/revisar/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_trabajo_revisar;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_trabajo_revisar')), array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\TrabajoController::revisarAction',));
            }
            not_admin_trabajo_revisar:

        }

        // validacion_index
        if (rtrim($pathinfo, '/') === '/validacion') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'validacion_index');
            }

            return array (  '_controller' => 'Sistema\\CPCEBundle\\Controller\\ValidacionController::indexAction',  '_route' => 'validacion_index',);
        }

        // sistema_clasificado_default_index
        if (rtrim($pathinfo, '/') === '/hello') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'sistema_clasificado_default_index');
            }

            return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\DefaultController::indexAction',  '_route' => 'sistema_clasificado_default_index',);
        }

        if (0 === strpos($pathinfo, '/admin/ofertalaboral')) {
            // admin_ofertalaboral
            if (rtrim($pathinfo, '/') === '/admin/ofertalaboral') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_ofertalaboral;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_ofertalaboral');
                }

                return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::indexAction',  '_route' => 'admin_ofertalaboral',);
            }
            not_admin_ofertalaboral:

            // admin_ofertalaboral_create
            if ($pathinfo === '/admin/ofertalaboral/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_admin_ofertalaboral_create;
                }

                return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::createAction',  '_route' => 'admin_ofertalaboral_create',);
            }
            not_admin_ofertalaboral_create:

            // admin_ofertalaboral_new
            if ($pathinfo === '/admin/ofertalaboral/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_ofertalaboral_new;
                }

                return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::newAction',  '_route' => 'admin_ofertalaboral_new',);
            }
            not_admin_ofertalaboral_new:

            // admin_ofertalaboral_show
            if (preg_match('#^/admin/ofertalaboral/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_ofertalaboral_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ofertalaboral_show')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::showAction',));
            }
            not_admin_ofertalaboral_show:

            // admin_ofertalaboral_edit
            if (preg_match('#^/admin/ofertalaboral/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_ofertalaboral_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ofertalaboral_edit')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::editAction',));
            }
            not_admin_ofertalaboral_edit:

            // admin_ofertalaboral_update
            if (preg_match('#^/admin/ofertalaboral/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_admin_ofertalaboral_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ofertalaboral_update')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::updateAction',));
            }
            not_admin_ofertalaboral_update:

            // admin_ofertalaboral_delete
            if (preg_match('#^/admin/ofertalaboral/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_admin_ofertalaboral_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ofertalaboral_delete')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::deleteAction',));
            }
            not_admin_ofertalaboral_delete:

            // admin_ofertalaboral_export
            if (0 === strpos($pathinfo, '/admin/ofertalaboral/exporter') && preg_match('#^/admin/ofertalaboral/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ofertalaboral_export')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::getExporter',));
            }

            // OfertaLaboral_autocomplete_cvPostulantes
            if ($pathinfo === '/admin/ofertalaboral/autocomplete-forms/get-cvPostulantes') {
                return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\OfertaLaboralController::getAutocompleteUserCurriculum',  '_route' => 'OfertaLaboral_autocomplete_cvPostulantes',);
            }

        }

        if (0 === strpos($pathinfo, '/matriculado/curriculum')) {
            // matriculado_curriculum_create
            if ($pathinfo === '/matriculado/curriculum/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_matriculado_curriculum_create;
                }

                return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::createAction',  '_route' => 'matriculado_curriculum_create',);
            }
            not_matriculado_curriculum_create:

            // matriculado_curriculum_new
            if ($pathinfo === '/matriculado/curriculum/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_matriculado_curriculum_new;
                }

                return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::newAction',  '_route' => 'matriculado_curriculum_new',);
            }
            not_matriculado_curriculum_new:

            // matriculado_curriculum_edit
            if (preg_match('#^/matriculado/curriculum/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_matriculado_curriculum_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'matriculado_curriculum_edit')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::editAction',));
            }
            not_matriculado_curriculum_edit:

            // matriculado_curriculum_update
            if (preg_match('#^/matriculado/curriculum/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_matriculado_curriculum_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'matriculado_curriculum_update')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::updateAction',));
            }
            not_matriculado_curriculum_update:

            // matriculado_curriculum_delete
            if (preg_match('#^/matriculado/curriculum/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_matriculado_curriculum_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'matriculado_curriculum_delete')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::deleteAction',));
            }
            not_matriculado_curriculum_delete:

            // matriculado_curriculum_export
            if (0 === strpos($pathinfo, '/matriculado/curriculum/exporter') && preg_match('#^/matriculado/curriculum/exporter/(?P<format>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'matriculado_curriculum_export')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::getExporter',));
            }

            if (0 === strpos($pathinfo, '/matriculado/curriculum/autocomplete-forms/get-')) {
                // UserCurriculum_autocomplete_usercv
                if ($pathinfo === '/matriculado/curriculum/autocomplete-forms/get-usercv') {
                    return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::getAutocompleteUser',  '_route' => 'UserCurriculum_autocomplete_usercv',);
                }

                // UserCurriculum_autocomplete_ofertaLaborals
                if ($pathinfo === '/matriculado/curriculum/autocomplete-forms/get-ofertaLaborals') {
                    return array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::getAutocompleteOfertaLaboral',  '_route' => 'UserCurriculum_autocomplete_ofertaLaborals',);
                }

            }

            // matriculado_getCurriculum
            if (0 === strpos($pathinfo, '/matriculado/curriculum/get-cvmatriculado') && preg_match('#^/matriculado/curriculum/get\\-cvmatriculado/(?P<matriculadoId>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_matriculado_getCurriculum;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'matriculado_getCurriculum')), array (  '_controller' => 'Sistema\\ClasificadoBundle\\Controller\\UserCurriculumController::getCVMatriculado',));
            }
            not_matriculado_getCurriculum:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
